sap.ui.define([
		"sap/ui/model/json/JSONModel",
		"sap/ui/Device"
	], function (JSONModel, Device) {
		"use strict";

		return {

			createDeviceModel : function () {
				var oModel = new JSONModel(Device);
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			},
			createFilterModel : function () {
				var oModel = new JSONModel();
			//	this.setModel(oModel,"mFilters");
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			},
			createIconModel : function () {
				var oModel = new JSONModel("./model/Icons.json");
			 
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			}


		};

	}
);