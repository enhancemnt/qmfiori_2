jQuery.sap.declare("com.nauticana.demo.model.formatter");

com.nauticana.demo.model.formatter = {

	numberUnit: function(sValue) {
		if (!sValue) {
			return "";
		}
		return parseFloat(sValue).toFixed(2);
	},
	formateDate: function(value) {
		if (!value) {
            return value;
        }

        // var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "dd.MM.yyyy" });
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "yyyyMMdd" });
        var dateFormatted = dateFormat.format(value);
        return dateFormatted;
	},
	
	formatTime: function(value){
		var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({ pattern: "HHmmss" });
		var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
		var timeStr = timeFormat.format(value);
		return timeStr;
	},
	
	timeFormat: function(time) {
        if (time == undefined) return "";
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({ pattern: "HH:mm" });
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs));
        return timeStr;
    },
    
    formateDate2: function(value){
    	if(value){
	    	var items = value.split("/Date(");
	    	if(items){
	    		var date = items[1].split(")/")[0];
	    		return new Date(parseInt(date));
	    	}
    	}
    	return "";
    },
    
    formatIconColor: function (status) {
    	if (status === "A") {
        	return "green";
    	}else if (status === "R"){
            return "red";
    	}
	},
    formatIcon: function (status) {
    	if(status==="A"){
    		return 'sap-icon://accept';
    	}else if(status==="R"){
    		return 'sap-icon://decline';
    	}
    }

};