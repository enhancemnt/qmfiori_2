jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
		"sap/ui/test/Opa5",
		"com/nauticana/demo/test/integration/pages/Common",
		"sap/ui/test/opaQunit",
		"com/nauticana/demo/test/integration/pages/Worklist",
		"com/nauticana/demo/test/integration/pages/Object",
		"com/nauticana/demo/test/integration/pages/NotFound",
		"com/nauticana/demo/test/integration/pages/Browser",
		"com/nauticana/demo/test/integration/pages/App"
	], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.nauticana.demo.view."
	});

	sap.ui.require([
		"com/nauticana/demo/test/integration/WorklistJourney",
		"com/nauticana/demo/test/integration/ObjectJourney",
		"com/nauticana/demo/test/integration/NavigationJourney",
		"com/nauticana/demo/test/integration/NotFoundJourney"
	], function () {
		QUnit.start();
	});
});