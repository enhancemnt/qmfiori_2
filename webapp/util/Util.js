jQuery.sap.declare("com.nauticana.demo.util.Util");

com.nauticana.demo.util.Util = {
	
		Filter:sap.ui.model.Filter,
		FilterOperator:sap.ui.model.FilterOperator,

	setNotifTable: function(items) {

		this.notifTable = items;
		if (this.notifFunc) {
			for (var i in this.notifFunc) {
				for (var i in this.notifTable) {
					if (this.notifTable[i].TableName === this.notifFunc[0].tableName) {
						this.notifFunc[0].func(this.notifTable[i].TableColumnSet.results, this.t);
					}
				}
			}
		} else {}
	},

	getNotifTable: function(func, t, tableName) {
		this.t = t;
		if (this.notifTable) {
			var items = [];
			for (var i in this.notifTable) {
				if (this.notifTable[i].TableName === tableName) {
					func(this.notifTable[i].TableColumnSet.results, t);
				}
			}
		} else {
			if (!this.notifFunc) {
				this.notifFunc = [];
			}
			this.notifFunc.push({
				func: func,
				tableName: tableName
			});
		}
		
		if (!this.readTableCustomFlag) {
			this.readTableCustom();
		}
	},
	
		
	readTableCustom: function() {
		var t = this;
		var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
		t.readTableCustomFlag = true;

		var sPath = "/DuzenSet(TableName='1')";
		var sPath = "/DuzenSet";
		//debugger;
		oModel.read(sPath, {
			urlParameters: {
				"$expand": "TableColumnSet"
			},
			success: function(oData, response) {
				t.setNotifTable(oData.results);
			},
			error: function(oError) {}
		});
		
	},

//-----------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------- Customising ------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//
	readCustomising: function() {
		if (!this.cutomisingFlag) {
			this.cutomisingFlag = true;
			this.readUserAuth();
		}
	},
	
	readCustomisingNotif: function(func,t){
		if (!this.cutomisingFlag) {
			this.cutomisingFlag = true;
			this.readUserAuth();
			this.notifFilterFunc = func;
			this.notifThis = t;
		}else if(this.AuthCompleted===true){
			func(t);
		}
	},
	
	readCustomisingWithFunc: function(items){
		
	},
	
	readUserAuth: function() {
		var t = this;
		var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");
		var ofilter = [new this.Filter("IvTip", this.FilterOperator.EQ, "1")];
		var sPath1 = "/ZQMBL_SCR_AUTH_TTSet";
		
		t._showBusy();
		oModel.read(sPath1, {
			filters: ofilter,
			success: function(oData, response) {
				var UnameSet = {
					Uname: oData.results[0].Uname,
					ZEditKnc: oData.results[0].ZEditKnc,
					ZEditKk: oData.results[0].ZEditKk,
					ZEditDeft: oData.results[0].ZEditDeft,
					ZDispKnc: oData.results[0].ZDispKnc,
					ZDispKk: oData.results[0].ZDispKk,
					ZDispDeft: oData.results[0].ZDispDeft,
					ZIvTip: oData.results[0].IvTip
				};
				var oJsonModel = new sap.ui.model.json.JSONModel();
				oJsonModel.setData(UnameSet);
				sap.ui.getCore().setModel(oJsonModel, "UserAuthModel");
				t._hideBusy();
			},
			error: function(oError) {
				t._hideBusy();
			}
		});
		
		var sPath2 = "/EtNotifAuthSet";
		oModel.read(sPath2, { 
			success : function(oData,response)
			{
				var EtNotifAuthSet = {
					Uname: oData.results[0].Uname,
					Qmart: oData.results[0].Qmart,
					CreateNot: oData.results[0].CreateNot,
					ChangeNot: oData.results[0].ChangeNot,
					DisplayNot: oData.results[0].DisplayNot
				};
				
				var oJsonModel = new sap.ui.model.json.JSONModel();
				oJsonModel.setData(EtNotifAuthSet);
				sap.ui.getCore().setModel(oJsonModel, "EtNotifAuthModel");
	
				t._hideBusy();
			},
			error: function(oError) {
				t._hideBusy();
			}
		});
		
		var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
		var sPath = "/AuthorizationSet(Uname='')";
		
		t._showBusy();
		oModel.read(sPath, {
			urlParameters: {
				"$expand": "AuthNotifSet"
			},
			success: function(oData, response) {
				t._hideBusy();
				
				var oJsonModel = new sap.ui.model.json.JSONModel({
					AuthNotif :oData.AuthNotifSet.results
				});
				sap.ui.getCore().setModel(oJsonModel, "AuthModel");
				
				if(t.notifFilterFunc){
					t.notifFilterFunc(t.notifThis);
				}
				t.AuthCompleted=true;
			},
			error: function(oError) {
				t._hideBusy();
			}
		});
		
		var sPath = "/CustomDataExpandSet(Data='X')";
		
		oModel.read(sPath, {
			urlParameters: {
				"$expand": "CustomUsrStatus,CustomUserInsp,CustomUsageDecision,CustomQualitaCat,CustomPriority,CustomNotifType,CustomNotifICatHead,CustomNotifICatDetail,CustomInspeType"
			},
			success: function(oData, response) {
				
				var oJsonModel = new sap.ui.model.json.JSONModel({
					CustomUsrStatus			:oData.CustomUsrStatus.results,
					CustomUserInsp			:oData.CustomUserInsp.results,
					CustomUsageDecision		:oData.CustomUsageDecision.results,
					CustomQualitaCat		:oData.CustomQualitaCat.results,
					CustomPriority			:oData.CustomPriority.results,
					CustomNotifType			:oData.CustomNotifType.results,
					CustomNotifICatHead		:oData.CustomNotifICatHead.results,
					CustomNotifICatDetail	:oData.CustomNotifICatDetail.results,
					CustomInspeType			:oData.CustomInspeType.results,
				});
				sap.ui.getCore().setModel(oJsonModel, "CustomInfoModel");
				// t.setNotifTable(oData.results);
			},
			error: function(oError) {}
		});
	},

//------------------------------- Busy Indicator ----------------------------------------------//
	showBusy: function(){
		this._showBusy();
	},
    _showBusy: function() {
    	if(this.busyCount==undefined){
    		this.busyCount = 1;
    	}else{
            this.busyCount += 1;
    	}
        sap.ui.core.BusyIndicator.show();
    },
    
	hideBusy: function(){
		this._hideBusy();
	},
    _hideBusy: function() {
        this.busyCount -= 1;
        if (this.busyCount <= 0) {
            this.busyCount = 0;
            sap.ui.core.BusyIndicator.hide();
        }
    },
	

};