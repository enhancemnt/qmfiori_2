/*global location */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator"
], function(Util,BaseController, JSONModel, Dialog, List, StandardListItem, Button, formatter, MessageBox, MessageToast, History, Filter,
	FilterOperator) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.KPHataGiris", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {

			var oRouter = this.getRouter();

			oRouter.getRoute("kphatagiris").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			var KPSSonuc = new JSONModel({
				enabled: true
			});

			this.setModel(KPSSonuc, "oSonuc");

			this.setModel(oViewModel, "detailView");

			var SelectHataModel = new JSONModel({

				Fekat: null,
				Fegrp: null,
				Fecod: null

			});

			var SelectHataModel2 = new JSONModel({

				Otkat: null,
				Otgrp: null,
				Oteil: null
			});
			var SelectHataModel3 = new JSONModel({
				Insplot: null
			});

			this.setModel(SelectHataModel3, "SelectHataModel3");
			this.setModel(SelectHataModel2, "SelectHataModel2");
			this.setModel(SelectHataModel, "SelectHataModel");
			
		},

		_onRouteMatched: function(oEvent) {
			//debugger;
			var oArgs, oView, oSelectedKey, oSelectedInspoper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			oSelectedKey = oArgs.objectId;

			this.getView().getModel("SelectHataModel3").setData({

				Insplot: oSelectedKey

			});

			var EtQalsSetM = sap.ui.getCore().getModel("EtQalsSetModel");
			var EtQals = {};
			EtQals.EtQalsCollection = EtQalsSetM.getData();
			var EtQalsM = new JSONModel(EtQals);
			this.getView().setModel(EtQalsM, "EtQalsSetModel");

			var NotifiCatDetlSetM = sap.ui.getCore().getModel("NotifiCatDetlSetModel");
			var NotifiCatDetlM = {};
			NotifiCatDetlM.NotifiCatCollection = NotifiCatDetlSetM.getData();
			var NotifiCatDM = new JSONModel(NotifiCatDetlM);
			this.getView().setModel(NotifiCatDM, "NotifiCatDetlSetModel");
			//var len1 = sap.ui.getCore().getModel("NotifiCatDetlSetModel").getData().length;

			var NotifiCatHeadSetM = sap.ui.getCore().getModel("NotifiCatHeadSetModel");

			var NotifiCatHeadlM = {};
			NotifiCatHeadlM.NotifiCatHeadCollection = NotifiCatHeadSetM.getData();
			var NotifiCatHeadDM = new JSONModel(NotifiCatHeadlM);
			this.getView().setModel(NotifiCatHeadDM, "NotifiCatHeadSetModel");

		},
		handleCloseHkod: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInputHataKod = aContexts[0].getModel().getProperty(oSelPath).Code;
			var oInputHataKodGrp = aContexts[0].getModel().getProperty(oSelPath).Qcodegrp;
			var oInputHataKod2 = aContexts[0].getModel().getProperty(oSelPath).Kurztext;

			var Index = parseInt(oSelPath.substr(26), 10);

			var Fekat = oEvent.getSource().getModel("NotifiCatGroup2Model").getData().NotifiCatHeadCollection[Index].Fekat;
			//	var Qcodegrp	= oEvent.getSource().getModel("NotifiCatGroup2Model").getData().NotifiCatHeadCollection[Index].Qcodegrp;

			this.getView().byId("__hataKod").setValue(oInputHataKod + " " + oInputHataKodGrp);
			this.getView().byId("__hataKod2").setValue(oInputHataKod2);

			this.getView().getModel("SelectHataModel").setData({

				Fekat: Fekat,
				Fegrp: oInputHataKodGrp,
				Fecod: oInputHataKod

			});

			//	 this._oDialogHataKod.close();
		},

		handleCloseHyeri: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInputHataYeri = aContexts[0].getModel().getProperty(oSelPath).Code;
			var oInputHataYeriGrp = aContexts[0].getModel().getProperty(oSelPath).Qcodegrp;
			var oInputHataYeri2 = aContexts[0].getModel().getProperty(oSelPath).Kurztext;

			var Index = parseInt(oSelPath.substr(31), 10);

			var Otkat = oEvent.getSource().getModel("NotifiCatHYeriGroup2Model").getData().NotifiCatHYeriHeadGroupCollection[Index].Otkat;

			this.getView().byId("_hataYeri").setValue(oInputHataYeri + " " + oInputHataYeriGrp);
			this.getView().byId("_hataYeri2").setValue(oInputHataYeri2);

			this.getView().getModel("SelectHataModel2").setData({

				Otkat: Otkat,
				Otgrp: oInputHataYeriGrp,
				Oteil: oInputHataYeri
			});

			//	this._oDialogHataYeri.close();
		},
		onSaveHata: function(oEvent) {

			//debugger;

			var ISubsys = "000001";
			var Insplot = this.getModel("SelectHataModel3").getData().Insplot;
			var Satzart = "Q90";
			var Posnr = "0001";
			var Fekat = this.getModel("SelectHataModel").getData().Fekat;
			var Fecod = this.getModel("SelectHataModel").getData().Fecod;
			var Fegrp = this.getModel("SelectHataModel").getData().Fegrp;
			var Anzfehler = this.getView().byId("__hatasayisiID").getValue();
			var Otkat = this.getModel("SelectHataModel2").getData().Otkat;
			var Otgrp = this.getModel("SelectHataModel2").getData().Otgrp;
			var Oteil = this.getModel("SelectHataModel2").getData().Oteil;
			var Fetxt = this.getView().byId("__aciklama").getValue();

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_DEMO_SRV");
			//var sPath = "/InspLotOpCharSet"+"(Insplot='"+Insplot+"',Inspoper='"+Inspoper+"',Inspchar='"+Inspchar+"')";
			// var sPath ="/DefectItemsSet"+"(Prueflos='"+Insplot+"')";

			var sPath = "/DefectItemsSet";
			var oPostData = {
				ISubsys: ISubsys,
				Prueflos: Insplot,
				Satzart: Satzart,
				Posnr: Posnr,
				Fekat: Fekat,
				Fecod: Fecod,
				Fegrp: Fegrp,
				Anzfehler: Anzfehler,
				Otkat: Otkat,
				Otgrp: Otgrp,
				Oteil: Oteil,
				Fetxt: Fetxt

			};

			oModel.create(sPath, oPostData,

				function(oData, response) {
					MessageToast.show("Başarılı bir şekilde Kaydedildi", {
						duration: 3000
					});
					//  console.log(oData);
					console.log(response);
				},

				function(e) {
					sap.m.MessageToast.show("Kaydetme sırasında hata oluştu", {
						duration: 3000
					});
					//console.log("Update failed", JSON.stringify(e));
				});
			var that = this;
			oModel.attachRequestSent(function() {
				sap.ui.core.BusyIndicator.show(5);
			});
			oModel.attachRequestCompleted(function(oData) {
				//debugger;
				sap.ui.core.BusyIndicator.hide();
				if (oData.getParameters("success")) {
					sap.m.MessageToast.show("Başarılı bir şekilde Kaydedildi", {
						duration: 3000
					});
					var oHistory = sap.ui.core.routing.History.getInstance();
					var sPreviousHash = oHistory.getPreviousHash();
					if (sPreviousHash !== undefined) {
						//	window.history.go(-1);
						that.getRouter().navTo("master", {}, true);
					} else {
						that.getRouter().navTo("master", {}, true);
					}

				}
			});
			oModel.attachRequestFailed(function(oEvent) {

				var operationResult = "0";
				var message = $(oEvent.response.body).find('message').first().text();
				sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR, sap.m.oBundle.getText("ERROR"));
			});

			//   sap.ui.getCore().getModel("EtZinsCharModel").refresh();
			//   sap.ui.getCore().getModel("EtSingleResultsModel").refresh();
		},
		onValueHelpHatakod: function(oEvent) {
			//debugger;
			if (!this._oDialogHataKod) {
				this._oDialogHataKod = sap.ui.xmlfragment("com.nauticana.demo.view.DialogHataKod", this, true);
				this._oDialogHataKod.setModel(this.getView().getModel("i18n"), "i18n");
			}

			var IM = this.getModel("NotifiCatGroupModel");
			this._oDialogHataKod.setModel(IM, "NotifiCatGroupModel");
			var IM2 = this.getModel("NotifiCatGroup2Model");
			this._oDialogHataKod.setModel(IM2, "NotifiCatGroup2Model");

			this.getView().addDependent(this._oDialogHataKod);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataKod);

			//	var  path = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.sPath;
			//	var model = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.getModel();

			/*			var Fekat =  "1" ;//model.getProperty(path).Fekat;
					var Rbnr = "1"; //model.getProperty(path).Rbnr;
				 
	 
					var ofilter =  [new sap.ui.model.Filter("Qkatart", FilterOperator.EQ, Fekat),new sap.ui.model.Filter("Rbnr", FilterOperator.EQ, Rbnr)];	
				
					var oList = sap.ui.getCore().byId("_hataKod");
					var oBinding = oList.getBinding("items");
					oBinding.filter(ofilter);
					*/

			this._oDialogHataKod.open();

		},
		onValueHelpHataYeri: function() {
			//debugger;

			if (!this._oDialogHataYeri) {
				this._oDialogHataYeri = sap.ui.xmlfragment("com.nauticana.demo.view.DialogHataYeri", this, true);
				this._oDialogHataYeri.setModel(this.getView().getModel("i18n"), "i18n");

			}
			var IM = this.getModel("NotifiCatHYeriGroupModel");
			var IM2 = this.getModel("NotifiCatHYeriGroup2Model");
			this._oDialogHataYeri.setModel(IM, "NotifiCatHYeriGroupModel");
			this._oDialogHataYeri.setModel(IM2, "NotifiCatHYeriGroup2Model");
			//    sap.ui.getCore().getModel('NotifiCatHYeriGroupModel').refresh();

			this.getView().addDependent(this._oDialogHataYeri);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataYeri);

			//	var  path = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.sPath;
			//	var model = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.getModel();

			/*		var Otkat ="1" ;//model.getProperty(path).Otkat;
				    	var Rbnr = "1" ;//model.getProperty(path).Rbnr;
				 
	 
					var ofilter =  [new sap.ui.model.Filter("Rbnr", FilterOperator.EQ, Rbnr),new sap.ui.model.Filter("Qkatart", FilterOperator.EQ, Otkat)];	
				
					var oList = sap.ui.getCore().byId("_idMunferit");
					var oBinding = oList.getBinding("items");
					oBinding.filter(ofilter);*/

			this._oDialogHataYeri.open();
		},

		onBeforeRendering: function() {
			//debugger;

			var NotifiCatGroup = {};
			NotifiCatGroup.NotifiCatGroupCollection = new Array();

			var NotifiCatHeadGroup = {};
			NotifiCatHeadGroup.NotifiCatHeadCollection = new Array();

			var collection1 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
			var collection2 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
			for (var key in collection2) {
				if (key === 'length' || !collection2.hasOwnProperty(key)) continue;
				for (var key1 in collection1) {
					if (key1 === 'length' || !collection1.hasOwnProperty(key1)) continue;
					if ((collection2[key].Rbnr === collection1[key1].Rbnr) && (collection2[key].Fekat === collection1[key1].Qkatart)) {

						NotifiCatGroup.NotifiCatGroupCollection.push(collection1[key1]);
						NotifiCatHeadGroup.NotifiCatHeadCollection.push(collection2[key]);
						//	break;
					}
				}
			}

			var NotifiCatGroupDM = new JSONModel(NotifiCatGroup);
			this.getView().setModel(NotifiCatGroupDM, "NotifiCatGroupModel");

			var NotifiCatGroup2DM = new JSONModel(NotifiCatHeadGroup);
			this.getView().setModel(NotifiCatGroup2DM, "NotifiCatGroup2Model");

			var NotifiCatHYeriGroup = {};
			NotifiCatHYeriGroup.NotifiCatHYeriGroupCollection = new Array();
			var NotifiCatHYeriHeadGroup = {};
			NotifiCatHYeriHeadGroup.NotifiCatHYeriHeadGroupCollection = new Array();

			var collection3 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
			var collection4 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
			for (var key in collection4) {
				if (key === 'length' || !collection4.hasOwnProperty(key)) continue;
				for (var key1 in collection3) {
					if (key1 === 'length' || !collection3.hasOwnProperty(key1)) continue;
					if ((collection4[key].Rbnr === collection3[key1].Rbnr) && (collection4[key].Otkat === collection3[key1].Qkatart)) {

						NotifiCatHYeriGroup.NotifiCatHYeriGroupCollection.push(collection3[key1]);
						NotifiCatHYeriHeadGroup.NotifiCatHYeriHeadGroupCollection.push(collection4[key]);
						//	break;
					}
				}
			}

			var NotifiCatHYeriGroupDM = new JSONModel(NotifiCatHYeriGroup);
			this.getView().setModel(NotifiCatHYeriGroupDM, "NotifiCatHYeriGroupModel");

			var NotifiCatHYeriGroup2DM = new JSONModel(NotifiCatHYeriHeadGroup);
			this.getView().setModel(NotifiCatHYeriGroup2DM, "NotifiCatHYeriGroup2Model");

			this.getView().getModel("NotifiCatGroupModel").refresh();
			this.getView().getModel("NotifiCatHYeriGroupModel").refresh();

			//	var ofilter =  [new sap.ui.model.Filter(NotifiCatDetlSetM.getData().Rbnr, FilterOperator.EQ, NotifiCatHeadSetM.getData().Rbnr)];	

			//			var oList = sap.ui.getCore().byId("_hataKod");
			//			var oBinding = oList.getBinding("items");
			//			oBinding.filter(ofilter);

		},

		onAfterRendering: function() {
			//debugger;
			//	var len2 = sap.ui.getCore().getModel("NotifiCatHeadSetModel").getData().length;

		},

		onNavBack: function() {

			this.getView().byId("__hatasayisiID").setValue("");
			this.getView().byId("__aciklama").setValue("");

			this.getView().byId("__hataKod").setValue("");
			this.getView().byId("__hataKod2").setValue("");
			this.getView().byId("_hataYeri").setValue("");
			this.getView().byId("_hataYeri2").setValue("");

			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				this.getRouter().navTo("master", {}, true);
				//	history.go(-1);
			} else {
				this.getRouter().navTo("master", {}, true);
			}

		},
		onExit: function() {
			if (this._oDialogHataYeri) {
				this._oDialogHataYeri.destroy();
			}
			if (this._oDialogHataKod) {
				this._oDialogHataKod.destroy();
			}

		}

	});

});