/*global location */
sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	
	], function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";
	return BaseController.extend("com.nauticana.demo.controller.BLDegistir", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLDegistir
		 */
			onInit: function() {
				
				//debugger;
			var oRouter = this.getRouter();

		 	var oModel = new JSONModel();
			oModel.setData({
				dateValue: new Date(),
				timeValue: new Date()
			});
			this.getView().setModel(oModel,"oModel");

		//	sap.ui.getCore().byId("iddate").setMinDate(new Date());
		//	this.byId("idtime").setDateValue(new Date());
			
					this._iEventDate = 0;
					this._iEventTime = 0;

			// for the data binding example do not use the change event for check but the data binding parsing events
			sap.ui.getCore().attachParseError(
					function(oEvent) {
						var oElement = oEvent.getParameter("element");

						if (oElement.setValueState) {
							oElement.setValueState(sap.ui.core.ValueState.Error);
						}
					});

			sap.ui.getCore().attachValidationSuccess(
					function(oEvent) {
						var oElement = oEvent.getParameter("element");

						if (oElement.setValueState) {
							oElement.setValueState(sap.ui.core.ValueState.None);
						}
					});		
			oRouter.getRoute("bldegistir").attachMatched(this._onRouteMatched, this);
			
			
			
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});
 
				this.setModel(oViewModel, "detailView");

			},
			
		handleChangeDate: function (oEvent) {
			//debugger;
		//	var oText = this.byId("T1");
			var oDP = oEvent.oSource;
			var sValue = oEvent.getParameter("value");
			var bValid = oEvent.getParameter("valid");
			this._iEventDate++;
		//	oText.setText("Change - Event " + this._iEvent + ": DatePicker " + oDP.getId() + ":" + sValue);

			if (bValid) {
				oDP.setValueState(sap.ui.core.ValueState.None);
			} else {
				oDP.setValueState(sap.ui.core.ValueState.Error);
			}
		},
			handleChangeTime: function (oEvent) {
				//debugger;
			//	var oText = this.byId("T2");
				var oTP = oEvent.oSource;
				var sValue = oEvent.getParameter("value");
				var bValid = oEvent.getParameter("valid");
				this._iEventTime++;
			//	oText.setText("Change - Event " + this._iEvent + ": TimePicker " + oTP.getId() + ":" + sValue);

				if (bValid) {
					oTP.setValueState(sap.ui.core.ValueState.None);
				} else {
					oTP.setValueState(sap.ui.core.ValueState.Error);
				}
						
			},
		 	
			onExit : function () {
				if (this._oDialog) {
					this._oDialog.destroy();
				}
			},

			handleOpenDialog: function (oEvent) {
				// create popover
				if (!this._oDialog) {
					this._oDialog = sap.ui.xmlfragment("fragment", "com.nauticana.demo.view.DateTime", this);
					this._oDialog.setModel(this.getView().getModel("i18n"), "i18n");
					this.getView().addDependent(this._oDialog);
						var	IM = this.getModel("oModel");
					    this._oDialog.setModel(IM);
			 	    
				}

				this._oDialog.open();
			},

			handleOKPress: function () {
			 
				 
				this._oDialog.close();
			this.getRouter().navTo("bllistesi", {}, true);
			 
			},

			handleCancelPress: function () {
			 
				this._oDialog.close();
					this.getRouter().navTo("bllistesi", {}, true);
			},
		 	
	 
			
			onTamamla : function(oEvent) {
				
				this.handleOpenDialog();
			},
			
			onSaveKarar : function(oEvent) {
					this.getRouter().navTo("bllistesi", {}, true);
			},
			
			
			onNavBack : function(oEvent) {

				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
					history.go(-1);
				} else {
					this.getRouter().navTo("master", {}, true);
				}
				
			},
			
		_onRouteMatched : function() {
				//debugger;
			var NotifModel = sap.ui.getCore().getModel("NotifModel");
		  
			this.getView().setModel(NotifModel,"NotifModel"); 
			
			
			
			}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLDegistir
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLDegistir
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLDegistir
		 */
		//	onExit: function() {
		//
		//	}

	});

});