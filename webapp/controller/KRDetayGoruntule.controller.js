/*global history */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller"
], function(BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.KRDetayGoruntule", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function(oEvent) {
			//debugger;
			//  filtering  
			this._oView = this.getView();

			// Control state model
			var oList = this.byId("list"),
				oViewModel = this._createViewModel(),
				// Put down master list's original value for busy indicator delay,
				// so it can be restored later on. Busy handling on the master list is
				// taken care of by the master list itself.
				iOriginalBusyDelay = oList.getBusyIndicatorDelay();

			this._oGroupSortState = new GroupSortState(oViewModel, grouper.groupUnitNumber(this.getResourceBundle()));

			this._oList = oList;
			// keeps the filter and search state
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};

			this.setModel(oViewModel, "masterView");
			// Make sure, busy indication is showing immediately so there is no
			// break after the busy indication for loading the view's meta data is
			// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
			oList.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for the list
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});

			var oSonucGModel = new JSONModel({
				enabled: false,
				Zkk: null
			});
			var oKulKarModel = new JSONModel({
				enabled: false
			});
			var oHataGModel = new JSONModel({
				enabled: false
			});
			var oSonuclarModel = new JSONModel({
				enabled: false
			});
			var oKararlarModel = new JSONModel({
				enabled: false
			});
			var oHatalarModel = new JSONModel({
				enabled: false
			});

			var SelectItemModel = new JSONModel({
				objectId: null,
				Inspoper: null
			});

			this.setModel(oSonucGModel, "oSonucG");
			this.setModel(oKulKarModel, "oKulKar");
			this.setModel(oHataGModel, "oHataG");
			this.setModel(oSonuclarModel, "oSonuclar");
			this.setModel(oKararlarModel, "oKararlar");
			this.setModel(oHatalarModel, "oHatalar");

			this.setModel(SelectItemModel, "oObjectId");

			this.getRouter().getRoute("krdetaygoruntule").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);

		},

		KRDetayGoruntuleme: function(oEvent) {

			//debugger;
			//var value = oEvent.oSource.getSelectedItem().getBindingContext().getPath();
			var InspoperM = sap.ui.getCore().getModel("EtInspoperModel");
			var InsM = {};
			InsM.InspoperCollection = InspoperM.getData();
			var InspM = new JSONModel(InsM);
			this.getView().setModel(InspM, "InspoperModel");

			var len = InspoperM.getData().length;

			if (len === 0) {
				sap.m.MessageToast.show("Operasyon Verisi Yok");
			} else if (len === 1) {

				//debugger;
				//	var bReplace = !Device.system.phone;
				var IM = this.getModel("InspoperModel");
				var Prueflos = this.getModel("oObjectId").getData().objectId;
				var Inspoper = IM.getData().InspoperCollection[0].Inspoper;
				var bReplace = !Device.system.phone;

				this.getRouter().navTo("krdetaysonuc", {
					objectId: Prueflos,
					Inspoper: Inspoper
				}, bReplace);

			} else if (len > 1) {

				this._getDialogKRInspoper().open();

			}

			//		this.getOwnerComponent().getRouter().navTo("krdetaysonuc",true);

		},

		_getDialogKRInspoper: function(list) {
			//debugger;
			if (!this._oDialogKRInspoper) {
				this._oDialogKRInspoper = sap.ui.xmlfragment("com.nauticana.demo.view.DialogKRInspoper", this, true);
				this._oDialogKRInspoper.setModel(this.getView().getModel("i18n"), "i18n");
				var IM = this.getModel("InspoperModel");
				this._oDialogKRInspoper.setModel(IM);
			}
			this.getView().addDependent(this._oDialogKRInspoper);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogKRInspoper);

			return this._oDialogKRInspoper;
		},

		handleKRClose: function(oEvent) {
			//debugger;

			var selectedItemObject = oEvent.getParameter("selectedContexts")[0].sPath;

			var insplot = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Insplot;
			var Inspoper = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Inspoper;

			var bReplace = !Device.system.phone;

			this.getRouter().navTo("krdetaysonuc", {
				objectId: insplot,
				Inspoper: Inspoper
			}, bReplace);

			// this._oDialogKRInspoper.destroy();
		},

		onBeforeRendering: function(oEvent) {

			//	var Zkkmodel =	sap.ui.getCore().getModel("ZkkModel");
			//    var ZkkData = Zkkmodel.getData();

			var userDataModel = sap.ui.getCore().getModel("UserAuthModel");

			var userData = userDataModel.getData();

			var oSonucGModel = this.getView().getModel("oSonucG");
			var oKulKarModel = this.getView().getModel("oKulKar");
			var oHataGModel = this.getView().getModel("oHataG");
			var oSonuclarModel = this.getView().getModel("oSonuclar");
			var oKararlarModel = this.getView().getModel("oKararlar");
			var oHatalarModel = this.getView().getModel("oHatalar");

			/*
						Z_EDIT_KNC	Sonuç Girişi	mhd-icontabbar-f1
						Z_EDIT_KK	Kullanım Kararı mhd-icontabbar-f5
						Z_EDIT_DEFT	Hata Girişi 	mhd-icontabbar-f3
						Z_DISP_KNC	Sonuçlar    	mhd-icontabbar-f2
						Z_DISP_KK	Kararlar        mhd-icontabbar-f6
						Z_DISP_DEFT	Hatalar 		mhd-icontabbar-f4    
						*/

			if (userData.ZEditKnc === 'X') {
				oSonucGModel.setData("true");
			}
			if (userData.ZEditKk === 'X') {
				oKulKarModel.setData("true");
			}
			if (userData.ZEditDeft === 'X') {
				oHataGModel.setData("true");
			}
			if (userData.ZDispKnc === 'X') {
				oSonuclarModel.setData("true");
			}
			if (userData.ZDispKk === 'X') {
				oKararlarModel.setData("true");
			}
			if (userData.ZDispDeft === 'X') {
				oHatalarModel.setData("true");
			}
			//			if(ZkkData.Zkk ==='X'){
			//			oSonucGModel.setData("false");
			//				sap.m.MessageToast.show(" Kullanım Kararı Girilmiş, Sonuç Girişi Yapılamaz");
			//			}

		},
		_onRouteMatched: function() {

			/*	var oModel =	this.getOwnerComponent().getModel();
			//debugger;
    		oModel.refresh(true);*/
		},

		_onObjectMatched: function(oEvent) {

			var sObjectId = oEvent.getParameter("arguments").objectId;
			this.getModel().metadataLoaded().then(function() {
				var sObjectPath = this.getModel().createKey("EtQalsSet", {
					Prueflos: sObjectId
				});
				this._bindView("/" + sObjectPath);
			}.bind(this));

		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * After list data is available, this handler method updates the
		 * master list counter and hides the pull to refresh control, if
		 * necessary.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the master list object counter after new data is loaded
			this._updateListItemCount(oEvent.getParameter("total"));
			// hide pull to refresh if necessary
			this.byId("pullToRefresh").hide();

		},

		/**
		 * Event handler for the master search field. Applies current
		 * filter value and triggers a new search. If the search field's
		 * 'refresh' button has been pressed, no new search is triggered
		 * and the list binding is refresh instead.
		 * @param {sap.ui.base.Event} oEvent the search event
		 * @public
		 */
		onSearch: function(oEvent) {

			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
				return;
			}

			var sQuery = oEvent.getParameter("query");

			var aFilters = [];

			if (sQuery && sQuery.length > 0) {
				aFilters.push(new Filter("Matnr", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Losmenge", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Mengeneinh", FilterOperator.Contains, sQuery));
				var oFilter = new Filter({
					filters: aFilters,
					and: false
				}); // OR filter 

				this._oListFilterState = oFilter;

			} else {
				oFilter = null;
				this._oListFilterState = oFilter;
			}
			// Losmenge
			// Mengeneinh

			/*	if (sQuery) {
					
					
					this._oListFilterState.aSearch = [new Filter("Matnr", FilterOperator.Contains, sQuery),new Filter("Losmenge", FilterOperator.Contains, sQuery),new Filter("Mengeneinh", FilterOperator.Contains, sQuery)];
				} else {
					this._oListFilterState.aSearch = [];
				}*/

			// update list binding
			var oBinding = this._oTable.getBinding("items");
			oBinding.filter(oFilter, "Application");
			this._applyFilterSearch();

		},

		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function() {
			this._oList.getBinding("items").refresh();
		},

		/**
		 * Event handler for the sorter selection.
		 * @param {sap.ui.base.Event} oEvent the select event
		 * @public
		 */
		onSort: function(oEvent) {
			var sKey = oEvent.getSource().getSelectedItem().getKey(),
				aSorters = this._oGroupSortState.sort(sKey);

			this._applyGroupSort(aSorters);
		},

		/**
		 * Event handler for the grouper selection.
		 * @param {sap.ui.base.Event} oEvent the search field event
		 * @public
		 */
		onGroup: function(oEvent) {
			var sKey = oEvent.getSource().getSelectedItem().getKey(),
				aSorters = this._oGroupSortState.group(sKey);

			this._applyGroupSort(aSorters);
		},

		/**
		 * Event handler for the filter button to open the ViewSettingsDialog.
		 * which is used to add or remove filters to the master list. This
		 * handler method is also called when the filter bar is pressed,
		 * which is added to the beginning of the master list when a filter is applied.
		 * @public
		 */
		onOpenViewSettings: function() {
			if (!this._oViewSettingsDialog) {
				this._oViewSettingsDialog = sap.ui.xmlfragment("com.nauticana.demo.view.ViewSettingsDialog", this);
				this._oViewSettingsDialog.setModel(this.getView().getModel("i18n"), "i18n");
				this.getView().addDependent(this._oViewSettingsDialog);
				// forward compact/cozy style into Dialog
				this._oViewSettingsDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			this._oViewSettingsDialog.open();
		},

		/**
		 * Event handler called when ViewSettingsDialog has been confirmed, i.e.
		 * has been closed with 'OK'. In the case, the currently chosen filters
		 * are applied to the master list, which can also mean that the currently
		 * applied filters are removed from the master list, in case the filter
		 * settings are removed in the ViewSettingsDialog.
		 * @param {sap.ui.base.Event} oEvent the confirm event
		 * @public
		 */
		onConfirmViewSettingsDialog: function(oEvent) {
			var aFilterItems = oEvent.getParameters().filterItems,
				aFilters = [],
				aCaptions = [];

			// update filter state:
			// combine the filter array and the filter string
			aFilterItems.forEach(function(oItem) {
				switch (oItem.getKey()) {
					case "Filter1":
						aFilters.push(new Filter("Losmenge", FilterOperator.LE, 100));
						break;
					case "Filter2":
						aFilters.push(new Filter("Losmenge", FilterOperator.GT, 100));
						break;
					default:
						break;
				}
				aCaptions.push(oItem.getText());
			});

			this._oListFilterState.aFilter = aFilters;
			this._updateFilterBar(aCaptions.join(", "));
			this._applyFilterSearch();
		},

		/**
		 * Event handler for the list selection event
		 * @param {sap.ui.base.Event} oEvent the list selectionChange event
		 * @public
		 */
		onSelectionChange: function(oEvent) {
			//debugger;

			var oSelect = oEvent.getSource();

			var selectedItemObject = oSelect.getSelectedItem().getBindingContext().getObject();

			var Prueflos = selectedItemObject.Prueflos;
			var Art = selectedItemObject.Art;

			this.getView().getModel("oObjectId").setData({
				objectId: Prueflos,
				Art: Art
			});

			var ofilter = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];

			var sPath = "/EtInspoperSet";

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");

			//debugger;

			oModel.read(sPath, {
				filters: ofilter,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtInspoperModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter2 = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];
			var sPath2 = "/EtZinsCharSet";

			oModel.read(sPath2, {
				filters: ofilter2,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtZinsCharModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter3 = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];
			var sPath3 = "/EtSingleResultsSet";

			oModel.read(sPath3, {
				filters: ofilter3,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtSingleResultsModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter4 = [new Filter("IvUyrlmTp", FilterOperator.EQ, "3")];

			var sPath4 = "/EtZqualitaCatSet";

			oModel.read(sPath4, {
				filters: ofilter4,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtZqualitaCatModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			//debugger;

			oModel.attachRequestSent(function() {
				sap.ui.core.BusyIndicator.show(10);
			});
			oModel.attachRequestCompleted(function() {
				sap.ui.core.BusyIndicator.hide();
			});

		},

		/**
		 * Event handler for the bypassed event, which is fired when no routing pattern matched.
		 * If there was an object selected in the master list, that selection is removed.
		 * @public
		 */
		onBypassed: function() {
			this._oList.removeSelections(true);
		},

		/**
		 * Used to create GroupHeaders with non-capitalized caption.
		 * These headers are inserted into the master list to
		 * group the master list's items.
		 * @param {Object} oGroup group whose text is to be displayed
		 * @public
		 * @returns {sap.m.GroupHeaderListItem} group header with non-capitalized caption.
		 */
		createGroupHeader: function(oGroup) {
			return new GroupHeaderListItem({
				title: oGroup.text,
				upperCase: false
			});
		},

		/**
		 * Event handler for navigating back.
		 * It there is a history entry or an previous app-to-app navigation we go one step back in the browser history
		 * If not, it will navigate to the shell home
		 * @public
		 */
		onNavBack: function() {

			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				history.go(-1);
			} else {
				this.getRouter().navTo("homepage", {}, true);
			}

		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		_createViewModel: function() {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "Matnr",
				groupBy: "None"
			});
		},

		/**
		 * If the master route was hit (empty hash) we have to set
		 * the hash to to the first item in the list as soon as the
		 * listLoading is done and the first item in the list is known
		 * @private
		 */
		_onMasterMatched: function() {

			//debugger;

			var sValDate = this.getModel("mFilters").getData().Idate;
			var sValFirstDate = this.getModel("mFilters").getData().firstDate;
			var sValSecondDate = this.getModel("mFilters").getData().secondDate;
			var sValStat = this.getModel("mFilters").getData().Stat35;

			var sFilterMatnr = this.getModel("mFilters").getData().Matnr;
			var sFilterWerk = this.getModel("mFilters").getData().Werk;
			var sFilterCharg = this.getModel("mFilters").getData().Charg;
			var sFilterArt = this.getModel("mFilters").getData().Art;
			var sFilterLagort = this.getModel("mFilters").getData().Lagortchrg;

			var sIvTip = "1";
			var sPathIvTip = "IvTip";
			var sPathDate = "Idate";
			var sPathStat = "Stat35";

			if (sValDate === "5") {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.BT, sValFirstDate, sValSecondDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});
			} else {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.EQ, sValDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});

			}

			var binding = this.byId("list").getBinding("items");
			binding.filter(oDataFilter, true);

		},

		/**
		 * Shows the selected item on the detail page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showDetail: function(oItem) {
			var bReplace = !Device.system.phone;

			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Prueflos")
			}, bReplace);
		},

		/**
		 * Sets the item count on the master list header
		 * @param {integer} iTotalItems the total number of items in the list
		 * @private
		 */
		_updateListItemCount: function(iTotalItems) {
			var sTitle;
			// only update the counter if the length is final
			if (this._oList.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
				this.getModel("masterView").setProperty("/title", sTitle);
			}
		},

		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @private
		 */
		_applyFilterSearch: function() {

			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("masterView");
			this._oList.getBinding("items").filter(aFilters, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
			} else if (this._oListFilterState.aSearch.length > 0) {
				// only reset the no data text to default when no new search was triggered
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},

		/**
		 * Internal helper method to apply both group and sort state together on the list binding
		 * @param {sap.ui.model.Sorter[]} aSorters an array of sorters
		 * @private
		 */
		_applyGroupSort: function(aSorters) {
			this._oList.getBinding("items").sort(aSorters);
		},

		/**
		 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
		 * @param {string} sFilterBarText the selected filter value
		 * @private
		 */
		_updateFilterBar: function(sFilterBarText) {
			var oViewModel = this.getModel("masterView");
			oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
			oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
		}

	});

});