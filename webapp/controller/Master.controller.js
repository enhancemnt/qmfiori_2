/*global history */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/util/Util",
	"sap/ui/table/Column"
], function(BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState,Column,Util) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.Master", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/*
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function(oEvent) { 
			//  filtering  
			this._oView = this.getView();
			
			
			var fragmentId = this.getView().createId("idTableFragment");
			this.oTable = sap.ui.core.Fragment.byId(fragmentId, "idTable");

			// Control state model
			var oList = this.byId("list"),
				oViewModel = this._createViewModel(),
				// Put down master list's original value for busy indicator delay,
				// so it can be restored later on. Busy handling on the master list is
				// taken care of by the master list itself.
				iOriginalBusyDelay = oList.getBusyIndicatorDelay();

			this._oGroupSortState = new GroupSortState(oViewModel, grouper.groupUnitNumber(this.getResourceBundle()));

			this._oList = oList;
			BaseController.oList = oList; 
			// keeps the filter and search state
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};

			this.setModel(oViewModel, "masterView");
			// Make sure, busy indication is showing immediately so there is no
			// break after the busy indication for loading the view's meta data is
			// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
			oList.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for the list
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});

			var oSonucGModel = new JSONModel({
				enabled: false,
				Zkk: null
			});
			var oKulKarModel = new JSONModel({
				enabled: false
			});
			var oHataGModel = new JSONModel({
				enabled: false
			});
			var oSonuclarModel = new JSONModel({
				enabled: false
			});
			var oKararlarModel = new JSONModel({
				enabled: false
			});
			var oHatalarModel = new JSONModel({
				enabled: false
			});

			var SelectItemModel = new JSONModel({
				objectId: null,
				Inspoper: null
			});

			var ZKKM = new JSONModel({
				Zkk: null,
				Stat: null
			});

			/*
			Z_EDIT_KNC	Sonuç Girişi	mhd-icontabbar-f1
			Z_EDIT_KK	Kullanım Kararı mhd-icontabbar-f5
			Z_EDIT_DEFT	Hata Girişi 	mhd-icontabbar-f3
			Z_DISP_KNC	Sonuçlar    	mhd-icontabbar-f2
			Z_DISP_KK	Kararlar        mhd-icontabbar-f6
			Z_DISP_DEFT	Hatalar 		mhd-icontabbar-f4    
			*/
			this.setModel(oSonucGModel, "oSonucG");
			this.setModel(oKulKarModel, "oKulKar");
			this.setModel(oHataGModel, "oHataG");
			this.setModel(oSonuclarModel, "oSonuclar");
			this.setModel(oKararlarModel, "oKararlar");
			this.setModel(oHatalarModel, "oHatalar");

			this.setModel(SelectItemModel, "oObjectId");
			this.setModel(ZKKM, "ZKKM");

			/*	this.getView().addEventDelegate({
					onBeforeFirstShow: function () {
						this.getOwnerComponent().oListSelector.setBoundMasterList(oList);
					}.bind(this)
				});*/

			this.setColumnsModel();
			this.readTableCustom();
			
			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);

		},
		KPSonucGiris: function(oEvent) {
			// debugger;
			//var value = oEvent.oSource.getSelectedItem().getBindingContext().getPath();
			var Zkk = this.getModel("ZKKM").getData().Zkk;
			var InspoperM = sap.ui.getCore().getModel("EtInspoperModel");
			if (Zkk === 'X') {
				sap.m.MessageToast.show("Kullanım Kararı Girilmiş, Sonuç Girişi Yapılamaz", {
					duration: 3000
				});
			}

			if (InspoperM && Zkk !== 'X') {

				var InsM = {};
				InsM.InspoperCollection = InspoperM.getData();
				var InspM = new JSONModel(InsM);
				this.getView().setModel(InspM, "InspoperModel");

				var len = InspoperM.getData().length;

				if (len === 0) {
					sap.m.MessageToast.show("Operasyon Verisi Yok");
				} else if (len === 1) {

					//debugger;
					//	var bReplace = !Device.system.phone;
					var IM = this.getModel("InspoperModel");
					var Prueflos = this.getModel("oObjectId").getData().objectId;
					var Inspoper = IM.getData().InspoperCollection[0].Inspoper;
					var bReplace = !Device.system.phone;

					this.getRouter().navTo("kpsonucgiris", {
						objectId: Prueflos,
						Inspoper: Inspoper
					}, bReplace);

				} else if (len > 1) {

					this._getDialogInspoper().open();

				}

			}

		},
		handleConfirmSonuc: function(oEvent) {
			//debugger;

			var selectedItemObject = oEvent.getParameter("selectedContexts")[0].sPath;
			var insplot = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Insplot;
			var Inspoper = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Inspoper;

			var bReplace = !Device.system.phone;

			this.getRouter().navTo("kpsonuclar", {
				objectId: insplot,
				Inspoper: Inspoper
			}, bReplace);
			//	this._oDialogInspoperSonuc.close();
		},
		handleClose: function(oEvent) {
			//debugger;

			var selectedItemObject = oEvent.getParameter("selectedContexts")[0].sPath
			var insplot = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Insplot;
			var Inspoper = oEvent.getParameter("selectedContexts")[0].getModel().getProperty(selectedItemObject).Inspoper;

			var bReplace = !Device.system.phone;

			this.getRouter().navTo("kpsonucgiris", {
				objectId: insplot,
				Inspoper: Inspoper
			}, bReplace);

			//	this._oDialogInspoper.close();
		},
		_getDialogInspoper: function(list) {
			//debugger;
			if (!this._oDialogInspoper) {
				this._oDialogInspoper = sap.ui.xmlfragment("com.nauticana.demo.view.DialogInspoper", this, true);
				this._oDialogInspoper.setModel(this.getView().getModel("i18n"), "i18n");
			}
			var IM = this.getModel("InspoperModel");
			this._oDialogInspoper.setModel(IM);

			this.getView().addDependent(this._oDialogInspoper);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogInspoper);

			return this._oDialogInspoper;
		},
		_getDialogInspoperSonuc: function(list) {
			//debugger;
			if (!this._oDialogInspoperSonuc) {
				this._oDialogInspoperSonuc = sap.ui.xmlfragment("com.nauticana.demo.view.DialogInspoperSonuc", this, true);
				this._oDialogInspoperSonuc.setModel(this.getView().getModel("i18n"), "i18n");
			}
			var IM = this.getModel("InspoperModel");
			this._oDialogInspoperSonuc.setModel(IM);

			this.getView().addDependent(this._oDialogInspoperSonuc);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogInspoperSonuc);

			return this._oDialogInspoperSonuc;
		},
		KPSonuclar: function(oEvent) {
			//debugger;	 

			//var value = oEvent.oSource.getSelectedItem().getBindingContext().getPath();
			var InspoperM = sap.ui.getCore().getModel("EtInspoperModel");
			if (InspoperM) {
				var InsM = {};
				InsM.InspoperCollection = InspoperM.getData();
				var InspM = new JSONModel(InsM);
				this.getView().setModel(InspM, "InspoperModel");

				var len = InspoperM.getData().length;

				if (len === 0) {
					sap.m.MessageToast.show("Operasyon Verisi Yok");
				} else if (len === 1) {

					//debugger;
					//	var bReplace = !Device.system.phone;
					var IM = this.getModel("InspoperModel");
					var Prueflos = this.getModel("oObjectId").getData().objectId;
					var Inspoper = IM.getData().InspoperCollection[0].Inspoper;
					var bReplace = !Device.system.phone;

					this.getRouter().navTo("kpsonuclar", {
						objectId: Prueflos,
						Inspoper: Inspoper
					}, bReplace);

				} else if (len > 1) {

					this._getDialogInspoperSonuc().open();

				}

			}

		},

		KPHataGiris: function(oEvent) {
			//debugger;

			var isSel = sap.ui.getCore().getModel("EtInspoperModel");
			if (isSel) {

				var Prueflos = this.getModel("oObjectId").getData().objectId;
				var bReplace = !Device.system.phone;
				this.getRouter().navTo("kphatagiris", {
					objectId: Prueflos
				}, bReplace);
			}

		},

		KPHatalar: function(oEvent) {
			var isSel = sap.ui.getCore().getModel("EtInspoperModel");
			if (isSel) {

				this.getRouter().navTo("kphatalar");

			}
		},

		KPKararGiris: function(oEvent) {
			//debugger;

			var Stat = this.getModel("ZKKM").getData().Stat;

			if (Stat === 'X') {
				sap.m.MessageToast.show("Karar Kararı Girilmiş, Karar Girişi Yapılamaz", {
					duration: 3000
				});
			}

			var isSel = sap.ui.getCore().getModel("EtInspoperModel");
			if (isSel && Stat !== 'X') {
				var Prueflos = this.getModel("oObjectId").getData().objectId;

				var bReplace = !Device.system.phone;
				this.getRouter().navTo("kpkarargiris", {
					objectId: Prueflos,
					art:this.art,
					werks:this.werks
				}, bReplace);
			}
		},

		KPKararlar: function(oEvent) {
			var isSel = sap.ui.getCore().getModel("EtInspoperModel");
			if (isSel) {
				this.getRouter().navTo("kpkararlar");

			}
		},
		onBeforeRendering: function(oEvent) {

			//	var Zkkmodel =	sap.ui.getCore().getModel("ZkkModel");
			//    var ZkkData = Zkkmodel.getData();

			var userDataModel = sap.ui.getCore().getModel("UserAuthModel");

			var userData = userDataModel.getData();

			var oSonucGModel = this.getView().getModel("oSonucG");
			var oKulKarModel = this.getView().getModel("oKulKar");
			var oHataGModel = this.getView().getModel("oHataG");
			var oSonuclarModel = this.getView().getModel("oSonuclar");
			var oKararlarModel = this.getView().getModel("oKararlar");
			var oHatalarModel = this.getView().getModel("oHatalar");

			/*
						Z_EDIT_KNC	Sonuç Girişi	mhd-icontabbar-f1
						Z_EDIT_KK	Kullanım Kararı mhd-icontabbar-f5
						Z_EDIT_DEFT	Hata Girişi 	mhd-icontabbar-f3
						Z_DISP_KNC	Sonuçlar    	mhd-icontabbar-f2
						Z_DISP_KK	Kararlar        mhd-icontabbar-f6
						Z_DISP_DEFT	Hatalar 		mhd-icontabbar-f4    
						*/

			if (userData.ZEditKnc === 'X') {
				oSonucGModel.setData("true");
			}
			if (userData.ZEditKk === 'X') {
				oKulKarModel.setData("true");
			}
			if (userData.ZEditDeft === 'X') {
				oHataGModel.setData("true");
			}
			if (userData.ZDispKnc === 'X') {
				oSonuclarModel.setData("true");
			}
			if (userData.ZDispKk === 'X') {
				oKararlarModel.setData("true");
			}
			if (userData.ZDispDeft === 'X') {
				oHatalarModel.setData("true");
			}
			//			if(ZkkData.Zkk ==='X'){
			//			oSonucGModel.setData("false");
			//				sap.m.MessageToast.show(" Kullanım Kararı Girilmiş, Sonuç Girişi Yapılamaz");
			//			}

		},
		_onRouteMatched: function() {

			/*	var oModel =	this.getOwnerComponent().getModel();
			//debugger;
    		oModel.refresh(true);*/
		},

		_onObjectMatched: function(oEvent) {

			var sObjectId = oEvent.getParameter("arguments").objectId;
			this.getModel().metadataLoaded().then(function() {
				var sObjectPath = this.getModel().createKey("EtQalsSet", {
					Prueflos: sObjectId
				});
				this._bindView("/" + sObjectPath);
			}.bind(this));

		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * After list data is available, this handler method updates the
		 * master list counter and hides the pull to refresh control, if
		 * necessary.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the master list object counter after new data is loaded
			this._updateListItemCount(oEvent.getParameter("total"));
			// hide pull to refresh if necessary
			// this.byId("pullToRefresh").hide();

		},

		/**
		 * Event handler for the master search field. Applies current
		 * filter value and triggers a new search. If the search field's
		 * 'refresh' button has been pressed, no new search is triggered
		 * and the list binding is refresh instead.
		 * @param {sap.ui.base.Event} oEvent the search event
		 * @public
		 */
		onSearch: function(oEvent) {

			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
				return;
			}

			var sQuery = oEvent.getParameter("query");

			var aFilters = [];

			if (sQuery && sQuery.length > 0) {
				aFilters.push(new Filter("Matnr", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Losmenge", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Mengeneinh", FilterOperator.Contains, sQuery));
				var oFilter = new Filter({
					filters: aFilters,
					and: false
				}); // OR filter 

				this._oListFilterState = oFilter;

			} else {
				oFilter = null;
				this._oListFilterState = oFilter;
			}
			// Losmenge
			// Mengeneinh

			/*	if (sQuery) {
					
					
					this._oListFilterState.aSearch = [new Filter("Matnr", FilterOperator.Contains, sQuery),new Filter("Losmenge", FilterOperator.Contains, sQuery),new Filter("Mengeneinh", FilterOperator.Contains, sQuery)];
				} else {
					this._oListFilterState.aSearch = [];
				}*/

			// update list binding
			var oBinding = this._oTable.getBinding("items");
			oBinding.filter(oFilter, "Application");
			this._applyFilterSearch();

		},

		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function() {
			this._oList.getBinding("items").refresh();
		},

		/**
		 * Event handler for the sorter selection.
		 * @param {sap.ui.base.Event} oEvent the select event
		 * @public
		 */
		onSort: function(oEvent) {
			var sKey = oEvent.getSource().getSelectedItem().getKey(),
				aSorters = this._oGroupSortState.sort(sKey);

			this._applyGroupSort(aSorters);
		},

		/**
		 * Event handler for the grouper selection.
		 * @param {sap.ui.base.Event} oEvent the search field event
		 * @public
		 */
		onGroup: function(oEvent) {
			var sKey = oEvent.getSource().getSelectedItem().getKey(),
				aSorters = this._oGroupSortState.group(sKey);

			this._applyGroupSort(aSorters);
		},

		/**
		 * Event handler for the filter button to open the ViewSettingsDialog.
		 * which is used to add or remove filters to the master list. This
		 * handler method is also called when the filter bar is pressed,
		 * which is added to the beginning of the master list when a filter is applied.
		 * @public
		 */
		onOpenViewSettings: function() {
			if (!this._oViewSettingsDialog) {
				this._oViewSettingsDialog = sap.ui.xmlfragment("com.nauticana.demo.view.ViewSettingsDialog", this);
				this._oViewSettingsDialog.setModel(this.getView().getModel("i18n"), "i18n");
				this.getView().addDependent(this._oViewSettingsDialog);
				// forward compact/cozy style into Dialog
				this._oViewSettingsDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			this._oViewSettingsDialog.open();
		},

		/**
		 * Event handler called when ViewSettingsDialog has been confirmed, i.e.
		 * has been closed with 'OK'. In the case, the currently chosen filters
		 * are applied to the master list, which can also mean that the currently
		 * applied filters are removed from the master list, in case the filter
		 * settings are removed in the ViewSettingsDialog.
		 * @param {sap.ui.base.Event} oEvent the confirm event
		 * @public
		 */
		onConfirmViewSettingsDialog: function(oEvent) {
			var aFilterItems = oEvent.getParameters().filterItems,
				aFilters = [],
				aCaptions = [];

			// update filter state:
			// combine the filter array and the filter string
			aFilterItems.forEach(function(oItem) {
				switch (oItem.getKey()) {
					case "Filter1":
						aFilters.push(new Filter("Losmenge", FilterOperator.LE, 100));
						break;
					case "Filter2":
						aFilters.push(new Filter("Losmenge", FilterOperator.GT, 100));
						break;
					default:
						break;
				}
				aCaptions.push(oItem.getText());
			});

			this._oListFilterState.aFilter = aFilters;
			this._updateFilterBar(aCaptions.join(", "));
			this._applyFilterSearch();
		},

		/**
		 * Event handler for the list selection event
		 * @param {sap.ui.base.Event} oEvent the list selectionChange event
		 * @public
		 */
		// onSelectionChange: function(oEvent) {
		// 	//debugger;
		// 	//var value = oEvent.oSource.getSelectedItem().getBindingContext().getPath();
		// 	var oSelect = oEvent.getSource();
			
		// 	var llist = BaseController.oList;

			// var selectedItemObject = oSelect.getSelectedItem().getBindingContext().getObject();

		onSelectionChange: function(selectedItemObject) {
			var Prueflos = selectedItemObject.Prueflos;
			var Art = selectedItemObject.Art;
			this.art = Art;
			this.werks = selectedItemObject.Werk;
			var Zkk = selectedItemObject.Zkk;
			var Stat = selectedItemObject.Stat35;
			//	var Inspoper = selectedItemObject.Inspoper;

			this.getView().getModel("ZKKM").setData({
				Zkk: Zkk,
				Stat: Stat

			});

			this.getView().getModel("oObjectId").setData({
				objectId: Prueflos
					//	Inspoper: Inspoper
			});
			//	var Zkk = "V";

			//		var ofilter =  [new Filter("Insplot", FilterOperator.EQ, Prueflos),new Filter("Zkk", FilterOperator.EQ, Zkk)];
			var ofilter = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];
			//	var ofilters = "?$filter=Insplot"+"%20eq%20%27"+Prueflos+"%27";
			//	var oModel = this.getView().getModel(); 

			// var sPath = "/EtZkkSet('"+ Prueflos +"')"; // here I am able to see the selected Line information

			var sPath = "/EtInspoperSet";
			// "/EtInspoperSet"+"?$filter=Insplot"+"%20eq%20%27"+Prueflos+"%27"
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");

			//debugger;
			//
			//  

			//	var oJsonModel = new sap.ui.model.json.JSONModel();
			// sap.ui.getCore().setModel(oJsonModel,"EtInspoperModel");

			oModel.read(sPath, {
				filters: ofilter,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;
					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtInspoperModel");
					console.log(jsonArray);
				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter2 = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];
			var sPath2 = "/EtZinsCharSet";
			//   var oJsonModel2 = new sap.ui.model.json.JSONModel();
			//  sap.ui.getCore().setModel(oJsonModel2,"EtZinsCharModel");
			oModel.read(sPath2, {
				filters: ofilter2,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtZinsCharModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter3 = [new Filter("Insplot", FilterOperator.EQ, Prueflos)];
			var sPath3 = "/EtSingleResultsSet";
			//   var oJsonModel3 = new sap.ui.model.json.JSONModel();
			//   sap.ui.getCore().setModel(oJsonModel3,"EtSingleResultsModel");
			oModel.read(sPath3, {
				filters: ofilter3,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtSingleResultsModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter4 = [new Filter("IvUyrlmTp", FilterOperator.EQ, "3")];

			var sPath4 = "/EtZqualitaCatSet";

			oModel.read(sPath4, {
				filters: ofilter4,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtZqualitaCatModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter5 = [new Filter("Prueflos", FilterOperator.EQ, Prueflos), new Filter("IvTip", FilterOperator.EQ, "1")];

			var sPath5 = "/EtQalsSet";

			oModel.read(sPath5, {
				filters: ofilter5,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtQalsSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter6 = [new Filter("IvUyrlmTp", FilterOperator.EQ, "2")];

			var sPath6 = "/EtZusageDecisionSet";

			oModel.read(sPath6, {
				filters: ofilter6,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtZusageDecisionSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter7 = [new Filter("Prueflos", FilterOperator.EQ, Prueflos)];
			var sPath7 = "/EtUsageDecisionSet";

			oModel.read(sPath7, {
				filters: ofilter7,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtusageDecisionSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter8 = [new Filter("Prueflos", FilterOperator.EQ, Prueflos)];

			var sPath8 = "/EtUsageDecTextSet";

			oModel.read(sPath8, {
				filters: ofilter8,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "EtUsageDecTextSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			//	var Art = this.getView().getModel("ArtModel").getData().Art;

			var Uname = sap.ui.getCore().getModel("UserAuthModel").getData().Uname;

			var ofilter9 = [new Filter("IvUname", FilterOperator.EQ, Uname), new Filter("Art", FilterOperator.EQ, Art)];

			var sPath9 = "/NotifiCatDetlSet";

			oModel.read(sPath9, {
				filters: ofilter9,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "NotifiCatDetlSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			var ofilter10 = [new Filter("IvUname", FilterOperator.EQ, Uname)];

			var sPath10 = "/NotifiCatHeadSet";

			oModel.read(sPath10, {
				filters: ofilter10,
				success: function(oData, response) {
					var jsonArray = [];
					jsonArray = response.data.results;

					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(jsonArray);
					sap.ui.getCore().setModel(oJSONModel, "NotifiCatHeadSetModel");

					console.log(jsonArray);

				},
				error: function(oError) {
					console.log("Error" + oError.responseText)
				}
			});

			//var ofilter11=  [new Filter("IvUname", FilterOperator.EQ, Uname)];
			/*

			var sPath11 = "/EtZinsCharSet('"+Prueflos+"')";
					 
    			oModel.read(sPath11, { 
    			//	filters: ofilter11,
    				urlParameters: {
        				"$expand": "EtSingleResults"
					},
					success : function(oData,response)
				{
					//	var jsonArray = [];
					//	 jsonArray = response.data.results;
						 
						 var oJSONModel = new sap.ui.model.json.JSONModel();
						 oJSONModel.setData(oData);
						sap.ui.getCore().setModel(oJSONModel,"CharSingleFullSetModel");
						console.log(oData);
			            console.log(response);
            		    console.log(oJSONModel);
    					}, 
					error : function(oError){ 
				    	console.log("Error" +oError.responseText) 
				    }
    			});	*/

			oModel.attachRequestSent(function() {
				sap.ui.core.BusyIndicator.show(10);
			});
			oModel.attachRequestCompleted(function() {
				sap.ui.core.BusyIndicator.hide();
			});

			if (Zkk === 'X') {
				sap.m.MessageToast.show("Kullanım Kararı Girilmiş, Sonuç Girişi Yapılamaz", {
					duration: 3000
				});
				var oSonucGModel = this.getView().getModel("oSonucG");
				//	sap.ui.getCore().getModel("UIModel").setProperty(tableIsBusy, true);
				oSonucGModel.setData("false");

			} else {
				// get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
				//		this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());

				//		this._getData();
			}

		},
		_getData: function(oEvent) {

		},
		/**
		 * Event handler for the bypassed event, which is fired when no routing pattern matched.
		 * If there was an object selected in the master list, that selection is removed.
		 * @public
		 */
		onBypassed: function() {
			this._oList.removeSelections(true);
		},

		/**
		 * Used to create GroupHeaders with non-capitalized caption.
		 * These headers are inserted into the master list to
		 * group the master list's items.
		 * @param {Object} oGroup group whose text is to be displayed
		 * @public
		 * @returns {sap.m.GroupHeaderListItem} group header with non-capitalized caption.
		 */
		createGroupHeader: function(oGroup) {
			return new GroupHeaderListItem({
				title: oGroup.text,
				upperCase: false
			});
		},

		/**
		 * Event handler for navigating back.
		 * It there is a history entry or an previous app-to-app navigation we go one step back in the browser history
		 * If not, it will navigate to the shell home
		 * @public
		 */
		onNavBack: function() {

			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				history.go(-1);
			} else {
				this.getRouter().navTo("homepage", {}, true);
			}

		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		_createViewModel: function() {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "Matnr",
				groupBy: "None"
			});
		},

		/**
		 * If the master route was hit (empty hash) we have to set
		 * the hash to to the first item in the list as soon as the
		 * listLoading is done and the first item in the list is known
		 * @private
		 */
		_onMasterMatched: function() {

			//////////////////////////

			//debugger;

			var sValDate = this.getModel("mFilters").getData().Idate;
			var sValFirstDate = this.getModel("mFilters").getData().firstDate;
			var sValSecondDate = this.getModel("mFilters").getData().secondDate;
			var sValStat = this.getModel("mFilters").getData().Stat35;

			var sFilterMatnr = this.getModel("mFilters").getData().Matnr;
			var sFilterWerk = this.getModel("mFilters").getData().Werk;
			var sFilterCharg = this.getModel("mFilters").getData().Charg;
			var sFilterArt = this.getModel("mFilters").getData().Art;
			var sFilterLagort = this.getModel("mFilters").getData().Lagortchrg;

			var sIvTip = "1";
			var sPathIvTip = "IvTip";
			var sPathDate = "Idate";
			var sPathStat = "Stat35";

			if (sValDate === "5") {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.BT, sValFirstDate, sValSecondDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});
			} else {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.EQ, sValDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});

			}

			var binding = this.byId("list").getBinding("items");
			binding.filter(oDataFilter, true);
			
			var binding2 = this.oTable.getBinding("rows");
			binding2.filter(oDataFilter, true);

		},

		/**
		 * Shows the selected item on the detail page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showDetail: function(oItem) {
			var bReplace = !Device.system.phone;

			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Prueflos")
			}, bReplace);
		},

		/**
		 * Sets the item count on the master list header
		 * @param {integer} iTotalItems the total number of items in the list
		 * @private
		 */
		_updateListItemCount: function(iTotalItems) {
			var sTitle;
			// only update the counter if the length is final
			if (this._oList.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
				this.getModel("masterView").setProperty("/title", sTitle);
			}
		},

		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @private
		 */
		_applyFilterSearch: function() {

			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("masterView");
			this._oList.getBinding("items").filter(aFilters, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
			} else if (this._oListFilterState.aSearch.length > 0) {
				// only reset the no data text to default when no new search was triggered
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},

		/**
		 * Internal helper method to apply both group and sort state together on the list binding
		 * @param {sap.ui.model.Sorter[]} aSorters an array of sorters
		 * @private
		 */
		_applyGroupSort: function(aSorters) {
			this._oList.getBinding("items").sort(aSorters);
		},

		/**
		 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
		 * @param {string} sFilterBarText the selected filter value
		 * @private
		 */
		_updateFilterBar: function(sFilterBarText) {
			var oViewModel = this.getModel("masterView");
			oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
			oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
		},

		onAfterRendering: function(oEvent) {
			//debugger;

		},

		onExit: function() {
			if (this._oDialogInspoper) {
				this._oDialogInspoper.destroy();
			}
			if (this._getDialogInspoperSonuc) {
				this._getDialogInspoperSonuc.destroy();
			}
		},

//-------------------------------------------------------------------------------------------------------------------------------//
//-------------------------------------------  TableSelectDialog  ---------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//
		setColumnsModel: function(){
			
			this.columns = []; 
		    this.columns.push({	fname:	"Ktextmat",   text: "Nesne kısa mtn.",	selected:false });//true
		    this.columns.push({	fname:	"Prueflos",   text: "Kontrol partisi",	selected:false });
		    this.columns.push({	fname:	"Werk",       text: "Üretim yeri",		selected:false });
		    this.columns.push({	fname:	"Art",        text: "Kontrol türü", 	selected:false });
		    this.columns.push({	fname:	"Herkunft",   text: "Parti kaynağı",	selected:false });
		    this.columns.push({	fname:	"Stsma",      text: "Durum şeması", 	selected:false });
		    this.columns.push({	fname:	"Stat35",     text: "Kullanım kararı",	selected:false });
		    this.columns.push({	fname:	"Stprver",    text: "Örnekleme ynt.",	selected:false });
		    this.columns.push({	fname:	"Enstehdat",  text: "Parti yaratıldı",	selected:false });
		    this.columns.push({	fname:	"Entstezeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Ersteller",  text: "Yaratan",			selected:false });
		    this.columns.push({	fname:	"Ersteldat",  text: "Yaratma tarihi",	selected:false });
		    this.columns.push({	fname:	"Erstelzeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Aufnr",      text: "Şipariş",			selected:false });
		    this.columns.push({	fname:	"Kunnr",      text: "Müşteri",			selected:false });
		    this.columns.push({	fname:	"Lifnr",      text: "Satıcı",			selected:false });
		    this.columns.push({	fname:	"Hersteller", text: "Üretici",			selected:false });
		    this.columns.push({	fname:	"Matnr",      text: "Malzeme",			selected:false });//true
		    this.columns.push({	fname:	"Charg",      text: "Parti",			selected:false });
		    this.columns.push({	fname:	"Lagortchrg", text: "Depo yeri",		selected:false });
		    this.columns.push({	fname:	"Ebeln",      text: "Satınalma blg",	selected:false });
		    this.columns.push({	fname:	"Mjahr",      text: "Mlz.belge yılı",	selected:false });
		    this.columns.push({	fname:	"Mblnr",      text: "Malzeme belgesi",  selected:false });
		    this.columns.push({	fname:	"Budat",      text: "Kayıt tarihi", 	selected:false });
		    this.columns.push({	fname:	"Bwart",      text: "İşlem türü",		selected:false });
		    this.columns.push({	fname:	"Lgnum",      text: "Depo numarası",	selected:false });
		    this.columns.push({	fname:	"Losmenge",   text: "Kontrol prt.mkt",	selected:false });//true
		    this.columns.push({	fname:	"Mengeneinh", text: "Temel ölçü brm.",	selected:false });//true
		    this.columns.push({	fname:	"Einhprobe",  text: "Örnekleme ÖB", 	selected:false });
		    this.columns.push({	fname:	"Idate",      text: "100 karakter", 	selected:false });
		    this.columns.push({	fname:	"Zkk",        text: "X Flag",			selected:false });
		    this.columns.push({	fname:	"IvTip",      text: "Versiyon numarası bileşenleri", selected:false });
			
		},
		
		readTableCustom: function(){
			// var t = this;
			// var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			// var sPath = "/DuzenSet(TableName='1')";
			
			// oModel.read(sPath, {
			// 	urlParameters: {
	  //              "$expand": "TableColumnSet"
	  //          },
			// 	success: function(oData, response) {
			// 		t.setColums(oData.TableColumnSet.results);
			// 	},
			// 	error: function(oError) {
			// 	}
			// });
			var util = Util;
			util.getNotifTable(this.setColums,this,"1");
		},
		
		setColums: function(items,that){
			
			var t = that;
				
			// var oTable = t.byId("idTable");
			for(var i in items){
				
				var t = that;
				var line = t.readTable(t.columns,"fname",items[i].FieldName);
				line.selected=true;
				
				if(line.fname==="Ersteldat" || line.fname==="Enstehdat"){
					var oText =  new sap.m.Text({
	                					text: "{path:'"+line.fname+"', formatter:'com.nauticana.demo.model.formatter.formateDate'}",
	                					wrapping:false
	                				});
				}else if(line.fname==="Entstezeit" || line.fname==="Erstelzeit"){
					 oText =  new sap.m.Text({
	            					text: "{path:'"+line.fname+"', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
	            					wrapping:false
	            				});
				}else{
					oText =  new sap.m.Text({
                					text:"{"+line.fname+"}",
                					wrapping:false
                				});
				}
				
				var oColumn = new sap.ui.table.Column({
									width:items[i].Width,
									filterProperty:line.fname,
									label: new sap.m.Label({
		                    			text: line.text
		                			}), 
		                			visible: true,
		                			template: oText
								});
				oColumn.data("fname",line.fname);
				t.oTable.addColumn( oColumn );
			}
			// t.oTable.attachColumnSelect(function(){t.deneme();});
			t.oTable.attachRowSelectionChange(function(oEvent){t.rowSelected(oEvent);});
		},
		
		rowSelected: function(oEvent){
			var src = oEvent.getSource();
			var index = src.getSelectedIndex();
			var selRow = src.getRows()[index];
			var selObj = selRow.getBindingContext().getObject();
			this.onSelectionChange(selObj);
		},
		
		onSettings: function(oEvent){
			if (!this._oDialogSettings) {
				this._oDialogSettings = sap.ui.xmlfragment("com.nauticana.demo.view.fragments.Settings", this);
				// this._oDialogSettings = this.byId("idSettingsDialog");
				this._oDialogSettings.setModel(this.getView().getModel("i18n"), "i18n");
				var oModel = new sap.ui.model.json.JSONModel({
					FieldCollection:this.columns
				});
				this._oDialogSettings.setModel(oModel,"FieldModel");
				var items = this._oDialogSettings.getItems();
				for(var i in this.columns){
					if(this.columns[i].selected){
						if(items[i]){
							items[i].setSelected(true);
						}
					}
				}
				
			var button2 = new sap.m.Button();
			button2.setText("Tamam");
			var button3 = new sap.m.Button();
			button3.setText("İptal");
			// dialog.getAggregation("_dialog").addButton(button1);
			// dialog.getAggregation("_dialog").addButton(button2);
			// dialog.getAggregation("_dialog").addButton(button3);
			}

			this._oDialogSettings.setRememberSelections(true);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogSettings);
			this._oDialogSettings.open();
			
			// var dialog = this._oDialogSettings;
			// var button1 = new sap.m.Button();
			// button1.setText("test1");
			// var toolbar=dialog.getAggregation("_dialog").getAggregation("_toolbar");
			
			// var content = toolbar.getContent();
			// toolbar.removeAllContent();
			
			// toolbar.addContent(content[0]);
			// toolbar.addContent(button1);
			// toolbar.addContent(content[1]);
			// toolbar.addContent(content[2]);
		},
		
		handleConfirmColumn: function(oEvent){
			
			var aContexts = oEvent.getParameter("selectedContexts");
			
			this.resetColumnsModel();
			
			if (aContexts && aContexts.length) {
				
				var oColumns = this.oTable.getColumns();

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					oSel.selected = true;
					var sel = aContexts[i].getPath().split("/FieldCollection/")[1];
					
					var created=false;
					for(var j in oColumns){
						if(oColumns[j].data().fname===oSel.fname){
							oColumns[j].setVisible(true);
							created=true;
							break;
						}
					}
					if(!created){
						if(oSel.fname==="Ersteldat" || oSel.fname==="Enstehdat"){
							var oText =  new sap.m.Text({
			                					text: "{path:'"+oSel.fname+"', formatter:'com.nauticana.demo.model.formatter.formateDate'}",
			                					wrapping:false
			                				});
						}else if(oSel.fname==="Entstezeit" || oSel.fname==="Erstelzeit"){
							 oText =  new sap.m.Text({
			            					text: "{path:'"+oSel.fname+"', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
			            					wrapping:false
			            				});
						}else{
							oText =  new sap.m.Text({
		                					text:"{"+oSel.fname+"}",
		                					wrapping:false
		                				});
						}
						
						
						var oColumn = new sap.ui.table.Column({
									width:"11rem",
									filterProperty:oSel.fname,
									label: new sap.m.Label({
		                    			text: oSel.text
		                			}), 
		                			visible: true,
		                			template: oText
								});
						oColumn.data("fname",oSel.fname);
						
						// var oTable = this.byId("idTable");
						this.oTable.addColumn( oColumn );
					}
				}
			}
		},
		handleCloseColumn: function(){
		},
		
		resetColumnsModel: function(){
			for(var i in this.columns){
				this.columns[i].selected = false;
			}
			var oColumns = this.oTable.getColumns();
			for(var i in oColumns){
				oColumns[i].setVisible(false);
			}
		},
		
		onSaveSettings: function(oEvent){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/DuzenSet";
			
			var tableColumn = [];
			// tableColumn.push({FieldName:"Art",Width:"10rem"});
			
			var oColumns = this.oTable.getColumns();
			for(var i in oColumns){
				if(oColumns[i].getVisible()){
					tableColumn.push({ FieldName: oColumns[i].data().fname,
									   Width: oColumns[i].getWidth() });
				}
			}
			
			var oPostData = {
				TableName:"1",
				Return:"",
				Message:"",
				TableColumnSet:tableColumn
			};
			// return;
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show(oData.Message);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},
		
		readTable: function(itab,fname,value){
			for(var i in itab){
				var line = itab[i];
				if(line[fname]===value){
					return line;
				}	
			}
			return null;
		}
		

	});

});