/*global location */
sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageBox",
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/List',
		'sap/m/StandardListItem',
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";

		return BaseController.extend("com.nauticana.demo.controller.KPKararlar", {

		 

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				
				var oRouter = this.getRouter();

				oRouter.getRoute("kpkararlar").attachMatched(this._onRouteMatched, this);
			
			 
		 
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});

        	/*	var operationModel = new JSONModel({
        			Insplot : null,
					Inspoper : null,
					TxtOper : null
				});*/



				 var KPSSonuc = new JSONModel({
						enabled : true
				});
				
				this.setModel(KPSSonuc, "oSonuc");
			
			
				this.setModel(oViewModel, "detailView");
		    
			    // this.setModel(operationModel, "operationM");
			},
				_onRouteMatched : function(oEvent) {
			 //debugger;
					var oArgs, oView,oSelectedKey,oSelectedInspoper;
					oArgs = oEvent.getParameter("arguments");
					oView = this.getView();
					oSelectedKey  = oArgs.objectId;
				  
				    var EtQalsSetM = sap.ui.getCore().getModel("EtQalsSetModel");
					var EtQals = {};  	EtQals.EtQalsCollection = EtQalsSetM.getData(); 
					var EtQalsM = new JSONModel(EtQals); 
					this.getView().setModel(EtQalsM,"EtQalsSetModel");
				 
		      
					var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
					var EtZinsM = {}; 	EtZinsM.EtZinsCharCollection = EtZinsCharM.getData(); 
					var EtZM = new JSONModel(EtZinsM); 
					this.getView().setModel(EtZM,"EtZinsCharModel"); 
				 
				 
				 
				 //debugger;
					var EtZusageDecisionM = sap.ui.getCore().getModel("EtZusageDecisionSetModel");
					var EtZusageM = {}; EtZusageM.EtZusageDecisionCollection = EtZusageDecisionM.getData(); 
					var EtzusageM = new JSONModel(EtZusageM); 
					this.getView().setModel(EtzusageM,"EtZusageDecisionSetModel"); 
					
					
					
					
					var EtusageDecisionM = sap.ui.getCore().getModel("EtusageDecisionSetModel");
					var EtusageM = {}; 	EtusageM.EtusageDecisionCollection = EtusageDecisionM.getData(); 
					var EtuseM = new JSONModel(EtusageM); 
					this.getView().setModel(EtuseM,"EtusageDecisionSetModel"); 
					
				/*	
					if(isNaN(EtusageDecisionM)){
							EtusageM.EtusageDecisionCollection = [];
					}else{
							EtusageM.EtusageDecisionCollection = EtusageDecisionM.getData(); 
					}*/
				
					
					
					
					var EtusageDecTextM = sap.ui.getCore().getModel("EtUsageDecTextSetModel");
					var EtusageTextM = {}; 	EtusageTextM.EtusageDecTextCollection = EtusageDecTextM.getData(); 
					var EtusetextM = new JSONModel(EtusageTextM); 
					this.getView().setModel(EtusetextM,"EtUsageDecTextSetModel"); 
					
				/*	if(isNaN(EtusageDecTextM)){
							EtusageTextM.EtusageDecTextCollection = [];
					}else{
							EtusageTextM.EtusageDecTextCollection = EtusageDecTextM.getData(); 
					}*/
				
					
				
			//    var andFilter = [];
			//	var orFilter = [];
				 
				 
				 
		//	     orFilter.push(new sap.ui.model.Filter("Evaluation",sap.ui.model.FilterOperator.EQ, "R"));
				 
			//	 andFilter.push(new sap.ui.model.Filter(orFilter, false));
		//	    orFilter = [];
				 
			//	 orFilter.push(new sap.ui.model.Filter("Obligatory",sap.ui.model.FilterOperator.EQ, "X"));
			//	 orFilter.push(new sap.ui.model.Filter("Code1",sap.ui.model.FilterOperator.EQ, null));
			//	 orFilter.push(new sap.ui.model.Filter("MeanValue",sap.ui.model.FilterOperator.EQ, null));
				 
			//	 andFilter.push(new sap.ui.model.Filter(orFilter, false));
			 
			//	var list = this.getView().byId("list");
			//	var oBinding = list.getBinding("items");
			 //	oBinding.filter(new sap.ui.model.Filter(andFilter, true));
			 //	oBinding.filter(orFilter);
			 	
			//	  var oFilter1 = [ new sap.ui.model.Filter("Evaluation","EQ", "R")];
		    //  var oFilter2 = [new sap.ui.model.Filter("Obligatory","EQ", "X"), new sap.ui.model.Filter("Code1","EQ", ""),new sap.ui.model.Filter("MeanValue","EQ", "") ];
		    
		    
				   
	
			},
			onNavBack : function() {
				
				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();
				
				if (sPreviousHash !== undefined) {
					history.go(-1);
				} else {
					this.getRouter().navTo("master", {}, true);
				}
				
			}

		});

	}
);