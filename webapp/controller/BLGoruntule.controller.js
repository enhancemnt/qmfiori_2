/*global location */
sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";


	return BaseController.extend("com.nauticana.demo.controller.BLGoruntule", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLGoruntule
		 */
		onInit: function() {
			//debugger;
			
			var oRouter = this.getRouter();

			oRouter.getRoute("blgoruntule").attachMatched(this._onRouteMatched, this);
			
			
			
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});
 
	 
			
				this.setModel(oViewModel, "detailView");
		    
		    
		    
		
			
			},
			
			_onRouteMatched : function() {
				//debugger;
			var NotifModel = sap.ui.getCore().getModel("NotifModel");
		  
			this.getView().setModel(NotifModel,"NotifModel"); 
			
			
			
			},
			onNavBack : function() {

				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
					history.go(-1);
				} else {
					this.getRouter().navTo("master", {}, true);
				}
				
			},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLGoruntule
		 */
			onBeforeRendering: function() {
				
		/*			var NotifiCatDetlSetM = sap.ui.getCore().getModel("NotifiCatDetlSetModel");
					var NotifiCatDetlM = {}; 
					NotifiCatDetlM.NotifiCatCollection = NotifiCatDetlSetM.getData(); 
					var NotifiCatDM = new JSONModel(NotifiCatDetlM); 
					this.getView().setModel(NotifiCatDM,"NotifiCatDetlSetModel");
					//var len1 = sap.ui.getCore().getModel("NotifiCatDetlSetModel").getData().length;
		
    			    var NotifiCatHeadSetM = sap.ui.getCore().getModel("NotifiCatHeadSetModel");
    			     
					var NotifiCatHeadlM = {}; 
					NotifiCatHeadlM.NotifiCatHeadCollection = NotifiCatHeadSetM.getData(); 
					var NotifiCatHeadDM = new JSONModel(NotifiCatHeadlM); 
					this.getView().setModel(NotifiCatHeadDM,"NotifiCatHeadSetModel"); 
				//	var len2 = sap.ui.getCore().getModel("NotifiCatHeadSetModel").getData().length;
					
			 
			  	var NotifiCatGroup = {}; 
				NotifiCatGroup.NotifiCatGroupCollection = new Array( ); 
			
		    	var collection1 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
				var collection2 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
			for (var key in collection2) {	
				if (key === 'length' || !collection2.hasOwnProperty(key)) continue;
				for (var key1 in collection1) {
		    			if (key1 === 'length' || !collection1.hasOwnProperty(key1)) continue;
		    			if ( (collection2[key].Rbnr === collection1[key1].Rbnr) && (collection2[key].Fekat === collection1[key1].Qkatart)){
		    			 
		    			 NotifiCatGroup.NotifiCatGroupCollection.push(collection1[key1]);
		    				
		    				break;
		    			}
		    	}
			}
			
			var NotifiCatGroupDM = new JSONModel(NotifiCatGroup); 
			this.getView().setModel(NotifiCatGroupDM,"NotifiCatGroupModel"); 
			
			
			
			var NotifiCatHYeriGroup = {}; 
				NotifiCatHYeriGroup.NotifiCatHYeriGroupCollection = new Array( ); 
			
		    	var collection3 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
				var collection4 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
			for (var key in collection4) {	
				if (key === 'length' || !collection4.hasOwnProperty(key)) continue;
				for (var key1 in collection3) {
		    			if (key1 === 'length' || !collection3.hasOwnProperty(key1)) continue;
		    			if ( (collection4[key].Rbnr === collection3[key1].Rbnr) && (collection4[key].Otkat === collection3[key1].Qkatart)){
		    			 
		    				NotifiCatHYeriGroup.NotifiCatHYeriGroupCollection.push(collection3[key1]);
		    				
		    				break;
		    			}
		    	}
			}
			
			var NotifiCatHYeriGroupDM = new JSONModel(NotifiCatHYeriGroup); 
			this.getView().setModel(NotifiCatHYeriGroupDM,"NotifiCatHYeriGroupModel"); 
			*/
			
			}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLGoruntule
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLGoruntule
		 */
		//	onExit: function() {
		//
		//	}

	});

});