/*global history */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller"
], function(Util,BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.Notif.NotifCreate", {


		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onInit: function() {
			//debugger;
			Util.readCustomising();

			var oRouter = this.getRouter();

			oRouter.getRoute("notifCreate").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			this.setModel(oViewModel, "detailView");

			var Oncelikonay = new JSONModel(jQuery.sap.getModulePath("com.nauticana.demo.model", "/oncelik.json"));

			sap.ui.getCore().setModel(Oncelikonay, "OncelikModel");

		},

		_onRouteMatched: function(oEvent) {
			
			var oModel = new sap.ui.model.json.JSONModel({
				Qmart:"",
				QmartText:"",
				Material:"",
				MaterialText:"",
				PrtNo:"",
				Sernr:"",
				HCode:"",
				HCodeText:"",
				Description:"",
				Saler:"",
				SalerName:"",
				CustomerName:"",
				PriorityText:"",
				Explanation:""
			});
			this.getView().setModel(oModel,"NCModel");
		},
		
//-----------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------- Bildirim Türü ----------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//
		onValueHelpQmart: function(oEvent) {
			//debugger;
			if (!this._oDialogQmart) {
				this._oDialogQmart = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogQmart", this);
				this._oDialogQmart.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogQmart.setModel(this.getView().getModel());
				
				var oJsonModel = sap.ui.getCore().getModel("AuthModel");
				this._oDialogQmart.setModel(oJsonModel,"AuthModel");
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogQmart.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogQmart.setRememberSelections(bRemember);

			// clear the old search filter
			// this._oDialogQmart.getBinding("items").filter([]);

			var ofilter = [new sap.ui.model.Filter("CreateNot", FilterOperator.EQ, "X")];

			var oBinding = this._oDialogQmart.getBinding("items");
			oBinding.filter(ofilter);
			
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogQmart);
			this._oDialogQmart.open();

		},

		handleConfirmQmart: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts")[0];
			// var oInput = this.getView().byId("__bilturu");
			var line = aContexts.getModel().getProperty(aContexts.sPath);
			var value = line.Qmart;
			// var value = oEvent.getParameter("selectedItem").mProperties.title;

			if (aContexts) {
				this.getView().getModel("NCModel").setProperty("/Qmart",line.Qmart);
				this.getView().getModel("NCModel").setProperty("/QmartText",line.Qmartx);
				this.getView().getModel("NCModel").setProperty("/HCode","");
				this.getView().getModel("NCModel").setProperty("/HCodeText","");
			} else {

			}
		},
		
		handleCloseQmart: function(oEvent){
			
		},
				
		onQmartSearch: function(oEvent){
	        // var value = oEvent.getSource().getValue();
	        var value =  oEvent.getSource()._searchField.mProperties.value;
	        var oF1 = new sap.ui.model.Filter("Qmartx", sap.ui.model.FilterOperator.Contains, value);
	        var oFilters = new sap.ui.model.Filter({
	            filters: [
	                oF1
	            ],
	            and: false
	        });
	        this._oDialogQmart.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	    },
//-----------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- Malzeme -------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//
		
		onValueHelpMatnr: function(oEvent){
			if (!this._oDialogMatnr) {
				this._oDialogMatnr = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogMatnr", this);
				this._oDialogMatnr.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogMatnr.setModel(this.getView().getModel());
				this.readMatnr();
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogMatnr.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogMatnr.setRememberSelections(bRemember);

			// clear the old search filter
			// this._oDialogMatnr.getBinding("items").filter([]);

			// var ofilter = [new sap.ui.model.Filter("CreateNot", FilterOperator.EQ, "X")];

			// var oBinding = this._oDialogMatnr.getBinding("items");
			// oBinding.filter(ofilter);
			
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogMatnr);
			this._oDialogMatnr.open();
		},
		
		readMatnr: function(){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/MaterialSet";
			
			Util.showBusy();
			oModel.read(sPath, {
				success: function(oData, response) {
					Util.hideBusy();
					var oJSONModel = new sap.ui.model.json.JSONModel({
						MaterialCollection:oData.results
					});
					t._oDialogMatnr.setModel(oJSONModel, "MaterialModel");
					// console.log(jsonArray);
				},
				error: function(oError) {
					Util.hideBusy();
					// console.log("Error" + oError.responseText)
				}
				});
		},
		
		handleConfirmMatnr: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts")[0];
			// var oInput = this.getView().byId("__bilturu");
			var line = aContexts.getModel().getProperty(aContexts.sPath);
			// var value = oEvent.getParameter("selectedItem").mProperties.title;

			if (aContexts) {
				this.getView().getModel("NCModel").setProperty("/Material",line.Matnr);
				this.getView().getModel("NCModel").setProperty("/MaterialText",line.Maktx);
			} else {

			}
		},
		
		onMatnrSearch: function(oEvent){
	        // var value = oEvent.getSource().getValue();
	        var value =  oEvent.getSource()._searchField.mProperties.value;
	        var oF1 = new sap.ui.model.Filter("Maktx", sap.ui.model.FilterOperator.Contains, value);
	        var oFilters = new sap.ui.model.Filter({
	            filters: [
	                oF1
	            ],
	            and: false
	        });
	        this._oDialogMatnr.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	    },

//-----------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------ Hata Kodu ------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//

		onValueHelpHataKod: function(oEvent) {
			
			var oJsonModel = sap.ui.getCore().getModel("CustomInfoModel");
			var items = oJsonModel.oData.CustomNotifICatHead;
			//debugger;
			if (!this._oDialogHataKod) {
				this._oDialogHataKod = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogBYHata", this);
				this._oDialogHataKod.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogHataKod.setModel(this.getView().getModel());
				this._oDialogHataKod.setModel(oJsonModel,"CustomInfoModel");
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogHataKod.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogHataKod.setRememberSelections(bRemember);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataKod);
			
			
			var qmart = this.getView().byId("__bilturu").getValue();
			for(var i in items){
				if(items[i].Qmart===qmart){
					var rbnr = items[i].Rbnr;
					var fekat = items[i].Fekat;
					break;
				}
			}
				
			var ofilter = [new sap.ui.model.Filter("Qmart", FilterOperator.EQ, qmart), new sap.ui.model.Filter("Rbnr", FilterOperator.EQ, rbnr),
				new sap.ui.model.Filter("Qkatart", FilterOperator.EQ, fekat)
			];

			var oBinding = this._oDialogHataKod.getBinding("items");
			oBinding.filter(ofilter);

			this._oDialogHataKod.open();

		},
		
		handleCloseHkod: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oHata = aContexts[0].getModel().getProperty(oSelPath);
			var oInputHataKod = oHata.Code;

			var oInputHataKod2 = oHata.Kurztext;

			this.getView().byId("__hatakod").setValue(oInputHataKod);
			this.getView().byId("__hatakod").data({"Qcodegrp":oHata.Qcodegrp});
			this.getView().byId("__hatakod").data({"Qkatart":oHata.Qkatart});
			this.getView().byId("__hatatext").setText(oInputHataKod2);

			// this._oDialogHataKod.close();
		},
				
		onHataKodSearch: function(oEvent){
	        // var value = oEvent.getSource().getValue();
	        var value =  oEvent.getSource()._searchField.mProperties.value;
	        var oF1 = new sap.ui.model.Filter("Kurztext", sap.ui.model.FilterOperator.Contains, value);
	        var oFilters = new sap.ui.model.Filter({
	            filters: [
	                oF1
	            ],
	            and: false
	        });
	        this._oDialogHataKod.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	    },

//-----------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------ Öncelik --------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//
		onValueHelpOncelik: function(oEvent) {
			//debugger;
			if (!this._oDialogOncelik) {
				this._oDialogOncelik = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogOncelik", this);
				this._oDialogOncelik.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogOncelik.setModel(this.getView().getModel());
			}
			var IM = sap.ui.getCore().getModel("OncelikModel");

			this._oDialogOncelik.setModel(IM, "OncelikModel");

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogOncelik.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogOncelik.setRememberSelections(bRemember);

			// clear the old search filter
			//this._oDialogOncelik.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogOncelik);
			this._oDialogOncelik.open();
		},

		handleCloseOncelik: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInput = aContexts[0].getModel().getProperty(oSelPath);

			this.getView().byId("__oncelik").setValue(oInput.No);
			
			this.getView().getModel("NCModel").setProperty("/PriorityText",oInput.derece);
			this.getView().byId("__oncelik").setValue(oInput.No);
		},
		
		onOncelikSearch: function(oEvent){
	        // var value = oEvent.getSource().getValue();
	        var value =  oEvent.getSource()._searchField.mProperties.value;
	        var oF1 = new sap.ui.model.Filter("derece", sap.ui.model.FilterOperator.Contains, value);
	        var oFilters = new sap.ui.model.Filter({
	            filters: [
	                oF1
	            ],
	            and: false
	        });
	        this._oDialogOncelik.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	    },

//-----------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- Genel ---------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------------//

		onNavBack: function() {

			// this.getView().byId("__oncelik").setValue("");
			// var oHistory = sap.ui.core.routing.History.getInstance();
			// var sPreviousHash = oHistory.getPreviousHash();

			// if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
				history.go(-1);
			// } else {
			// 	this.getRouter().navTo("master", {}, true);
			// }

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onBeforeRendering: function() {

			/*	  	var NotifiCatGroup = {}; 
					NotifiCatGroup.NotifiCatGroupCollection = new Array( ); 
				
			    	var collection1 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
					var collection2 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
				for (var key in collection2) {	
					if (key === 'length' || !collection2.hasOwnProperty(key)) continue;
					for (var key1 in collection1) {
			    			if (key1 === 'length' || !collection1.hasOwnProperty(key1)) continue;
			    			if ((collection2[key].Qmart === collection1[key1].Qmart) && (collection2[key].Rbnr === collection1[key1].Rbnr) && (collection2[key].Sakat === collection1[key1].Qkatart)){
			    			 
			    			 NotifiCatGroup.NotifiCatGroupCollection.push(collection1[key1]);
			    				
			    			//	break;
			    			}
			    	}
				}
				
				var NotifiCatGroupDM = new JSONModel(NotifiCatGroup); 
				this.getView().setModel(NotifiCatGroupDM,"NotifiCatGroupModel"); 
				
				 
				
						this.getView().getModel("NotifiCatGroupModel").refresh();*/

		},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onAfterRendering: function() {

		},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		//	onExit: function() {
		//
		//	}

		//------------------------------------------------------------------------------------------------------------------------//

		onSaveKarar: function(oEvent) {
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/NotifCreateSet";
			
			var Qmart     = this.byId("__bilturu").getValue();
			var Matnr     = this.byId("__malzeme").getValue();
			var Batch     = this.byId("__partino").getValue();
			var Serialno  = this.byId("__serino").getValue();
			
			var Catalogue = this.getView().byId("__hatakod").data("Qkatart");
			var CodeGroup = this.getView().byId("__hatakod").data("Qcodegrp");
			var Code      = this.byId("__hatakod").getValue();
			var ShortText = this.byId("__tanim").getValue();
			var VendNo    = this.byId("__satici").getValue();
			var CustNo    = this.byId("__mustari").getValue();
			var Priority  = this.byId("__oncelik").getValue();
			var Longtext  = this.byId("__aciklama").getValue();
			
			var oPostData = {
				Qmart:     Qmart,
				Matnr:     Matnr,
				Batch:     Batch,
				Serialno:  Serialno ,
				Catalogue: Catalogue  ,
				CodeGroup: CodeGroup  ,
				Code:      Code     ,
				ShortText: ShortText,
				VendNo:    VendNo   ,
				CustNo:    CustNo   ,
				Priority:  Priority ,
				Longtext:  Longtext,
				Return:    "",
				Message:   ""
			};
			
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show("Bildirim Oluşturuldu");	
						history.go(-1);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		}

	});

});