/*global history */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller"
], function(Util, BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, Formatter, grouper,
	GroupSortState) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.Notif.NotifDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onInit: function() {
			//debugger;
			// Util.readCustomising();
			Util.readCustomisingNotif(this.setEditAuth,this);
			// var items = [];
			// items.push({ models:"Auth", func:this.setEditAuth, t:this });
			// Util.readCustomisingWithFunc(items,this);

			var oRouter = this.getRouter();

			oRouter.getRoute("notifDetail").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			this.setModel(oViewModel, "detailView");

			var Oncelikonay = new JSONModel(jQuery.sap.getModulePath("com.nauticana.demo.model", "/oncelik.json"));
			
			
			var oJSONModel = new sap.ui.model.json.JSONModel({
				Edit:false
			});
			this.getView().setModel(oJSONModel);

			sap.ui.getCore().setModel(Oncelikonay, "OncelikModel");

		},

		_onRouteMatched: function(oEvent) {
			var oArgs = oEvent.getParameter("arguments");
			this.qmnum = oArgs.qmnum;
			this.getView().getModel().setProperty("/EditAuth",false);

			var oJSONModel = new sap.ui.model.json.JSONModel({
				Edit:false,
				Span1:"L3 M5 S12",
				Span2:"L3 M4 S12"
			});
			this.getView().setModel(oJSONModel);
			this.getDetail();
		},
		
		setEditAuth: function(t){
			if(t.editAuthFlag!==true){
				t.editAuthFlag=true;
			}else{
				var oJsonModel = sap.ui.getCore().getModel("AuthModel");
				var items = oJsonModel.oData.AuthNotif;
				for(var i in items){
					if(items[i].Qmart===t.qmart){
						if(items[i].ChangeNot==="X"){
							t.getView().getModel().setProperty("/EditAuth",true);
						}
						break;	
					}
				}
			}
		},

		onNavBack: function() {
			history.go(-1);
		},

		getDetail: function() {
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			var sPath = "/NotifDetailSet(Qmnum='" + this.qmnum + "')";
			Util.showBusy();
			oModel.read(sPath, {
				urlParameters: {
		        	"$expand": "NotifStatusSet"
            	},
				success: function(oData, response) {
					Util.hideBusy();
					t.qmart = oData.Qmart;
					t.setEditAuth(t);
					var jsonArray = [];
					var oJSONModel = new sap.ui.model.json.JSONModel();
					oJSONModel.setData(oData);
					oData.NotifStatusSet = oData.NotifStatusSet.results;
					// oData.CreatedDate = Formatter.formateDate2(oData.CreatedDate);
					// oData.CreatedDate = Formatter.formateDate(oData.CreatedDate);
					// oData.ChangedDate = Formatter.formateDate2(oData.ChangedDate);
					// oData.ChangedDate = Formatter.formateDate(oData.ChangedDate);
					t.NDModel = oData;
					t.getView().setModel(oJSONModel, "NDModel");
					t.byId("__aciklama").setEditable(true);
					t.byId("__aciklama").setEditable(false);
					// console.log(jsonArray);
				},
				error: function(oError) {
					Util.hideBusy();
					// console.log("Error" + oError.responseText)
				}
			});
		},
		
		onEdit: function(oEvent){
			this.NDModelTemp = JSON.parse(JSON.stringify(this.NDModel));
			// this.NDModel = JSON.parse(JSON.stringify(this.getView().getModel("NDModel")));
			// this.getView().getModel().setProperty("/Span1","L3 M4 S12");
			// this.getView().getModel().setProperty("/Span2","L3 M5 S12");
			this.getView().getModel().setProperty("/Edit",true);
			this.getView().getModel("NDModel").setProperty("/Longtext","");
		},
		
		onComplate: function(oEvent){
			// this.getView().getModel().setProperty("/Edit",false);
			if (!this._oDialogComplete) {
				this._oDialogComplete = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogNotifComplete", this);
				this._oDialogComplete.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogComplete.setModel(this.getView().getModel());
			}
			var oModel = new sap.ui.model.json.JSONModel({
						Date: new Date(),
						Time: new Date()
				});
			this._oDialogComplete.setModel(oModel);
			
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogComplete);
			this._oDialogComplete.open();
		},
		
		onNotifCompleteConfirm: function(oEvent){
			
			var date = this._oDialogComplete.getModel().getProperty("/Date");
			var time = this._oDialogComplete.getModel().getProperty("/Time");
			date.setHours(time.getHours());
			date.setMinutes(time.getMinutes());
			date.setSeconds(time.getSeconds());
			
			this.CompleteDate = Formatter.formateDate(date);
			this.CompleteTime = Formatter.formatTime(time);
			// this.CompleteTime = ""+time.getHours()+time.getMinutes()+time.getSeconds();
			this.save(true);
			this._oDialogComplete.close();
		},
		
		onNotifCompleteCancel: function(oEvent){
			this._oDialogComplete.close();
		},
		
		onSave: function(oEvent){
			this.save(false);
		},
		
		save: function(complete){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			var NDModel = this.getView().getModel("NDModel");
			
			var sPath = "/NotifUpdateSet";
			var oPostData = {
				Qmnum:this.qmnum,
				Stat:this.NDModel.Stat,
				Longtext: this.NDModel.Longtext,
				CompleteDate:complete ? this.CompleteDate: "", 
				CompleteTime:complete ? this.CompleteTime: "", 
				Return:"",
				Message:""
			};
			
			Util.showBusy();
			oModel.create(sPath,oPostData,{
				success: function(oData, response) {
					Util.hideBusy();
					sap.m.MessageToast.show(oData.Message);
					if(oData.Return==="SUCCESS"){
						t.getView().getModel("NDModel").setProperty("/Txt30",t.byId("idStatus")._getSelectedItemText());
						if(complete){
							t.getView().getModel("NDModel").setProperty("/Completed","X");
						}
						// this.getView().getModel().setProperty("/Span1","L3 M5 S12");
						// this.getView().getModel().setProperty("/Span2","L3 M4 S12");
						t.getView().getModel().setProperty("/Edit",false);
						t.getView().getModel("NDModel").setProperty("/Longtext",oData.Longtext);
					}else{
					}
				},
				error: function(oError) {
					Util.hideBusy();
					// console.log("Error" + oError.responseText)
				}
			});
			
		},
		
		onCancel: function(oEvent){
			this.NDModel = this.NDModelTemp;
			// this.getView().getModel(oJSONModel,"NDModel");
			this.getView().getModel("NDModel").setProperty("/Stat",this.NDModel.Stat);
			this.getView().getModel("NDModel").setProperty("/Txt30",this.NDModel.Txt30);
			this.getView().getModel("NDModel").setProperty("/Longtext",this.NDModel.Longtext);
			// this.getView().getModel().setProperty("/Span1","L3 M5 S12");
			// this.getView().getModel().setProperty("/Span2","L3 M4 S12");
			this.getView().getModel().setProperty("/Edit",false);
			
		}

	});

});