sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"com/nauticana/demo/util/Util",
	'sap/m/Token',
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller"
], function(BaseController, Util, Token, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper,
	GroupSortState) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.Notif.NotifList", {
		onInit: function() {
			var fragmentId = this.getView().createId("idTableFragment");
			this.oTable = sap.ui.core.Fragment.byId(fragmentId, "idTable");
			this.oSearchField = sap.ui.core.Fragment.byId(fragmentId, "idSearchField");
			this.setColumnsModel();
			this.readTableCustom();
			// Util.readCustomising();
			Util.readCustomisingNotif(this.setDefaultFilter,this);
			this.defaultFilter = true;

			this.getRouter().getRoute("notif").attachPatternMatched(this._onMasterMatched, this);
		},

		_onMasterMatched: function(oEvent) {
			var t = this;

			if(!this.filterFlag){
				this.getView().getModel().setProperty("/NotifListCollection",[]);
				var bReplace = Device.system.phone;
				// setTimeout(function() {
				// 		t.onFilterDialog();	
				// }, Device.system.phone == true ? 1 : 1000);
			}else{
			}
			this.setDefaultFilter(this);
			this.handleFilterSearchPress();

		},

		onAfterRendering: function(oEvent) {
			// this.onFilterDialog();
		},

		onNavBack: function() {
			this.filterFlag=false;
			history.go(-1);
		},

		onFilterDialog: function() {

			if (!this._oDialogFilter) {
				this._oDialogFilter = this.byId("idFilterDialog");
				this._oDialogFilter.setModel(this.getView().getModel("i18n"), "i18n");
				var oModel = new sap.ui.model.json.JSONModel({
						DateVisible: false
				});
				this._oDialogFilter.setModel(oModel);
			}
				

			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogFilter);
			this._oDialogFilter.setVisible(true);
			this._oDialogFilter.open();

			var fragmentId = this.getView().createId("idFilterFragment");
			var oQmnum = sap.ui.core.Fragment.byId(fragmentId, "idQmnum");

			oQmnum.addValidator(function(args) {
				var text = args.text;
				return new Token({
					key: text,
					text: text
				});
			});
			this.filterFlag = true;
		},
		
		onNotifDateRangeChange: function(){
			var fragmentId = this.getView().createId("idFilterFragment");
			var oDateSelect = sap.ui.core.Fragment.byId(fragmentId, "__dateID");
			if(oDateSelect.getSelectedKey()==="5"){
				this._oDialogFilter.getModel().setProperty("/DateVisible",true);
			}else{
				this._oDialogFilter.getModel().setProperty("/DateVisible",false);
			}
			
			
		},
		
		onLiveChangeQmnumFilter: function(oEvent){
			this.liveChangeLength(oEvent,12);
		},
		
		onLiveChangeMatnrFilter: function (oEvent){
			this.liveChangeLength(oEvent,18);
		},
		
		liveChangeLength: function(oEvent,max){
			var value = oEvent.getParameters().value;
			if(value.length>max){
				var input = oEvent.getSource();
				input.setValue(value.substr(0,max));
			}
		},
		
		setDefaultFilter: function(t){
			if(!t.routeFinish){
				t.routeFinish = true;
			}else{
				if (!t._oDialogQmart) {
					t._oDialogQmart = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogQmart", t);
					t._oDialogQmart.setModel(t.getView().getModel("i18n"), "i18n");
					t._oDialogQmart.setModel(t.getView().getModel());
					var oJsonModel = sap.ui.getCore().getModel("AuthModel");
					t._oDialogQmart.setModel(oJsonModel,"AuthModel");
				}
				
				var oJsonModel = sap.ui.getCore().getModel("AuthModel");
				var items = oJsonModel.oData.AuthNotif;
				var aTokens = [];
				var fragmentId = t.getView().createId("idFilterFragment");
				var oInput = sap.ui.core.Fragment.byId(fragmentId, "idQmart");
				for(var i in items){
					if(items[i].DisplayNot==="X"){
						var token1 = new sap.m.Token({
								key: items[i].Qmart,
								text: items[i].Qmart
							});
						aTokens.push(token1);
					}
				}
					oInput.setTokens(aTokens);
			}
		},

		handleFilterSearchPress: function(oEvent) {
			var t = this;
			var fragmentId = this.getView().createId("idFilterFragment");
			
			var oFilter = [];
			if(this.defaultFilter){
				//Default Filter -----------------------------------------------------------------------//
				oFilter.push(new sap.ui.model.Filter("DefaultFilter", sap.ui.model.FilterOperator.EQ, "X"));
				this.defaultFilter = false;
			}else{
				
				// Bildirim Türü...	----------------------------------------------------------------------//
				var oQmart = sap.ui.core.Fragment.byId(fragmentId, "idQmart");
				var oQmartFilter = [];
				var qmartList = oQmart.getTokens();
				for(var i in qmartList){
			        oQmartFilter.push(new sap.ui.model.Filter("Qmart", sap.ui.model.FilterOperator.EQ, qmartList[i].getKey()));
				}
				if(oQmartFilter.length > 0){
					oFilter.push(new sap.ui.model.Filter({ filters: oQmartFilter, and: false }));
				}
				
				//Bildirim Numarası...	------------------------------------------------------------------//
				var oQmnum = sap.ui.core.Fragment.byId(fragmentId, "idQmnum");
				var oQmnumFilter = [];
				var qmnumList = oQmnum.getTokens();
				for(var i in qmnumList){
					// var qmnum = qmnumList[i].getKey();
			        oQmnumFilter.push(new sap.ui.model.Filter("Qmnum", sap.ui.model.FilterOperator.EQ, qmnumList[i].getKey()));
				}
				if(oQmnumFilter.length > 0){
					oFilter.push(new sap.ui.model.Filter({ filters: oQmnumFilter, and: false }));
				}
				
				//Bildirim Tarihi... --------------------------------------------------------------------//
				var oDateSelect = sap.ui.core.Fragment.byId(fragmentId, "__dateID");
				oFilter.push(new sap.ui.model.Filter("DateRangeType", sap.ui.model.FilterOperator.EQ, oDateSelect.getSelectedKey()));
				
				if(oDateSelect.getSelectedKey()==="5"){
					var oDate = sap.ui.core.Fragment.byId(fragmentId, "idDate");
					oFilter.push(new sap.ui.model.Filter("Date", sap.ui.model.FilterOperator.BT, oDate.getDateValue(),oDate.getSecondDateValue()));
				}
				
				//Malzeme Numarası... -------------------------------------------------------------------//
				var oMatnr = sap.ui.core.Fragment.byId(fragmentId, "idMatnr");
				var oMatnrFilter = [];
				var matnrList = oMatnr.getTokens();
				for(var i in matnrList){
			        oMatnrFilter.push(new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.EQ, matnrList[i].getKey()));
				}
				if(oMatnrFilter.length > 0){
					oFilter.push(new sap.ui.model.Filter({ filters: oMatnrFilter, and: false }));
				}
				
				//Statü... -----------------------------------------------------------------------------//
				var oCb1 = sap.ui.core.Fragment.byId(fragmentId, "idChkbx1");
				var cb1 = oCb1.getSelected();
				var oCb2 = sap.ui.core.Fragment.byId(fragmentId, "idChkbx2");
				var cb2 = oCb2.getSelected();
				var oStatusFilter = [];
				
				if(cb1 === true){
			        oStatusFilter.push(new sap.ui.model.Filter("Status", sap.ui.model.FilterOperator.EQ, "O"));
				}
				if(cb2 === true){
			        oStatusFilter.push(new sap.ui.model.Filter("Status", sap.ui.model.FilterOperator.EQ, "C"));
				}
				if(oStatusFilter.length>0){
					oFilter.push(new sap.ui.model.Filter({ filters: oStatusFilter, and: false }));
				}
			
			}
			
			var oDataFilter = new sap.ui.model.Filter({ 
										filters: oFilter,
										and: true 
									});
			
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			var sPath = "/NotifListSet";
			
			Util.showBusy();
			
			oModel.read(sPath, {
				filters: [oDataFilter],
				success: function(oData, response) {
					Util.hideBusy();
					var oModel = new sap.ui.model.json.JSONModel({
						NotifListCollection: response.data.results
					});
					t.getView().setModel(oModel);//,"NotifListModel"
					t.searchLiveChange(t.oSearchField.getValue());
				},
				error: function(oError) {
					Util.hideBusy();
					// console.log("Error" + oError.responseText)
				}
			});
			
			if(this._oDialogFilter){
				this._oDialogFilter.close();
				this._oDialogFilter.setVisible(false);
			}
		},

		handleFilterCloselPress: function(oEvent) {
			this._oDialogFilter.close();
			this._oDialogFilter.setVisible(false);
		},
		
		
		onRefresh: function(oEvent){
			this.handleFilterSearchPress();
		},
		
		
//------------------------------- Search ----------------------------------------------//
    onSearchLiveChange: function(oEvent){
        var value = oEvent.getSource().getValue();
        this.searchLiveChange(value);
    },
    
    searchLiveChange: function(value){
        var oF1 = new sap.ui.model.Filter("Qmtxt", sap.ui.model.FilterOperator.Contains, value);
        var oFilters = new sap.ui.model.Filter({
            filters: [
                oF1
            ],
            and: false
        });
        this.oTable.getBinding("rows").filter(oFilters, sap.ui.model.FilterType.Application);

    },

//-------------------------------------------------------------------------------------------------------------------------------//
//-----------------------------------------------  Qmart Help  ------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//		
		onValueHelpQmart: function(oEvent) {
			//debugger;
			if (!this._oDialogQmart) {
				this._oDialogQmart = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogQmart", this);
				this._oDialogQmart.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogQmart.setModel(this.getView().getModel());
				var oJsonModel = sap.ui.getCore().getModel("AuthModel");
				this._oDialogQmart.setModel(oJsonModel,"AuthModel");
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogQmart.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogQmart.setRememberSelections(false);

			// clear the old search filter
			// this._oDialogQmart.getBinding("items").filter([]);

			var ofilter = [new sap.ui.model.Filter("DisplayNot", FilterOperator.EQ, "X")];

			var oBinding = this._oDialogQmart.getBinding("items");
			oBinding.filter(ofilter);
			
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogQmart);
			this._oDialogQmart.open();

		},

		handleSearchQmart: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Qmart", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},

		handleConfirmQmart: function(oEvent) {

			var aContexts = oEvent.getParameter("selectedContexts");

			var fragmentId = this.getView().createId("idFilterFragment");
			var oInput = sap.ui.core.Fragment.byId(fragmentId, "idQmart");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Qmart,
						text: oSel.Qmart
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		
		handleCloseQmart: function(oEvent) {
			
		},
				
		onQmartSearch: function(oEvent){
	        // var value = oEvent.getSource().getValue();
	        var value =  oEvent.getSource()._searchField.mProperties.value;
	        var oF1 = new sap.ui.model.Filter("Qmartx", sap.ui.model.FilterOperator.Contains, value);
	        var oFilters = new sap.ui.model.Filter({
	            filters: [
	                oF1
	            ],
	            and: false
	        });
	        this._oDialogQmart.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	    },

		//-------------------------------------------------------------------------------------------------------------------------------//
		//----------------------------------------------  Material Help  ----------------------------------------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//			
		onValueHelpMatnr: function(oEvent) {
			if (!this._oDialogBL) {
				this._oDialogBL = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.DialogBLMatnr", this);
				this._oDialogBL.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogBL.setModel(this.getView().getModel());
				this.readMatnr();
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogBL.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogBL.setRememberSelections(false);

			// clear the old search filter
			// this._oDialogBL.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogBL);
			this._oDialogBL.open();
		},
		
		readMatnr: function(){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/MaterialSet";
			
			Util.showBusy();
			oModel.read(sPath, {
				success: function(oData, response) {
					Util.hideBusy();
					var oJSONModel = new sap.ui.model.json.JSONModel({
						MaterialCollection:oData.results
					});
					t._oDialogBL.setModel(oJSONModel, "MaterialModel");
					// console.log(jsonArray);
				},
				error: function(oError) {
					Util.hideBusy();
					// console.log("Error" + oError.responseText)
				}
				});
		},

		handleSearchMatnr: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Maktx", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		// handleSearchMatnr: function(oEvent){
	 //       // var value = oEvent.getSource().getValue();
	 //       var value =  oEvent.getSource()._searchField.mProperties.value;
	 //       var oF1 = new sap.ui.model.Filter("Maktx", sap.ui.model.FilterOperator.Contains, value);
	 //       var oFilters = new sap.ui.model.Filter({
	 //           filters: [
	 //               oF1
	 //           ],
	 //           and: false
	 //       });
	 //       this._oDialogMatnr.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
	 //   },

		handleCloseBLMatnr: function(oEvent) {

			var aContexts = oEvent.getParameter("selectedContexts");
			var fragmentId = this.getView().createId("idFilterFragment");
			var oInput = sap.ui.core.Fragment.byId(fragmentId, "idMatnr");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Matnr,
						text: oSel.Matnr
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		
		onCreate: function(){
			this.getOwnerComponent().getRouter().navTo("notifCreate", true);
		},

		//-------------------------------------------------------------------------------------------------------------------------------//
		//-------------------------------------------  TableSelectDialog  ---------------------------------------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//
		setColumnsModel: function() {

			this.columns = [];
			// this.columns.push({
			// 	fname: "Low",
			// 	text: "Tarih",
			// 	selected: false
			// });
			// this.columns.push({
			// 	fname: "High",
			// 	text: "Tarih",
			// 	selected: false
			// });
			this.columns.push({
				fname: "IDateRangeType",
				text: "Karakter 1",
				selected: false
			});
			this.columns.push({
				fname: "IStat",
				text: "Notification Status",
				selected: false
			});
			this.columns.push({
				fname: "IUname",
				text: "User Name",
				selected: false
			});
			this.columns.push({
				fname: "Qmart",
				text: "Bildirim türü",
				selected: false
			});
			this.columns.push({
				fname: "Qmnum",
				text: "Bildirim",
				selected: false
			});
			this.columns.push({
				fname: "Matnr",
				text: "Malzeme",
				selected: false
			});
			this.columns.push({
				fname: "Qmtxt",
				text: "Tanım",
				selected: false
			});
			this.columns.push({
				fname: "Txt30",
				text: "Metin",
				selected: false
			});
			this.columns.push({
				fname: "Charg",
				text: "Parti",
				selected: false
			});
			this.columns.push({
				fname: "Serialnr",
				text: "Seri numarası",
				selected: false
			});
			this.columns.push({
				fname: "Qmgrp",
				text: "Kod grubu",
				selected: false
			});
			this.columns.push({
				fname: "Qmcod",
				text: "Kodlama",
				selected: false
			});
			this.columns.push({
				fname: "Lifnum",
				text: "Satıcı",
				selected: false
			});
			this.columns.push({
				fname: "Kunum",
				text: "Müşteri",
				selected: false
			}); //true
			this.columns.push({
				fname: "Priok",
				text: "Öncelik",
				selected: false
			});
			this.columns.push({
				fname: "Ernam",
				text: "Yaratan",
				selected: false
			});
			this.columns.push({
				fname: "Erdat",
				text: "Yaratma trh.",
				selected: false
			});
			this.columns.push({
				fname: "Aenam",
				text: "Değiştiren",
				selected: false
			});
			this.columns.push({
				fname: "Aedat",
				text: "Dğşk.tarihi",
				selected: false
			});
		},

		readTableCustom: function() {
			var util = Util;
			util.getNotifTable(this.setColums, this, "2");
		},

		setColums: function(items, that) {

			var t = that;
			t.oTable.destroyColumns();

			// var oTable = t.byId("idTable");
			for (var i in items) {

				var line = t.readTable(t.columns, "fname", items[i].FieldName);
				if (line) {
					line.selected = true;

					if (line.fname === "Erdat" || line.fname === "Aedat") {
						var oText = new sap.m.Text({
							text: "{path:'" + line.fname + "', formatter:'com.nauticana.demo.model.formatter.formateDate'}",//NotifListModel>
							wrapping: false
						});
					} else if (line.fname === "Entstezeit" || line.fname === "Erstelzeit") {
						oText = new sap.m.Text({
							text: "{path:'" + line.fname + "', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
							wrapping: false
						});
					} else {
						oText = new sap.m.Text({
							text: "{" + line.fname + "}",
							wrapping: false
						});
					}

					var oColumn = new sap.ui.table.Column({
						width: items[i].Width,
						filterProperty: line.fname,
						label: new sap.m.Label({
							text: line.text
						}),
						visible: true,
						template: oText
					});
					oColumn.data("fname", line.fname);
					t.oTable.addColumn(oColumn);
				}
			}
			// t.oTable.attachColumnSelect(function(){t.deneme();});
			t.oTable.attachRowSelectionChange(function(oEvent) {
				t.rowSelected(oEvent);
			});
			// t.oTable.attachColumnSelect(function(oEvent) {
			// 	t.colSelected(oEvent);
			// });
			
		},

		rowSelected: function(oEvent) {
			// var src = oEvent.getSource();
			// // var index = src.getSelectedIndex();
			// var index = oEvent.getParameter("rowIndex");
			// var selRow = src.getRows()[index];
			// var selObj = selRow.getBindingContext().getObject();
			
			var aContexts = oEvent.getParameter("rowContext");
			var selObj = aContexts.oModel.getProperty(aContexts.getPath());
			
			this.getOwnerComponent().getRouter().navTo("notifDetail", {
                        qmnum: selObj.Qmnum
                    });
			
		},
		
		// colSelected: function(oEvent) {
			
		// },

		onSettings: function(oEvent) {
			if (!this._oDialogSettings) {
				this._oDialogSettings = sap.ui.xmlfragment("com.nauticana.demo.view.Notif.fragments.Settings", this);
				// this._oDialogSettings = this.byId("idSettingsDialog");
				this._oDialogSettings.setModel(this.getView().getModel("i18n"), "i18n");
				var oModel = new sap.ui.model.json.JSONModel({
					FieldCollection: this.columns
				});
				this._oDialogSettings.setModel(oModel, "FieldModel");
				var items = this._oDialogSettings.getItems();
				for (var i in this.columns) {
					if (this.columns[i].selected) {
						if (items[i]) {
							items[i].setSelected(true);
						}
					}
				}

			}

			this._oDialogSettings.setRememberSelections(true);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogSettings);
			this._oDialogSettings.open();

		},

		handleConfirmColumn: function(oEvent) {

			var aContexts = oEvent.getParameter("selectedContexts");

			this.resetColumnsModel();

			if (aContexts && aContexts.length) {

				var oColumns = this.oTable.getColumns();

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					oSel.selected = true;
					var sel = aContexts[i].getPath().split("/FieldCollection/")[1];

					var created = false;
					for (var j in oColumns) {
						if (oColumns[j].data().fname === oSel.fname) {
							oColumns[j].setVisible(true);
							created = true;
							break;
						}
					}
					if (!created) {
						if (oSel.fname === "Erdat" || oSel.fname === "Aedat") {
							var oText = new sap.m.Text({
								text: "{path:'" + oSel.fname + "', formatter:'com.nauticana.demo.model.formatter.formateDate'}",
								wrapping: false
							});
						} else if (oSel.fname === "Entstezeit" || oSel.fname === "Erstelzeit") {
							oText = new sap.m.Text({
								text: "{path:'" + oSel.fname + "', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
								wrapping: false
							});
						} else {
							oText = new sap.m.Text({
								text: "{" + oSel.fname + "}",
								wrapping: false
							});
						}

						var oColumn = new sap.ui.table.Column({
							width: "11rem",
							filterProperty: oSel.fname,
							label: new sap.m.Label({
								text: oSel.text
							}),
							visible: true,
							template: oText
						});
						oColumn.data("fname", oSel.fname);

						// var oTable = this.byId("idTable");
						this.oTable.addColumn(oColumn);
					}
				}
			}
		},
		handleCloseColumn: function() {},

		resetColumnsModel: function() {
			for (var i in this.columns) {
				this.columns[i].selected = false;
			}
			var oColumns = this.oTable.getColumns();
			for (var i in oColumns) {
				oColumns[i].setVisible(false);
			}
		},

		onSaveSettings: function(oEvent) {
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");

			var sPath = "/DuzenSet";

			var tableColumn = [];
			// tableColumn.push({FieldName:"Art",Width:"10rem"});

			var oColumns = this.oTable.getColumns();
			for (var i in oColumns) {
				if (oColumns[i].getVisible()) {
					tableColumn.push({
						FieldName: oColumns[i].data().fname,
						Width: oColumns[i].getWidth()
					});
				}
			}

			var oPostData = {
				TableName: "2",
				Return: "",
				Message: "",
				TableColumnSet: tableColumn
			};
			// return;
			oModel.create(sPath, oPostData, {
				success: function(oData, response) {
					if (oData.Return == "SUCCESS") {
						sap.m.MessageToast.show(oData.Message);
					} else {
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});
					}
				},
				error: function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},

		readTable: function(itab, fname, value) {
			for (var i in itab) {
				var line = itab[i];
				if (line[fname] === value) {
					return line;
				}
			}
			return null;
		}

	});

});