/*global history */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/mvc/Controller",
	"sap/ui/table/Column"
], function(BaseController, JSONModel, History, Device, formatter,Column) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.Master2", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/*
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */ 
		onInit: function(oEvent) {
			//  filtering  
			this._oView = this.getView();

			// Control state model
			//var oList = this.byId("list"),
			var oViewModel = this._createViewModel();
			// Put down master list's original value for busy indicator delay,
			// so it can be restored later on. Busy handling on the master list is
			// taken care of by the master list itself.
			//iOriginalBusyDelay = oList.getBusyIndicatorDelay();

			// jQuery.sap.require("sap.ui.core.util.MockServer");
			// var oMockServer = new sap.ui.core.util.MockServer({
			// 	rootUri: "sapuicompsmarttable/"
			// });
			// this._oMockServer = oMockServer;
			// oMockServer.simulate("com/nauticana/demo/mockserver/metadata.xml", "com/nauticana/demo/mockserver/");
			// oMockServer.start();
			// var oModel = new sap.ui.model.odata.ODataModel("sapuicompsmarttable", true);
			// oModel.setCountSupported(false);
			// var oView = this.getView();
			// oView.setModel(oModel);

			// this.setModel(oViewModel, "masterView");
			
			this.setColumnsModel();
			this.readTableCustom();

			this.getRouter().getRoute("master").attachPatternMatched(this._onRouteMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);

		},
		
		onExit: function() {
			if (this._oDialogInspoper) {
				this._oDialogInspoper.destroy();
			}
			if (this._getDialogInspoperSonuc) {
				this._getDialogInspoperSonuc.destroy();
			}

			this._oMockServer.stop();
		},
		/**
		 * If the master route was hit (empty hash) we have to set
		 * the hash to to the first item in the list as soon as the
		 * listLoading is done and the first item in the list is known
		 * @private
		 */
		_onRouteMatched: function(oEvent) {
			// this.readTableCustom();
		},
		
		columnFactory : function(sId, oContext) {
			var oModel = this.getView().getModel();
			var sName = oContext.getProperty("name");
			var sType = oContext.getProperty("type");
			var sSemantics = oContext.getProperty("sap:semantics");
			var bVisible = oContext.getProperty("sap:visible") != "false";
			var iLen = oContext.getProperty("maxLength");
			var sColumnWidth = "5rem";

			iLen = iLen ? parseInt(iLen, 10) : 10;

			if (iLen > 50) {
				sColumnWidth = "15rem";
			} else if (iLen > 9) {
				sColumnWidth = "10rem";
			}

			return new Column(sId, {
				sortProperty: oContext.getProperty("sap:sortable") == "true" ? sName : null,
				filterProperty: oContext.getProperty("sap:filterable") == "true" ? sName : null,
				width: sColumnWidth,
				label: new sap.m.Label({text: "text"}),
			});
		},
		
		setColumnsModel: function(){
			
			this.columns = []; 
		    this.columns.push({	fname:	"Ktextmat",   text: "Nesne kısa mtn.",	selected:false });//true
		    this.columns.push({	fname:	"Prueflos",   text: "Kontrol partisi",	selected:false });
		    this.columns.push({	fname:	"Werk",       text: "Üretim yeri",		selected:false });
		    this.columns.push({	fname:	"Art",        text: "Kontrol türü", 	selected:false });
		    this.columns.push({	fname:	"Herkunft",   text: "Parti kaynağı",	selected:false });
		    this.columns.push({	fname:	"Stsma",      text: "Durum şeması", 	selected:false });
		    this.columns.push({	fname:	"Stat35",     text: "Kullanım kararı",	selected:false });
		    this.columns.push({	fname:	"Stprver",    text: "Örnekleme ynt.",	selected:false });
		    this.columns.push({	fname:	"Enstehdat",  text: "Parti yaratıldı",	selected:false });
		    this.columns.push({	fname:	"Entstezeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Ersteller",  text: "Yaratan",			selected:false });
		    this.columns.push({	fname:	"Ersteldat",  text: "Yaratma tarihi",	selected:false });
		    this.columns.push({	fname:	"Erstelzeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Aufnr",      text: "Şipariş",			selected:false });
		    this.columns.push({	fname:	"Kunnr",      text: "Müşteri",			selected:false });
		    this.columns.push({	fname:	"Lifnr",      text: "Satıcı",			selected:false });
		    this.columns.push({	fname:	"Hersteller", text: "Üretici",			selected:false });
		    this.columns.push({	fname:	"Matnr",      text: "Malzeme",			selected:false });//true
		    this.columns.push({	fname:	"Charg",      text: "Parti",			selected:false });
		    this.columns.push({	fname:	"Lagortchrg", text: "Depo yeri",		selected:false });
		    this.columns.push({	fname:	"Ebeln",      text: "Satınalma blg",	selected:false });
		    this.columns.push({	fname:	"Mjahr",      text: "Mlz.belge yılı",	selected:false });
		    this.columns.push({	fname:	"Mblnr",      text: "Malzeme belgesi",  selected:false });
		    this.columns.push({	fname:	"Budat",      text: "Kayıt tarihi", 	selected:false });
		    this.columns.push({	fname:	"Bwart",      text: "İşlem türü",		selected:false });
		    this.columns.push({	fname:	"Lgnum",      text: "Depo numarası",	selected:false });
		    this.columns.push({	fname:	"Losmenge",   text: "Kontrol prt.mkt",	selected:false });//true
		    this.columns.push({	fname:	"Mengeneinh", text: "Temel ölçü brm.",	selected:false });//true
		    this.columns.push({	fname:	"Einhprobe",  text: "Örnekleme ÖB", 	selected:false });
		    this.columns.push({	fname:	"Idate",      text: "100 karakter", 	selected:false });
		    this.columns.push({	fname:	"Zkk",        text: "X Flag",			selected:false });
		    this.columns.push({	fname:	"IvTip",      text: "Versiyon numarası bileşenleri", selected:false });
		    
		 //   var oTable = this.byId("idTable");
			// for(var i in this.columns){
				
			// 	var oColumn = new sap.ui.table.Column({
			// 						width:"11rem",
			// 						filterProperty:this.columns[i].fname,
			// 						label: new sap.m.Label({
		 //                   			text: this.columns[i].text
		 //               			}), 
		 //               			visible: this.columns[i].selected,
		 //               			template: new sap.m.Text({
			// 		                					text:"{"+this.columns[i].fname+"}",
			// 		                					wrapping:false
			// 		                				})
			// 					});
			// 	oColumn.data("fname",this.columns[i].fname);
			// 	oTable.addColumn( oColumn );
			// }
		},
		
		setColums: function(items){
				
			var oTable = this.byId("idTable");
			for(var i in items){
				
				var line = this.readTable(this.columns,"fname",items[i].FieldName);
				line.selected=true;
				var oColumn = new sap.ui.table.Column({
									width:items[i].Width,
									filterProperty:this.columns[i].fname,
									label: new sap.m.Label({
		                    			text: line.text
		                			}), 
		                			visible: true,
		                			template: new sap.m.Text({
					                					text:"{"+line.fname+"}",
					                					wrapping:false
					                				})
								});
				oColumn.data("fname",line.fname);
				oTable.addColumn( oColumn );
			}
		},
		
		readTable: function(itab,fname,value){
			for(var i in itab){
				var line = itab[i];
				if(line[fname]===value){
					return line;
				}	
			}
			return null;
		},
		
		onSettings: function(oEvent){
			if (!this._oDialogSettings) {
				this._oDialogSettings = sap.ui.xmlfragment("com.nauticana.demo.view.fragments.Settings", this);
				// this._oDialogSettings = this.byId("idSettingsDialog");
				this._oDialogSettings.setModel(this.getView().getModel("i18n"), "i18n");
				var oModel = new sap.ui.model.json.JSONModel({
					FieldCollection:this.columns
				});
				this._oDialogSettings.setModel(oModel,"FieldModel");
				var items = this._oDialogSettings.getItems();
				for(var i in this.columns){
					if(this.columns[i].selected){
						if(items[i]){
							items[i].setSelected(true);
						}
					}
				}
			}

			// // Multi-select if required
			// //	var bMultiSelect = !!oEvent.getSource().data("multi");
			// var bMultiSelect = !!oEvent.getSource().data();
			// this._oDialogSettings.setMultiSelect(bMultiSelect);

			// Remember selections if required
			// var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogSettings.setRememberSelections(true);

			// // clear the old search filter
			// this._oDialogSettings.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogSettings);
			
			// jQuery.sap.syncStyleClass("sapMSLIIconThumb", this.getView(), this._oDialogSettings);
			this._oDialogSettings.open();
		},
		
		handleConfirmColumn: function(oEvent){
			
			var aContexts = oEvent.getParameter("selectedContexts");
			
			this.resetColumnsModel();
			
			if (aContexts && aContexts.length) {
				
				var oColumns = this.byId("idTable").getColumns();

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					oSel.selected = true;
					var sel = aContexts[i].getPath().split("/FieldCollection/")[1];
					
					var created=false;
					for(var j in oColumns){
						if(oColumns[j].data().fname===oSel.fname){
							oColumns[j].setVisible(true);
							created=true;
							break;
						}
					}
					if(!created){
						var oColumn = new sap.ui.table.Column({
									width:"11rem",
									filterProperty:oSel.fname,
									label: new sap.m.Label({
		                    			text: oSel.text
		                			}), 
		                			visible: true,
		                			template: new sap.m.Text({
					                					text:"{"+oSel.fname+"}",
					                					wrapping:false
					                				})
								});
						oColumn.data("fname",oSel.fname);
						
						var oTable = this.byId("idTable");
						oTable.addColumn( oColumn );
					}
				}
			}
			// this._oDialogSettings.close();
		},
		handleCloseColumn: function(){
			// this._oDialogSettings.close();
		},
		
		resetColumnsModel: function(){
			for(var i in this.columns){
				this.columns[i].selected = false;
			}
			var oColumns = this.byId("idTable").getColumns();
			for(var i in oColumns){
				oColumns[i].setVisible(false);
			}
		},
		
		onSaveSettings: function(oEvent){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/DuzenSet";
			
			var tableColumn = [];
			// tableColumn.push({FieldName:"Art",Width:"10rem"});
			
			var oColumns = this.byId("idTable").getColumns();
			for(var i in oColumns){
				if(oColumns[i].getVisible()){
					tableColumn.push({ FieldName: oColumns[i].data().fname,
									   Width: oColumns[i].getWidth() });
				}
			}
			
			var oPostData = {
				TableName:"1",
				Return:"",
				Message:"",
				TableColumnSet:tableColumn
			};
			// return;
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show(oData.Message);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},
		
		readTableCustom: function(){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/DuzenSet(TableName='1')";
			
			oModel.read(sPath, {
				urlParameters: {
	                "$expand": "TableColumnSet"
	            },
				success: function(oData, response) {
					t.setColums(oData.TableColumnSet.results);
				},
				error: function(oError) {
				}
			});
		},

		_onObjectMatched: function(oEvent) {

			var sObjectId = oEvent.getParameter("arguments").objectId;
			this.getModel().metadataLoaded().then(function() {
				var sObjectPath = this.getModel().createKey("EtQalsSet", {
					Prueflos: sObjectId
				});
				this._bindView("/" + sObjectPath);
			}.bind(this));

		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * After list data is available, this handler method updates the
		 * master list counter and hides the pull to refresh control, if
		 * necessary.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the master list object counter after new data is loaded
			this._updateListItemCount(oEvent.getParameter("total"));
			// hide pull to refresh if necessary
			this.byId("pullToRefresh").hide();

		},

		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function() {
			this._oList.getBinding("items").refresh();
		},

		/**
		 * Event handler for the list selection event
		 * @param {sap.ui.base.Event} oEvent the list selectionChange event
		 * @public
		 */
		onSelectionChange: function(oEvent) {

		},

		/**
		 * Event handler for navigating back.
		 * It there is a history entry or an previous app-to-app navigation we go one step back in the browser history
		 * If not, it will navigate to the shell home
		 * @public
		 */
		onNavBack: function() {

			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				history.go(-1);
			} else {
				this.getRouter().navTo("homepage", {}, true);
			}

		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		_createViewModel: function() {},

		/**
		 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
		 * @param {string} sFilterBarText the selected filter value
		 * @private
		 */
		_updateFilterBar: function(sFilterBarText) {},

		onAfterRendering: function(oEvent) {
			//debugger;

		}

	});

});