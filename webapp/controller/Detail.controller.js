/*global location */
sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
			"sap/ui/model/Filter",
			"sap/m/MessageBox",
	"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";

		return BaseController.extend("com.nauticana.demo.controller.Detail", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				
			 
				
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});
				
				var oSonucGModel = new JSONModel({
				 enabled: false,
				 Zkk: null
				});
				var oKulKarModel = new JSONModel({
				 enabled: false
				});
				var oHataGModel = new JSONModel({
				 enabled: false
				});
				var oSonuclarModel = new JSONModel({
				 enabled: false
				});
				var oKararlarModel = new JSONModel({
				 enabled: false
				});
				var oHatalarModel = new JSONModel({
				 enabled: false
				});
				
				/*
				Z_EDIT_KNC	Sonuç Girişi	mhd-icontabbar-f1
				Z_EDIT_KK	Kullanım Kararı mhd-icontabbar-f5
				Z_EDIT_DEFT	Hata Girişi 	mhd-icontabbar-f3
				Z_DISP_KNC	Sonuçlar    	mhd-icontabbar-f2
				Z_DISP_KK	Kararlar        mhd-icontabbar-f6
				Z_DISP_DEFT	Hatalar 		mhd-icontabbar-f4    
				*/   
				this.setModel(oSonucGModel, "oSonucG");
				this.setModel(oKulKarModel, "oKulKar");
				this.setModel(oHataGModel, "oHataG");
				this.setModel(oSonuclarModel, "oSonuclar");
				this.setModel(oKararlarModel, "oKararlar");
				this.setModel(oHatalarModel, "oHatalar");
		
        
			   // this.getView().byId("mhd-icontabbar-f1").setModel(oSonucGModel,"oSonucG");
			   // this.getView().byId("mhd-icontabbar-f1").bindElement("oSonucG>/");

			
				this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

				this.setModel(oViewModel, "detailView");
			
				
			//	var oPayerModel = this.getOwnerComponent().getModel("ZkkModel");
		
				this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
			
			},
				onBeforeRendering: function (oEvent) {
			        //debugger;
			        
			        //	var Zkkmodel =	sap.ui.getCore().getModel("ZkkModel");
					//    var ZkkData = Zkkmodel.getData();
					    
					    var userDataModel =	sap.ui.getCore().getModel("UserAuthModel");
					    var userData = userDataModel.getData();

			        	var oSonucGModel =	this.getView().getModel("oSonucG");
			        	var oKulKarModel =	this.getView().getModel("oKulKar");
			        	var oHataGModel =	this.getView().getModel("oHataG");
			        	var oSonuclarModel =	this.getView().getModel("oSonuclar");
			        	var oKararlarModel =	this.getView().getModel("oKararlar");
			        	var oHatalarModel =	this.getView().getModel("oHatalar");

        				/*
						Z_EDIT_KNC	Sonuç Girişi	mhd-icontabbar-f1
						Z_EDIT_KK	Kullanım Kararı mhd-icontabbar-f5
						Z_EDIT_DEFT	Hata Girişi 	mhd-icontabbar-f3
						Z_DISP_KNC	Sonuçlar    	mhd-icontabbar-f2
						Z_DISP_KK	Kararlar        mhd-icontabbar-f6
						Z_DISP_DEFT	Hatalar 		mhd-icontabbar-f4    
						*/   
				
				//debugger;
						
						if(userData.ZEditKnc ==='X' ){
							oSonucGModel.setData("true");
						}
						if(userData.ZEditKk ==='X'){
							oKulKarModel.setData("true");
						}
						if(userData.ZEditDeft ==='X'){
							oHataGModel.setData("true");
						}
						if(userData.ZDispKnc ==='X'){
							oSonuclarModel.setData("true");
						}
						if(userData.ZDispKk ==='X'){
							oKararlarModel.setData("true");
						}
						if(userData.ZDispDeft ==='X'){
							oHatalarModel.setData("true");
						}
			//			if(ZkkData.Zkk ==='X'){
				//			oSonucGModel.setData("false");
			//				sap.m.MessageToast.show(" Kullanım Kararı Girilmiş, Sonuç Girişi Yapılamaz");
			//			}
						
				},
				onSonucGirisPress: function (oEvent) {
				 
				
    			
				},
				onAfterRendering: function(oEvent) {
				 //debugger;
				
	//		var	 EtInspoperModel =	sap.ui.getCore().getModel("EtInspoperModel");
			//var	 inspoperData = EtInspoperModel.getData();
				 
					
					
		 	   },
				onAttachUpload  : function (oEvent) {
					//debugger;
					 
				//	var value = oModel.getProperty("/ZQMBL_SCR_AUTH_TTSet/Uname");
					
				    //this.getOwnerComponent().getModel().read("/ZQMBL_SCR_AUTH_TTSet", "ZEditKnc");
				/*
				
					var oUserData = "";
					
					var usernameService = "/sap/bc/ui2/start_up"; //Standard service
					
					var HttpRequest = "";
					
					HttpRequest = new XMLHttpRequest();
					
					HttpRequest.onreadystatechange = function() {
					
					if (HttpRequest.readyState == 4 && HttpRequest.status == 200) {
					
					oUserData = JSON.parse(xmlHttp.responseText);
					
					}
					
					};
					
					HttpRequest.open( "GET", usernameService, false );
					
					var usernameFinal = oUserData.id
					//debugger; */
				/**
			    oModel.read("/ZQMBL_SCR_AUTH_TTSet", {
			      method:"GET",
			      success: function(data){
			      		var oYetki = new sap.ui.model.json.JSONModel();
						sap.ui.getCore().setModel(oYetki,"YetkiModel");
						oYetki.setData(data);
					//debugger;
						var a =	data.Uname;
						var b =	data.ZEditKnc;
						var c =	data.ZEditKk;
						var d =	data.ZEditDeft;
						var e =	data.ZDispKnc;
						var f =	data.ZDispKk;
						var g =	data.ZDispDeft;
			        
			      }, 
			      error: function(){
			      	
			    }});
			 
    
    			//debugger;
			    var sPath = "/ZQMBL_SCR_AUTH_TTSet";
				var x = oModel.read( sPath, null, null, true, fSuccess, fError);
				
				
					//If data is successfully read
					function fSuccess(productdata){
						var oYetki = new sap.ui.model.json.JSONModel();
						sap.ui.getCore().setModel(oYetki,"YetkiModel");
						oYetki.setData(productdata);
					
						var a =	productdata.Uname;
						var b =	productdata.ZEditKnc;
						var c =	productdata.ZEditKk;
						var d =	productdata.ZEditDeft;
						var e =	productdata.ZDispKnc;
						var f =	productdata.ZDispKk;
						var g =	productdata.ZDispDeft;
						//debugger;
					}
					//If fails to read data
					function fError(oEvent){
						//debugger;
					 console.log("Error occured while reading Product Data!");
					 
				};*/

					//var a = oEvent.getSource();	
						
			//	
					
			
				},
			
			onNavBack : function () {
					
					var oHistory = History.getInstance();
					var sPreviousHash = oHistory.getPreviousHash();
		
					if (sPreviousHash !== undefined) {
						window.history.go(-1);
					} else {
						var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
						oRouter.navTo("master", true);
					}
			},
			createGroupHeader : function () {
				
			},
			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Event handler when the share by E-Mail button has been clicked
			 * @public
			 */
			onShareEmailPress : function () {
				var oViewModel = this.getModel("detailView");

				sap.m.URLHelper.triggerEmail(
					null,
					oViewModel.getProperty("/shareSendEmailSubject"),
					oViewModel.getProperty("/shareSendEmailMessage")
				);
			},

			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress : function () {
				var oViewModel = this.getModel("detailView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name : "sap.collaboration.components.fiori.sharing.dialog",
						settings : {
							object :{
								id : location.href,
								share : oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});

				oShareDialog.open();
			},
 
	//	filterByInspoper : function () {
 	//		var 	path = 'countryId',
    //		var 	operator = "EQ",
	//		var 	value1 =  '10'
	//	 },
			/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */

			/**
			 * Binds the view to the object path and expands the aggregated line items.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
			
		//
			       //  var ZkkData = Zkkmodel.getData();
		//		 sap.m.MessageToast.show("Survey successfully saved", {
		//				duration: 3000
		//				});
			
			//	var userData = userDataModel.getData();
			 var sObjectId =  oEvent.getParameter("arguments").objectId;
		 	 this.getOwnerComponent().getModel().metadataLoaded().then( function() {
					var sObjectPath = this.getOwnerComponent().getModel().createKey("EtQalsSet", {
						Prueflos :  sObjectId
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
				 /*
				this.getView().bindElement({
        			path: "/" + oEvent.getParameter("arguments").sObjectPath,
        			Prueflos: sObjectId
				  });*/
			},

			/**
			 * Binds the view to the object path. Makes sure that detail view displays
			 * a busy indicator while data for the corresponding element binding is loaded.
			 * @function
			 * @param {string} sObjectPath path to the object to be bound to the view.
			 * @private
			 */
			_bindView : function (sObjectPath) {
				//debugger;
				// Set busy indicator during view binding
				var oViewModel = this.getModel("detailView");
		 		// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				oViewModel.setProperty("/busy", false);

				this.getView().bindElement({
					path : sObjectPath,
					events: {
						change : this._onBindingChange.bind(this),
						dataRequested : function () {
							oViewModel.setProperty("/busy", true);
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
			},

			_onBindingChange : function () {
				//debugger;
				var oView = this.getView(),
					oElementBinding = oView.getElementBinding();

				// No data for the binding
				if (!oElementBinding.getBoundContext()) {
					this.getRouter().getTargets().display("detailObjectNotFound");
					// if object could not be found, the selection in the master list
					// does not make sense anymore.
					this.getOwnerComponent().oListSelector.clearMasterListSelection();
					return;
				}

				var sPath = oElementBinding.getPath(),
					oResourceBundle = this.getResourceBundle(),
					oObject = oView.getModel().getObject(sPath),
					sObjectId = oObject.Prueflos,
					sObjectName = oObject.Matnr,
					oViewModel = this.getModel("detailView");

				this.getOwnerComponent().oListSelector.selectAListItem(sPath);

				oViewModel.setProperty("/saveAsTileTitle",oResourceBundle.getText("shareSaveTileAppTitle", [sObjectName]));
				oViewModel.setProperty("/shareOnJamTitle", sObjectName);
				oViewModel.setProperty("/shareSendEmailSubject",
					oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
				oViewModel.setProperty("/shareSendEmailMessage",
					oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
			},

			_onMetadataLoaded : function () {
			 //debugger;
			//	var Zkkmodel =	this.getOwnerComponent().getModel("ZkkModel");
			    // var ZkkData = Zkkmodel.getData();
		//		var metadata = oPayerModel.getServiceMetadata();
			 
	//			 sap.m.MessageToast.show("Survey successfully saved", {
		//				duration: 3000
	//			});
			
				
				// Store original busy indicator delay for the detail view
				var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
					oViewModel = this.getModel("detailView");

				// Make sure busy indicator is displayed immediately when
				// detail view is displayed for the first time
				oViewModel.setProperty("/delay", 0);

				// Binding the view will set it to not busy - so the view is always busy if it is not bound
				oViewModel.setProperty("/busy", true);
				// Restore original busy indicator delay for the detail view
				oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
			}

		});

	}
);