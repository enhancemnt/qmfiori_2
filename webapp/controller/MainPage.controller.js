sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/unified/DateRange",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/nauticana/demo/util/Util"
], function(Controller, BaseController, JSONModel, DateRange, Filter, FilterOperator, Util) {

	"use strict";

	return Controller.extend("com.nauticana.demo.controller.MainPage", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.MainPage
		 */
			onInit: function() {
        		this.readTableCustom();
				Util.readCustomising();
			},

		onPressKontrol: function(oEvent) {
			var nextPage = sap.ui.getCore().getModel("UserAuthModel");
			if (nextPage) {
				this.getOwnerComponent().getRouter().navTo("homepage", true);
			}

		},
		
		onPressBildirim: function(oEvent) {

			var nextPage = sap.ui.getCore().getModel("UserAuthModel");

			if (nextPage) {
				this.getOwnerComponent().getRouter().navTo("notif", true);
			}

		},
		onPressKalite: function(oEvent) {
			var nextPage = sap.ui.getCore().getModel("UserAuthModel");
			if (nextPage) {
				this.getOwnerComponent().getRouter().navTo("homepagekalite", true);
			}
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.MainPage
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.MainPage
		 */
		onAfterRendering: function() {

			// var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");
			// var ofilter = [new Filter("IvTip", FilterOperator.EQ, "1")];
			// var sPath1 = "/ZQMBL_SCR_AUTH_TTSet";

			// oModel.read(sPath1, {
			// 	filters: ofilter,
			// 	success: function(oData, response) {
			// 		var UnameSet = {
			// 			Uname: oData.results[0].Uname,
			// 			ZEditKnc: oData.results[0].ZEditKnc,
			// 			ZEditKk: oData.results[0].ZEditKk,
			// 			ZEditDeft: oData.results[0].ZEditDeft,
			// 			ZDispKnc: oData.results[0].ZDispKnc,
			// 			ZDispKk: oData.results[0].ZDispKk,
			// 			ZDispDeft: oData.results[0].ZDispDeft,
			// 			ZIvTip: oData.results[0].IvTip
			// 		};
			// 		//debugger;
			// 		var oJsonModel = new sap.ui.model.json.JSONModel();
			// 		oJsonModel.setData(UnameSet);
			// 		sap.ui.getCore().setModel(oJsonModel, "UserAuthModel");

			// 		//console.log(UnameSet);

			// 	},
			// 	error: function(oError) {
			// 		// console.log("Error" +oError.responseText) 
			// 	}
			// });
			//debugger;

			//var ofilter = [new Filter("IvTip", FilterOperator.EQ, "1")];
			// var sPath2 = "/EtNotifAuthSet";
			// //debugger;
			// oModel.read(sPath2, {
			// 	//	filters : ofilter,
			// 	success: function(oData, response) {
			// 				var UnameSet = {
			//         			Uname : oData.results[0].Uname,
			//         			ZEditKnc : 	oData.results[0].ZEditKnc,
			// 					ZEditKk :	oData.results[0].ZEditKk,
			// 				    ZEditDeft :	oData.results[0].ZEditDeft,
			// 					ZDispKnc : oData.results[0].ZDispKnc,
			// 					ZDispKk : oData.results[0].ZDispKk,
			// 					ZDispDeft : oData.results[0].ZDispDeft,
			// 					ZIvTip : oData.results[0].IvTip
			// 		       	};
			// 		       	//debugger;
			//         		var oJsonModel = new sap.ui.model.json.JSONModel();
			//             	oJsonModel.setData(UnameSet);
			//         		sap.ui.getCore().setModel(oJsonModel,"UserAuthModel");
					
			//           //console.log(UnameSet);
            		 
   // 					}, 
			// 		error : function(oError){ 
			// 	    	// console.log("Error" +oError.responseText) 
			// 	    }
   // 			});
    			//debugger;
    			
  //  			//var ofilter = [new Filter("IvTip", FilterOperator.EQ, "1")];
		// 		var sPath2 = "/EtNotifAuthSet";
		// //debugger;
		// 		oModel.read(sPath2, { 
  //  			//	filters : ofilter,
		// 			success : function(oData,response)
		// 		{
		// 			var EtNotifAuthSet = {
		// 				Uname: oData.results[0].Uname,
		// 				Qmart: oData.results[0].Qmart,
		// 				CreateNot: oData.results[0].CreateNot,
		// 				ChangeNot: oData.results[0].ChangeNot,
		// 				DisplayNot: oData.results[0].DisplayNot
		// 			};
		// 			//debugger;
		// 			var oJsonModel = new sap.ui.model.json.JSONModel();
		// 			oJsonModel.setData(EtNotifAuthSet);
		// 			sap.ui.getCore().setModel(oJsonModel, "EtNotifAuthModel");

		// 			//console.log(EtNotifAuthSet);

		// 		},
		// 		error: function(oError) {
		// 			// console.log("Error" +oError.responseText) 
		// 		}
		// 	});

			// //var ofilter = [new Filter("IvTip", FilterOperator.EQ, "1")];
			// var sPath3 = "/NotifAuthSet";
			// //debugger;
			// oModel.read(sPath3, {
			// 	//	filters : ofilter,
			// 	success: function(oData, response) {
			// 				var EtNotifAuthSet = {
			//         			Uname : oData.results[0].Uname,
			//         			Qmart : 	oData.results[0].Qmart,
			// 					CreateNot :	oData.results[0].CreateNot,
			// 				    ChangeNot :	oData.results[0].ChangeNot,
			// 				    DisplayNot :	oData.results[0].DisplayNot
			// 		       	};
			// 		       	//debugger;
			//         		var oJsonModel = new sap.ui.model.json.JSONModel();
			//             	oJsonModel.setData(EtNotifAuthSet);
			//         		sap.ui.getCore().setModel(oJsonModel,"EtNotifAuthModel");
					
			//           //console.log(EtNotifAuthSet);
            		 
   // 					}, 
			// 		error : function(oError){ 
			// 	    	// console.log("Error" +oError.responseText) 
			// 	    }
   // 			});

			// oModel.attachRequestSent(function() {
			// 	sap.ui.core.BusyIndicator.show(10);
			// });
			// oModel.attachRequestCompleted(function() {
			// 	sap.ui.core.BusyIndicator.hide();
			// });

		},

		readTableCustom: function() {
			var util = Util;
			Util.readTableCustom();
		},

	});

});