sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/unified/DateRange",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller,BaseController,JSONModel,DateRange,Filter,FilterOperator) {
	"use strict";
	return Controller.extend("com.nauticana.demo.controller.BLListeleSec", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLListele
		 */
			onInit: function() {
				
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({pattern: "yyyy-MM-dd", calendarType: sap.ui.core.CalendarType.Gregorian});
	 	
	 		var dateFrom = "";
			var dateTo = "";
			
			var cModel = new JSONModel();
			cModel.setData({
					firstDate: dateFrom,
					secondDate: dateTo
			});
			
			this.getView().setModel(cModel,"calModel");
			
			var mBLFilters = new JSONModel();
				
			mBLFilters.setData({
				checkbox0:null,
        		checkbox1:null,
        		bilturu:null,
        		bildirim:null,
        		Matnr: null,
        		firstDate: null,
				secondDate: null,
				Idate: null
			});
			
		//	this.getView().setModel(mBLFilters,"mBLFilters");
			
			sap.ui.getCore().setModel(mBLFilters,"mBLFilters");
			this._iEvent = 0;
			},
			
			
			onValueHelpQmart : function(oEvent) {
					//debugger;
			if (!this._oDialogQmart) {
				this._oDialogQmart = sap.ui.xmlfragment("com.nauticana.demo.view.DialogQmart", this);
				this._oDialogQmart.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogQmart.setModel(this.getView().getModel());
			}
 
			// Multi-select if required
		//	var bMultiSelect = !!oEvent.getSource().data("multi");
		    var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogQmart.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogQmart.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogQmart.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogQmart);
			this._oDialogQmart.open();
				
			},
		 
			handleSearchQmart: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Qmart", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleCloseQmart: function(oEvent) {
		 
	 //debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oInput = this.getView().byId("__bilturu");
      
    		var aTokens = [];
    
			if (aContexts && aContexts.length) {
				
				for(var i = 0; i < aContexts.length; i++){
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
				    var token1 = new sap.m.Token({key: oSel.Qmart,text: oSel.Qmart});
				    aTokens[i]  = token1 ;
				}
			    oInput.setTokens(aTokens);
			} else {
			
			//	MessageToast.show("No new item was selected.");
		
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		
		_getQmartNumbers: function() {
			//debugger;
		    var aTokens = this.byId("__bilturu").getTokens();//ITERATE THROUGH EACH VALUES ENTERED BY USER
		     if (aTokens.length !== 0){
		    var aQmartFilter = [];
		    for (var i in aTokens) {
		        aQmartFilter.push(new sap.ui.model.Filter("Qmart", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey())); 
		    }
		    var oFilter = new sap.ui.model.Filter({ filters: aQmartFilter, and: false });
		     }else{
			 	var aValue = this.byId("__bilturu").getValue();
			 	var sfilterQmart =  new sap.ui.model.Filter("Qmart", sap.ui.model.FilterOperator.EQ, aValue);
			 	oFilter = sfilterQmart;
			 }
		    return oFilter;
		}
		,
			onListele : function() {
				//debugger; 
				
				if(this.getView().byId("__bilturu").getTokens() == "" || this.getView().byId("__bilturu").getTokens()  == null ) {

                           alert("Bildirim Türü Seçiniz");

				}else{
				      var dateid = this.getView().byId("__dateID").getSelectedKey();
			          var checkbox0 = this.getView().byId("__checkbox0").getProperty("selected");
			          var checkbox1 = this.getView().byId("__checkbox1").getProperty("selected");
			          var bilturu = this._getQmartNumbers(); 
			          var bildirim = this.getView().byId("__bildirim").getValue();  
			          var firstDateid =  this.getView().getModel("calModel").getData().firstDate;
			          var secondDateid =  this.getView().getModel("calModel").getData().secondDate;
			          var oMaterialFilter = this._getMatNumbers();

					 sap.ui.getCore().getModel("mBLFilters").setData({
			        	
			        		checkbox0:checkbox0,
			        		checkbox1:checkbox1,
			        		bilturu:bilturu,
			        		bildirim:bildirim,
			        		Matnr: oMaterialFilter,
			        		firstDate: firstDateid,
							secondDate: secondDateid,
							Idate: dateid
					
							  
			            });
   
					this.getOwnerComponent().getRouter().navTo("bllistesi",true);
				}
          
			},
			
		onValueHelpMatnr: function (oEvent) {
			if (!this._oDialogBL) {
				this._oDialogBL = sap.ui.xmlfragment("com.nauticana.demo.view.DialogBLMatnr", this);
				this._oDialogBL.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogBL.setModel(this.getView().getModel());
			}
		 
			// Multi-select if required
		//	var bMultiSelect = !!oEvent.getSource().data("multi");
		    var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogBL.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogBL.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogBL.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogBL);
			this._oDialogBL.open();
		},
		
		_getMatNumbers: function() {
			//debugger;
		    var aTokens = this.byId("__malzemeID3").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
		    if (aTokens.length != 0){
			    var aMatnrFilter = [];
			    
			    for (var i in aTokens) {
			        aMatnrFilter.push(new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey())); 
			    }
			    var oFilter = new sap.ui.model.Filter({ filters: aMatnrFilter, and: false });
			 }else{
			 	var aValue = this.byId("__malzemeID3").getValue();
			 	var sfilterMatnr =  new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.EQ, aValue);
			 	oFilter = sfilterMatnr;
			 }
	 
			  
		    return oFilter;
		},
		
			handleSearchMatnr: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Maktg", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		
		handleCloseBLMatnr: function(oEvent) {
	 
			var aContexts = oEvent.getParameter("selectedContexts");
			var oInput = this.getView().byId("__malzemeID3");
      
        	var aTokens = [];
    
			if (aContexts && aContexts.length) {
				
				for(var i = 0; i < aContexts.length; i++){
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
				    var token1 = new sap.m.Token({key: oSel.Matnr,text: oSel.Maktg});
				    aTokens[i]  = token1 ;
				}
			    oInput.setTokens(aTokens);
			} else {
			
			//	MessageToast.show("No new item was selected.");
		
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
			handleChange: function (oEvent) {
			var sFrom = oEvent.getParameter("from");
			var sTo = oEvent.getParameter("to");
			var bValid = oEvent.getParameter("valid");

			this._iEvent++;

			var oText = this.byId("TextEvent");
			oText.setText("Id: " + oEvent.getSource().getId() + "\nFrom: " + sFrom + "\nTo: " + sTo);

			var oDRS = oEvent.getSource();
			if (bValid) {
				oDRS.setValueState(sap.ui.core.ValueState.None);
			} else {
				oDRS.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onSelectChange : function (oEvent) {
			//debugger;
				var skey = oEvent.getParameter("selectedItem").mProperties.key;
				 
			    if(skey==="5"){
		 
			  	var oCalendarDialog = sap.ui.xmlfragment("com.nauticana.demo.view.Calendar", this);
			  	oCalendarDialog.setModel(this.getView().getModel("i18n"), "i18n");
				this.getView().addDependent(oCalendarDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
				oCalendarDialog.open();

			    }	
		},
			
		handleOkPress: function(oEvent) {
		   //debugger;
			var dateFrom = sap.ui.getCore().byId("selectedDateFrom").getText();
			var dateTo =  sap.ui.getCore().byId("selectedDateTo").getText();
		   
		   this.getView().getModel("calModel").setData({
				firstDate: dateFrom,
				secondDate: dateTo
            });
            
           	var oText = this.byId("__dateID");
	 	//	var oText2 = this.byId("__dateID2");
	 		oText.getList().getItems()[5].setText(dateFrom + " / " + dateTo);
		//	oText2.getList().getItems()[5].setText(dateFrom + "-" + dateTo);
			sap.ui.getCore().byId("Calendar").destroy();
	
	
		},
		
		handleCalendarSelect: function(oEvent) {
			var oCalendar = oEvent.oSource;
			this._updateText(oCalendar);
		},
		_updateText: function(oCalendar) {
			 
			var oSelectedDateFrom =   sap.ui.getCore().byId("selectedDateFrom");
			var oSelectedDateTo = sap.ui.getCore().byId("selectedDateTo");
			var aSelectedDates = oCalendar.getSelectedDates();
			var oDate;
			if (aSelectedDates.length > 0 ) {
				oDate = aSelectedDates[0].getStartDate();
				if (oDate) {
					oSelectedDateFrom.setText(this.oFormatYyyymmdd.format(oDate));
				} else {
					oSelectedDateTo.setText("No Date Selected");
				}
				oDate = aSelectedDates[0].getEndDate();
				if (oDate) {
					oSelectedDateTo.setText(this.oFormatYyyymmdd.format(oDate));
				} else {
					oSelectedDateTo.setText("No Date Selected");
				}
				
			} else {
				oSelectedDateFrom.setText("No Date Selected");
				oSelectedDateTo.setText("No Date Selected");
			}
		},

		handleSelectThisWeek: function(oEvent) {
			this._selectWeekInterval(6);
		},

		handleSelectWorkWeek: function(oEvent) {
			this._selectWeekInterval(4);
		},

		_selectWeekInterval: function(iDays) {
			var oCurrent = new Date();     // get current date
			var iWeekstart = oCurrent.getDate() - oCurrent.getDay() + 1;
			var iWeekend = iWeekstart + iDays;       // end day is the first day + 6
			var oMonday = new Date(oCurrent.setDate(iWeekstart));
			var oSunday = new Date(oCurrent.setDate(iWeekend));

			var oCalendar = this.getView().byId("calendar");

			oCalendar.removeAllSelectedDates();
			oCalendar.addSelectedDate(new DateRange({startDate: oMonday, endDate: oSunday}));

			this._updateText(oCalendar);
		},
		onExit : function () {
			if (this._oDialog) {
				this._oDialog.destroy();
			}
		},
		
			onNavBack : function() {

				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
					history.go(-1);
				} else {
					this.getRouter().navTo("master", {}, true);
				}
				
			},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLListele
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLListele
		 */
			onAfterRendering: function() {
		
			}

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLListele
		 */
		//	onExit: function() {
		//
		//	}

	});

});