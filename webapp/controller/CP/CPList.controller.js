/*global history */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/util/Util",
	"sap/ui/table/Column"
], function(BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState,Column,Util) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.CP.CPList", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/*
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function(oEvent) { 
			//  filtering  
			this._oView = this.getView();
			
			var fragmentId = this.getView().createId("idTableFragment");
			this.oTable = sap.ui.core.Fragment.byId(fragmentId, "idTable");

			this.setColumnsModel();
			this.readTableCustom();
			
			this.getRouter().getRoute("cplist").attachPatternMatched(this._onRouteMatched, this);
			// this.getRouter().attachBypassed(this.onBypassed, this);

		},
		
		onBeforeRendering: function(oEvent) {
			
		},
		
		_onRouteMatched: function() {
			if(this.RouteFlag){
				this.RouteFlag = false;
				return;
			}
			if(!this.getModel("mFilters").getData().Idate){
				return;
			}
			var sValDate = this.getModel("mFilters").getData().Idate;
			var sValFirstDate = this.getModel("mFilters").getData().firstDate;
			var sValSecondDate = this.getModel("mFilters").getData().secondDate;
			var sValStat = this.getModel("mFilters").getData().Stat35;

			var sFilterMatnr = this.getModel("mFilters").getData().Matnr;
			var sFilterWerk = this.getModel("mFilters").getData().Werk;
			var sFilterCharg = this.getModel("mFilters").getData().Charg;
			var sFilterArt = this.getModel("mFilters").getData().Art;
			var sFilterLagort = this.getModel("mFilters").getData().Lagortchrg;

			var sIvTip = "1";
			var sPathIvTip = "IvTip";
			var sPathDate = "Idate";
			var sPathStat = "Stat35";

			if (sValDate === "5") {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.BT, sValFirstDate, sValSecondDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});
			} else {
				//debugger;
				var sfilterDate = new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.EQ, sValDate);
				var sfilterStat = new sap.ui.model.Filter(sPathStat, sap.ui.model.FilterOperator.EQ, sValStat);
				var sFilterIvTip = new sap.ui.model.Filter(sPathIvTip, sap.ui.model.FilterOperator.EQ, sIvTip);

				var oDataFilter = new sap.ui.model.Filter({
					filters: [sfilterDate, sFilterMatnr, sFilterWerk, sFilterCharg, sFilterLagort, sFilterArt, sfilterStat, sFilterIvTip],
					and: true
				});

			}

			var binding = this.byId("list").getBinding("items");
			binding.filter(oDataFilter, true);
			
			var binding2 = this.oTable.getBinding("rows");
			binding2.filter(oDataFilter, true);
			
		},

		onExit: function() {
			if (this._oDialogInspoper) {
				this._oDialogInspoper.destroy();
			}
			if (this._getDialogInspoperSonuc) {
				this._getDialogInspoperSonuc.destroy();
			}
		},

		onSearch: function(oEvent) {

			if (oEvent.getParameters().refreshButtonPressed) {
				this.onRefresh();
				return;
			}

			var sQuery = oEvent.getParameter("query");

			var aFilters = [];

			if (sQuery && sQuery.length > 0) {
				aFilters.push(new Filter("Matnr", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Losmenge", FilterOperator.Contains, sQuery));
				aFilters.push(new Filter("Mengeneinh", FilterOperator.Contains, sQuery));
				var oFilter = new Filter({
					filters: aFilters,
					and: false
				});

				this._oListFilterState = oFilter;

			} else {
				oFilter = null;
				this._oListFilterState = oFilter;
			}
			// update list binding
			var oBinding = this._oTable.getBinding("items");
			oBinding.filter(oFilter, "Application");
			this._applyFilterSearch();
		},

		onRefresh: function() {
			this._oList.getBinding("items").refresh();
		},


		onSelectionChange: function() {
		
		},

		onNavBack: function() {
			history.go(-1);
		},
				
//------------------------------- Search ----------------------------------------------//
    onSearchLiveChange: function(oEvent){
        var value = oEvent.getSource().getValue();
        // this.searchLiveChange(value);
    },
    
    searchLiveChange: function(value){
        var oF1 = new sap.ui.model.Filter("Ktextmat", sap.ui.model.FilterOperator.Contains, value);
        var oFilters = new sap.ui.model.Filter({
            filters: [
                oF1
            ],
            and: false
        });
        this.oTable.getBinding("rows").filter(oFilters, sap.ui.model.FilterType.Application);

    },
		
//-------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------------  Results  --------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//
		KPSonuclar: function(oEvent){
			var bReplace = !Device.system.phone;
			this.RouteFlag = true;
			this.getRouter().navTo("qrdetail", {
				objectId: this.selObj.Prueflos,
			}, false);
		},
		
		KPSonucGiris: function(oEvent){
			var bReplace = !Device.system.phone;
			if(this.selObj.Zkk==="X"){
				sap.m.MessageToast.show("Kullanım kararı girilmiş, sonuç girişi yapılamaz");
				return;
			}
			
			this.RouteFlag = true;
			this.getRouter().navTo("cpaddresults", {
				prueflos: this.selObj.Prueflos,
			}, false);
		},
		
		KPKararlar: function(oEvent){
			var bReplace = !Device.system.phone;
			this.RouteFlag = true;
			this.getRouter().navTo("cpkararlar", {
				objectId: this.selObj.Prueflos,
				mode:"Display",
				art:this.selObj.Art,
				werks:this.selObj.Werk
			}, false);
		},
		
		KPKararGiris: function(oEvent){
			if(this.selObj.Zkk){
				sap.m.MessageToast.show("Kullanım kararı girilmiştir, karar girişi yapılamaz");
				return;
			}
			
			var bReplace = !Device.system.phone;
			this.RouteFlag = true;
			this.getRouter().navTo("cpkararlar", {
				objectId: this.selObj.Prueflos,
				mode:"Create",
				art:this.selObj.Art,
				werks:this.selObj.Werk
			}, false);
		},
		
		KPHataGiris: function(oEvent){
			if(this.selObj.Zkk){
				sap.m.MessageToast.show("Kullanım kararı girilmiştir, hata girişi yapılamaz");
				return;
			}
			
			var bReplace = !Device.system.phone;
			this.RouteFlag = true;
			this.getRouter().navTo("cperrorcreate", {
				objectId: this.selObj.Prueflos,
				matnr:    this.selObj.Matnr,
				art:      this.selObj.Art
			}, false);
		},
		
		KPHatalar: function(oEvent){
			var bReplace = !Device.system.phone;
			this.RouteFlag = true;
			this.getRouter().navTo("cperrordisplay", {
				objectId: this.selObj.Prueflos,
				matnr: this.selObj.Matnr
			}, false);
		},
		
//-------------------------------------------------------------------------------------------------------------------------------//
//-------------------------------------------  TableSelectDialog  ---------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//
		setColumnsModel: function(){
			
			this.columns = []; 
		    this.columns.push({	fname:	"Ktextmat",   text: "Nesne kısa mtn.",	selected:false });//true
		    this.columns.push({	fname:	"Prueflos",   text: "Kontrol partisi",	selected:false });
		    this.columns.push({	fname:	"Werk",       text: "Üretim yeri",		selected:false });
		    this.columns.push({	fname:	"Art",        text: "Kontrol türü", 	selected:false });
		    this.columns.push({	fname:	"Herkunft",   text: "Parti kaynağı",	selected:false });
		    this.columns.push({	fname:	"Stsma",      text: "Durum şeması", 	selected:false });
		    this.columns.push({	fname:	"Stat35",     text: "Kullanım kararı",	selected:false });
		    this.columns.push({	fname:	"Stprver",    text: "Örnekleme ynt.",	selected:false });
		    this.columns.push({	fname:	"Enstehdat",  text: "Parti yaratıldı",	selected:false });
		    this.columns.push({	fname:	"Entstezeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Ersteller",  text: "Yaratan",			selected:false });
		    this.columns.push({	fname:	"Ersteldat",  text: "Yaratma tarihi",	selected:false });
		    this.columns.push({	fname:	"Erstelzeit", text: "Saat", 			selected:false });
		    this.columns.push({	fname:	"Aufnr",      text: "Şipariş",			selected:false });
		    this.columns.push({	fname:	"Kunnr",      text: "Müşteri",			selected:false });
		    this.columns.push({	fname:	"Lifnr",      text: "Satıcı",			selected:false });
		    this.columns.push({	fname:	"Hersteller", text: "Üretici",			selected:false });
		    this.columns.push({	fname:	"Matnr",      text: "Malzeme",			selected:false });//true
		    this.columns.push({	fname:	"Charg",      text: "Parti",			selected:false });
		    this.columns.push({	fname:	"Lagortchrg", text: "Depo yeri",		selected:false });
		    this.columns.push({	fname:	"Ebeln",      text: "Satınalma blg",	selected:false });
		    this.columns.push({	fname:	"Mjahr",      text: "Mlz.belge yılı",	selected:false });
		    this.columns.push({	fname:	"Mblnr",      text: "Malzeme belgesi",  selected:false });
		    this.columns.push({	fname:	"Budat",      text: "Kayıt tarihi", 	selected:false });
		    this.columns.push({	fname:	"Bwart",      text: "İşlem türü",		selected:false });
		    this.columns.push({	fname:	"Lgnum",      text: "Depo numarası",	selected:false });
		    this.columns.push({	fname:	"Losmenge",   text: "Kontrol prt.mkt",	selected:false });//true
		    this.columns.push({	fname:	"Mengeneinh", text: "Temel ölçü brm.",	selected:false });//true
		    this.columns.push({	fname:	"Einhprobe",  text: "Örnekleme ÖB", 	selected:false });
		    this.columns.push({	fname:	"Idate",      text: "100 karakter", 	selected:false });
		    this.columns.push({	fname:	"Zkk",        text: "X Flag",			selected:false });
		    this.columns.push({	fname:	"IvTip",      text: "Versiyon numarası bileşenleri", selected:false });
			
		},
		
		readTableCustom: function(){
			var util = Util;
			util.getNotifTable(this.setColums,this,"1");
		},
		
		setColums: function(items,that){
			
			var t = that;
				
			// var oTable = t.byId("idTable");
			for(var i in items){
				
				var t = that;
				var line = t.readTable(t.columns,"fname",items[i].FieldName);
				line.selected=true;
				
				if(line.fname==="Ersteldat" || line.fname==="Enstehdat"){
					var oText =  new sap.m.Text({
	                					text: "{path:'"+line.fname+"', formatter:'com.nauticana.demo.model.formatter.formateDate'}",
	                					wrapping:false
	                				});
				}else if(line.fname==="Entstezeit" || line.fname==="Erstelzeit"){
					 oText =  new sap.m.Text({
	            					text: "{path:'"+line.fname+"', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
	            					wrapping:false
	            				});
				}else{
					oText =  new sap.m.Text({
                					text:"{"+line.fname+"}",
                					wrapping:false
                				});
				}
				
				var oColumn = new sap.ui.table.Column({
									width:items[i].Width,
									filterProperty:line.fname,
									label: new sap.m.Label({
		                    			text: line.text
		                			}), 
		                			visible: true,
		                			template: oText
								});
				oColumn.data("fname",line.fname);
				t.oTable.addColumn( oColumn );
			}
			// t.oTable.attachColumnSelect(function(){t.deneme();});
			t.oTable.attachRowSelectionChange(function(oEvent){t.rowSelected(oEvent);});
		},
		
		rowSelected: function(oEvent){
			// var src = oEvent.getSource();
			// var index = oEvent.getParameter("rowIndex");
			// if(index!==-1){
			// 	var selRow = src.getRows()[index];
			// 	this.selObj = selRow.getBindingContext().getObject();
			// }
			var aContexts = oEvent.getParameter("rowContext");
			this.selObj = aContexts.oModel.getProperty(aContexts.getPath());
		},
		
		onSettings: function(oEvent){
			if (!this._oDialogSettings) {
				this._oDialogSettings = sap.ui.xmlfragment("com.nauticana.demo.view.fragments.Settings", this);
				// this._oDialogSettings = this.byId("idSettingsDialog");
				this._oDialogSettings.setModel(this.getView().getModel("i18n"), "i18n");
				var oModel = new sap.ui.model.json.JSONModel({
					FieldCollection:this.columns
				});
				this._oDialogSettings.setModel(oModel,"FieldModel");
				var items = this._oDialogSettings.getItems();
				for(var i in this.columns){
					if(this.columns[i].selected){
						if(items[i]){
							items[i].setSelected(true);
						}
					}
				}
				
			var button2 = new sap.m.Button();
			button2.setText("Tamam");
			var button3 = new sap.m.Button();
			button3.setText("İptal");
			}

			this._oDialogSettings.setRememberSelections(true);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogSettings);
			this._oDialogSettings.open();
		},
		
		handleConfirmColumn: function(oEvent){
			
			var aContexts = oEvent.getParameter("selectedContexts");
			
			this.resetColumnsModel();
			
			if (aContexts && aContexts.length) {
				
				var oColumns = this.oTable.getColumns();

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					oSel.selected = true;
					var sel = aContexts[i].getPath().split("/FieldCollection/")[1];
					
					var created=false;
					for(var j in oColumns){
						if(oColumns[j].data().fname===oSel.fname){
							oColumns[j].setVisible(true);
							created=true;
							break;
						}
					}
					if(!created){
						if(oSel.fname==="Ersteldat" || oSel.fname==="Enstehdat"){
							var oText =  new sap.m.Text({
			                					text: "{path:'"+oSel.fname+"', formatter:'com.nauticana.demo.model.formatter.formateDate'}",
			                					wrapping:false
			                				});
						}else if(oSel.fname==="Entstezeit" || oSel.fname==="Erstelzeit"){
							 oText =  new sap.m.Text({
			            					text: "{path:'"+oSel.fname+"', formatter:'com.nauticana.demo.model.formatter.timeFormat'}",
			            					wrapping:false
			            				});
						}else{
							oText =  new sap.m.Text({
		                					text:"{"+oSel.fname+"}",
		                					wrapping:false
		                				});
						}
						
						
						var oColumn = new sap.ui.table.Column({
									width:"11rem",
									filterProperty:oSel.fname,
									label: new sap.m.Label({
		                    			text: oSel.text
		                			}), 
		                			visible: true,
		                			template: oText
								});
						oColumn.data("fname",oSel.fname);
						
						// var oTable = this.byId("idTable");
						this.oTable.addColumn( oColumn );
					}
				}
			}
		},
		handleCloseColumn: function(){
		},
		
		resetColumnsModel: function(){
			for(var i in this.columns){
				this.columns[i].selected = false;
			}
			var oColumns = this.oTable.getColumns();
			for(var i in oColumns){
				oColumns[i].setVisible(false);
			}
		},
		
		onSaveSettings: function(oEvent){
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/DuzenSet";
			
			var tableColumn = [];
			// tableColumn.push({FieldName:"Art",Width:"10rem"});
			
			var oColumns = this.oTable.getColumns();
			for(var i in oColumns){
				if(oColumns[i].getVisible()){
					tableColumn.push({ FieldName: oColumns[i].data().fname,
									   Width: oColumns[i].getWidth() });
				}
			}
			
			var oPostData = {
				TableName:"1",
				Return:"",
				Message:"",
				TableColumnSet:tableColumn
			};
			// return;
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show(oData.Message);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},
		
		readTable: function(itab,fname,value){
			for(var i in itab){
				var line = itab[i];
				if(line[fname]===value){
					return line;
				}	
			}
			return null;
		}
		

	});

});