/*global history */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
		'sap/m/MessageBox',
	"sap/ui/core/mvc/Controller"
], function(Util, BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper,
	GroupSortState,MessageBox) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.CP.CPAddResults", {

		onInit: function(oEvent) {

			this.getRouter().getRoute("cpaddresults").attachPatternMatched(this.onRouteMatched, this);
			
			this.setIconLayout();
			Util.readCustomising();
		},
		
		setIconLayout: function(){
			var as = new sap.ui.layout.GridData();
			var ic = this.byId("idResultIcon");
			as.setSpan("L1 M1 S1");
			ic.setLayoutData(as);
		},

		onRouteMatched: function(oEvent) {
			var oArgs = oEvent.getParameter("arguments");
			this.prueflos = oArgs.prueflos;
			
			this.selItem=0;
			this.selChrItem=1;
			this.getData();
		},
		
		onNavBack: function() {
			history.go(-1);
		},
		
		getData: function(){
			
			this.QRItems=[];
			this.QRInsChar=[];
			this.QRSingleResults=[];
			this.QRInsCharTmp=[];
			this.destroyAdditionalResults();
			this.createSegmentButtons();
			
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/QualityReportListSet(Prueflos='"+this.prueflos+"')";
			
			Util.showBusy();
			oModel.read(sPath, {
				urlParameters: {
			                "$expand": "QRItem,QRInsChar,QRSingleResults"
			            },
				success: function(oData, response) {
					var jsonArray = [];
					t.QRItems   = oData.QRItem.results;
					t.QRInsChar = oData.QRInsChar.results;
					for(var i in t.QRInsChar){
						t.QRInsChar[i].Icon="";
						if(t.QRInsChar[i].CharType==="01"){
							
							t.QRInsChar[i].Result=t.QRInsChar[i].MeanValue;
							if(t.QRInsChar[i].Result!==""){
								t.QRInsChar[i].Icon=t.QRInsChar[i].Evaluation;
							}
						}else if(t.QRInsChar[i].CharType==="02"){
							t.QRInsChar[i].Result=t.QRInsChar[i].MeanValue;
							t.QRInsChar[i].Key=t.QRInsChar[i].Code1;//
							if(t.QRInsChar[i].Key!==""){
								t.QRInsChar[i].Icon=t.QRInsChar[i].Evaluation;
							}
						}else{
						}
						t.QRInsChar[i].additionalFieldsLength=0;
					}
					t.QRSingleResults = oData.QRSingleResults.results;
					var oJSONModel = new sap.ui.model.json.JSONModel({
						QRItems : t.QRItems
					});
					t.getView().setModel(oJSONModel);
					
					t.defaultResults();
					
					var oJSONModel = new sap.ui.model.json.JSONModel();
					t.getView().setModel(oJSONModel,"InsCharModel");
					if(t.QRItems.length>0){
						t.getView().getModel().setProperty("/QRSelectedItem",t.QRItems[t.selItem].Inspoper);
						t.selectedItem(t.QRItems[t.selItem].Inspoper);
					}
					
					Util.hideBusy();
				},
				error: function(oError) {
					Util.hideBusy();
				}
			});
			
			this.destroyAdditionalResults();
		},
		
		defaultResults: function(){
			for(var i in this.QRInsChar){
				if(this.QRInsChar[i].SingleRes==="X"){
					if(!this.QRInsChar[i].AddDefaultResultCreated){
						this.chrLine = this.QRInsChar[i];
						var length = this.getSingleLength();
						this.QRInsChar[i].additionalFieldsLength = parseInt(this.QRInsChar[i].Scope);
						if(this.QRInsChar[i].additionalFieldsLength<length){
							this.QRInsChar[i].additionalFieldsLength = length;
						}
						this.QRInsChar[i].AddDefaultResultCreated=true;
						this.setDefaultResults();
						// 	this.addDefaultResults(parseInt(this.chrLine.Scope));
					}
				}
				this.QRInsChar[i].buttonColor = this.checkAnyAddElement(this.QRInsChar[i]);
			}
		},
		
		onNextOperationPressed: function(oEvent){
			var length = this.QRItems.length;
			for(var i in this.QRItems){
				if(this.QRItems[i].Inspoper === this.item){
					var item = i;
					break;
				}
			}
			item=parseInt(item)+1;
			this.selItem = item;
			this.selChrItem=1;
			if(length>item){
				this.destroyAdditionalResults();
				this.getView().getModel().setProperty("/QRSelectedItem",this.QRItems[item].Inspoper);
				this.selectedItem(this.QRItems[item].Inspoper);
			}
			
		},
		
		onItemSelectionChange: function(oEvent){
			 var oSource = oEvent.getSource();
			 this.selItem = oSource.getSelectedIndex();
			 this.selChrItem=1;
			 this.selectedItem(oSource.getSelectedKey());
		},
		
		selectedItem: function(Inspoper){
			this.QRInsCharTmp = [];
			for(var i in this.QRInsChar){
				if(this.QRInsChar[i].Inspoper === Inspoper){
					this.QRInsCharTmp.push(this.QRInsChar[i]);	
				}
			}
			this.item = Inspoper;
			this.selectChr(this.selChrItem-1);
			this.createSegmentButtons();
			// this.getView().getModel("InsCharModel").setProperty("/QRInsChar",items);		
		},
		
		selectChr: function(sel){
			// this.getView().getModel("InsCharModel").setProperty("/QRInsChar",this.QRInsCharTmp[sel-1]);
			var line = this.QRInsCharTmp[sel];
			line.opened="X";
			line.Key="";
			this.chrLine = line; 
						//Enable...
			if(this.chrLine.SingleRes==="X"){
				if(!this.chrLine.AddDefaultResultCreated){
					var length = this.getSingleLength();
					line.additionalFieldsLength = parseInt(this.chrLine.Scope);
					if(line.additionalFieldsLength<length){
						line.additionalFieldsLength = length;
					}
					this.chrLine.AddDefaultResultCreated=true;
					this.setDefaultResults();
					// 	this.addDefaultResults(parseInt(this.chrLine.Scope));
				}
				
			}
			
			if(line.CharType==="01"){
				this.evaluateSingleResults(line);
			}else if(this.chrLine.SingleRes==="X"){
				this.setSingleIcon();
			}
				
			if(this.chrLine.SingleRes==="X"||this.chrLine.Closed==="X"){
				this.byId("idResult").setEnabled(false);	
			}else{
				this.byId("idResult").setEnabled(true);
			}
			
			this.getView().getModel("InsCharModel").setData(line);
			if(line.CharType==="01"){
				this.getView().getModel("InsCharModel").setProperty("/Limit",line.LwTolLmt+"-"+line.TargetVal+"-"+line.UpTolLmt);
				// this.byId("idResult").setType("Number");
				this.byId("idResult").setShowValueHelp(false);
			}else if(line.CharType==="02"){
				this.getView().getModel("InsCharModel").setProperty("/Limit",line.SelSet1);
				// this.byId("idResult").setType("Text");
				this.byId("idResult").setShowValueHelp(true);
			}else{
				// this.byId("idResult").setType("Text");
				this.byId("idResult").setShowValueHelp(false);
			}
			

			//Add Remove Buttons...
			if(this.chrLine.ScopeInd===""&&this.chrLine.SingleRes==="X"&&this.chrLine.Closed===""){
				this.byId("idAddButton").setEnabled(true);
				this.byId("idRemoveButton").setEnabled(true);
			}else{
				this.byId("idAddButton").setEnabled(false);
				this.byId("idRemoveButton").setEnabled(false);
			}
			
			this.getView().getModel("InsCharModel").setProperty("/CharDescr",line.CharDescr);
			this.getView().getModel("InsCharModel").setProperty("/CharDescr",line.CharDescr);
			this.getView().getModel("InsCharModel").setProperty("/Result",line.Result);
			// this.byId("idResult").setValue(line.Result);
			this.getView().getModel("InsCharModel").setProperty("/Icon",line.Icon);
			
			// Additional Results...
			this.destroyAdditionalResults();
			this.addFlag=false;
			var count = 0;
			for(var i=0;i<line.additionalFieldsLength;i++){
				count++;
				
				line.additionalFieldsId = count;
				this.addResultInput(line);
			}
		},
		
		evaluateSingleResults: function(line){
			if(line.additionalFieldsLength>0){
				if(line.CharType==="01"){
					var addValue = 0;
					var devide=0;
					for(var i=0;i<line.additionalFieldsLength;i++){
						var id = parseInt(i)+1;
						var fName = "addValue_"+line.Inspchar+"_"+id;
						var value = parseInt(line[fName]);
						if(value){
							addValue+=parseInt(line[fName]);
							devide++;
						}
					}
				}else if(line.CharType==="02"){
					
				}
				if(devide>0){
					line.Result = addValue/devide;
					line.Result = parseInt(line.Result);
				}
			}
			var icon = this.getIconColor(this,line.Result);
			line.Icon=icon;
		},
		
		onLiveChangeResult: function(oEvent){
			var oSource = oEvent.getSource();
			var value = oEvent.getParameters().value;
			if(this.chrLine.CharType==="01"){
				value = parseInt(value)?parseInt(value):"";
				this.onLiveChangeResult_01(oEvent,this);
				this.QRInsCharTmp[this.selChrItem-1].Result = value;
				oSource.setValue(value);
			}else{
				var result = this.QRInsCharTmp[this.selChrItem-1].Result;
				var lngth = result.length;
				var temp = result.substr(0,lngth-1);
				if(temp===value){
					this.QRInsCharTmp[this.selChrItem-1].Result="";
					this.QRInsCharTmp[this.selChrItem-1].Icon="";
					this.QRInsCharTmp[this.selChrItem-1].Key="";
					this.getView().getModel("InsCharModel").setProperty("/Icon","");
					this.getView().getModel("InsCharModel").setProperty("/Key","");		
				}
				var oSource = oEvent.getSource();
				oSource.setValue(this.QRInsCharTmp[this.selChrItem-1].Result);
			}
		},
		
		onLiveChangeResult2: function(oEvent,t){
			var value = oEvent.getParameters().value;
			var oSource = oEvent.getSource();
			var prop = "addValue_"+t.chrLine.Inspchar+"_"+oSource.id;
			var propKey = "addKey_"+t.chrLine.Inspchar+"_"+oSource.id;
			if(t.chrLine.CharType==="01"){
				t.onLiveChangeResult_01(oEvent,t);
				t.QRInsCharTmp[this.selChrItem-1][prop] = value;
				oSource.setValue(value);
				t.evaluateSingleResults(t.chrLine);
			}else{
				var result = this.QRInsCharTmp[this.selChrItem-1][prop];
				var lngth = result.length;
				var temp = result.substr(0,lngth-1);
				if(temp===value){
					this.QRInsCharTmp[this.selChrItem-1][prop]="";
					this.QRInsCharTmp[this.selChrItem-1][propKey]="";
					var propIcon = "addIcon_"+t.chrLine.Inspchar+"_"+oSource.id;
					this.QRInsCharTmp[this.selChrItem-1][propIcon]="";
					this.getView().getModel("InsCharModel").setProperty("/"+propIcon,"");
					this.getView().getModel("InsCharModel").setProperty("/"+propKey,"");	
				}
				oSource.setValue(t.QRInsCharTmp[this.selChrItem-1][prop]);
				t.setSingleIcon();
			}
		},
		
		onLiveChangeResult_01: function(oEvent,t){
			var value = oEvent.getParameters().value;
			// value = parseInt(value)?parseInt(value):"";
			// var valueInt = parseInt(value)?parseInt(value):0;
			value = parseFloat(value);
			var oSource = oEvent.getSource();
			var icon = t.getIconColor(t,value);
			
			if(oSource.id){
				var prop = "/addIcon_"+t.chrLine.Inspchar+"_"+oSource.id;
				t.getView().getModel("InsCharModel").setProperty(prop,icon);
				var asd = t.QRInsCharTmp[t.selChrItem-1];
				asd[prop] = icon; 
			}else{
				t.getView().getModel("InsCharModel").setProperty("/Icon",icon);
				t.QRInsCharTmp[t.selChrItem-1].Icon = icon;
				t.changeColorSegmentButton(t.selChrItem-1);
			}
		},
		
		getIconColor:function(t,value){
			var icon = "";
			var valueInt = parseInt(value)?parseInt(value):0;
			if(t.chrLine.LwTolLmt===""){
				var min = 0;
			}else{
				var min = parseInt(t.chrLine.LwTolLmt);
			}
			if(t.chrLine.UpTolLmt===""){
				var max = 0;
			}else{
				var max = parseInt(t.chrLine.UpTolLmt);
			}
			if(min==0&&max==0){
				min=max=parseInt(t.chrLine.TargetVal);
			}
			if(value===""){
				var icon = "";
			}else if(valueInt>=min && valueInt<=max){
				var icon = "A";
			}else if(valueInt>=min && max==0){
				var icon = "A";
			
			}else{
				var icon = "R";
			}
			return icon;
		},
		
//----------------------------------------------------------------------------------------------------------------//
		onDecBefore: function(oEvent){
			if(this.selChrItem > 1){
				this.selChrItem-=1;
				this.selectChr(this.selChrItem-1);
				this.selectSegmentButton(this.selChrItem-1);
			}
		},
		
		onDecNext: function(oEvent){
			var length = this.QRInsCharTmp.length;
			if(this.selChrItem < length){
				this.selChrItem+=1;
				this.selectChr(this.selChrItem-1);
				this.selectSegmentButton(this.selChrItem-1);
			}
		},
		
//-----------------------------------------------------------------------------------------------------------------//
	
		createSegmentButtons: function(){
			var t = this;
			var idSB = this.byId("idSegmentButtons");
			idSB.destroyItems();
			idSB.destroyButtons();
			for(var i in this.QRInsCharTmp){
				var sb = new sap.m.SegmentedButtonItem({
					text:parseInt(i)+1,
					press: function(oEvent){t.onBS(oEvent,t);}
				});
				if(i==="0"){
					// sb.addStyleClass("white");
					// sb.addStyleClass("selected");
				}
				idSB.addItem(sb);
			}
			this.selectSegmentButton(this.selChrItem-1);
		},
		
		selectSegmentButton: function(value){
			var sel = value +"";
			var idSB = this.byId("idSegmentButtons");
			var items =idSB.getButtons();
			for(var i in items){
				items[i].addCustomData().id=i;
				if(i===sel){
					items[i].addStyleClass("white");
					items[i].addStyleClass("selected");
					idSB.setSelectedButton(items[i].getId());
				}else{
					items[i].removeStyleClass("selected");
					items[i].removeStyleClass("green");
					items[i].removeStyleClass("yellow");
				}
				// var a = this.checkAnyAddElement(this.QRInsCharTmp[i]);
				if(this.QRInsCharTmp[i].buttonColor==="Completed"){//this.QRInsCharTmp[i].Icon==="A"
					items[i].removeStyleClass("white");
					items[i].removeStyleClass("selected");
					items[i].addStyleClass("green");
				}else if(this.QRInsCharTmp[i].buttonColor==="Continue"){//this.QRInsCharTmp[i].Icon==="R"
					items[i].removeStyleClass("white");
					items[i].removeStyleClass("selected");
					items[i].addStyleClass("yellow");
				}
			}
		},
		
		changeColorSegmentButton: function(value){
			return;
			var sel = value+"";
			var idSB = this.byId("idSegmentButtons");
			var icon = this.QRInsCharTmp[sel].Icon;
			var item = idSB.getButtons()[sel];
			
			if(this.QRInsCharTmp[sel].buttonColor==="Completed"){
				item.addStyleClass("green");
				item.removeStyleClass("white");
				item.removeStyleClass("yellow");
			}else if(this.QRInsCharTmp[sel].buttonColor==="Continue"){
				item.removeStyleClass("white");
				item.removeStyleClass("green");
				item.addStyleClass("yellow");
				
			}else{
				item.addStyleClass("white");
				item.removeStyleClass("green");
				item.removeStyleClass("yellow");
			}
		},
		
		onBS: function(oEvent,t){
			var source = oEvent.getSource();
			var id = source.oButton.id;
			this.selChrItem = parseInt(id) +1;
			this.selectChr(parseInt(id));
			this.selectSegmentButton(id);
		},
		
		checkAnyAddElement: function(line){
			if(line.additionalFieldsLength>0){
				for(var i = 0;i<line.additionalFieldsLength;i++){
					var id = parseInt(i)+1;
					if(line["addValue_"+line.Inspchar+"_"+id]!==""){
						var a = true;
					}else if(line["addKey_"+line.Inspchar+"_"+id]!==""){
						var a = true;
					}else{
						var empty = true;
					}
				}
				if(a&&!empty){
					return "Completed";
				}else if(a){
					return "Continue";
				}else{
					return "Empty";
				}
			}
			if((line.CharType==="01"&&line.Result!=="")||(line.CharType==="02"&&line.Key!=="")){
				return "Completed";
			}
			return "Empty";
		},

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------  Additional Results  -------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
		getSingleLength: function(){
			var count = 0;
			for(var i=0;i<this.QRSingleResults.length;i++){
				if(this.QRSingleResults[i].Inspoper===this.chrLine.Inspoper && this.QRSingleResults[i].Inspchar===this.chrLine.Inspchar){
					count++;
				}
			}
			return count;
		},
		
		setDefaultResults: function(){
			// this.chrLine
			for(var i=0;i<this.QRSingleResults.length;i++){
				if(this.QRSingleResults[i].Inspoper===this.chrLine.Inspoper && this.QRSingleResults[i].Inspchar===this.chrLine.Inspchar){
					var id = parseInt(this.QRSingleResults[i].ResNo);
					var addValueFname = "addValue_"+this.chrLine.Inspchar+"_"+id;
					var addIconFname  = "addIcon_"+this.chrLine.Inspchar+"_"+id;
					var addKeyFname  = "addKey_"+this.chrLine.Inspchar+"_"+id;
					
					this.chrLine[addValueFname] = this.QRSingleResults[i].OriginalInput;//
					this.chrLine[addIconFname]  = this.QRSingleResults[i].ResValuat;
					this.chrLine[addKeyFname]   = "";
					if(this.chrLine.CharType==="02"){
						this.chrLine[addKeyFname] = this.QRSingleResults[i].Code1;
					}
				}
			}
		},
		
		addDefaultResults:function(scope){
			for(var i = 0; i<scope; i++){
				this.onAdd();
			}
			this.chrLine.AddDefaultResultCreated=true;
		},

		destroyAdditionalResults: function(){
			var oAddtElemnt = this.byId("idAdditionalElement");
			oAddtElemnt.destroyFields();
			oAddtElemnt.setVisible(false);
		},
		
		onAdd: function(oEvent){
			// var oSource = oEvent.getSource();
			var line = this.QRInsCharTmp[this.selChrItem-1];
			line.additionalFieldsLength+=1;
			line.additionalFieldsId = line.additionalFieldsLength;
			var addValueFname = "addValue_"+line.Inspchar+"_"+line.additionalFieldsLength;
			var addIconFname = "addIcon_"+line.Inspchar+"_"+line.additionalFieldsLength;
			
			line[addValueFname]="";
			line[addIconFname]="";
			
			if(line.CharType==="02"){
				var addKeyFname = "addKey_"+line.Inspchar+"_"+line.additionalFieldsLength;
				line[addKeyFname]="";
			}
			
			this.getView().getModel("InsCharModel").setProperty("/"+addValueFname,"");
			this.getView().getModel("InsCharModel").setProperty("/"+addIconFname,"");
			
			this.addResultInput(line);
		},
		
		addResultInput:function(line){
			var t = this;
			var oAddtElemnt = this.byId("idAdditionalElement");
			oAddtElemnt.setVisible(true);
			
			if(this.addFlag){
				// ilk alan dışındakilere lable eklenir...
				var label = new sap.m.Label({
								vAlign:"Middle"
							});
				label.setText("Münferit değer:");
				label.setDesign("Bold");
				label.setTextAlign("End");
				label.addStyleClass("labelAllign");
				
				var grid = new sap.ui.layout.GridData();
				grid.setSpan("L4 M4 S4");
				grid.setLinebreak(true);
				label.setLayoutData(grid);
				label.addCustomData().id = line.additionalFieldsId;
			
				oAddtElemnt.addField(label);
			}else{
				this.addFlag = true;	
			}
			
			// Input....... ---------------------------------------------------------------------//
			var input = new sap.m.Input({
				value:"{InsCharModel>/addValue_"+line.Inspchar+"_"+line.additionalFieldsId+"}",
				liveChange: function(oEvent){t.onLiveChangeResult2(oEvent,t);},
				valueHelpRequest: function(oEvent){t.valueHelpResult(oEvent,t);},
				type:line.CharType==="01" ? "Number":"Text",
				showValueHelp: t.chrLine.CharType === "01" ? false:true,
				enabled: t.chrLine.Closed === "X" ? false:true
				
			});
			var as = new sap.ui.layout.GridData();
			as.setSpan("L4 M4 S7");
			// input.setModel("{InsCharModel>/addValue"+line.additionalFieldsLength+"}");
			input.setLayoutData(as);
			input.addCustomData().id = line.additionalFieldsId;
			oAddtElemnt.addField(input);
			
			// Icon....... ----------------------------------------------------------------------//
			var icon = new sap.ui.core.Icon({
				width:"50%",
				size:"1.5rem",
				src:"{path: 'InsCharModel>/addIcon_"+line.Inspchar+"_"+line.additionalFieldsId+"', formatter: 'com.nauticana.demo.model.formatter.formatIcon'}",
				color:"{path: 'InsCharModel>/addIcon_"+line.Inspchar+"_"+line.additionalFieldsId+"', formatter: 'com.nauticana.demo.model.formatter.formatIconColor'}"
			});
			var iconSpan = new sap.ui.layout.GridData();
			iconSpan.setSpan("L1 M1 S1");
			icon.setLayoutData(iconSpan);
			icon.addCustomData().id = line.additionalFieldsId;
			oAddtElemnt.addField(icon);
			
		},
		
		onRemove: function(oEvent){
			var oAddtElemnt = this.byId("idAdditionalElement");
			var line = this.QRInsCharTmp[this.selChrItem-1];
			var lineModel = this.getView().getModel("InsCharModel").getData();
			if(line.additionalFieldsLength===0){
				return;
			}
			
			delete lineModel["addValue_"+line.Inspchar+"_"+line.additionalFieldsLength];
			delete lineModel["addIcon_"+line.Inspchar+"_"+line.additionalFieldsLength];
			delete line["addValue_"+line.Inspchar+"_"+line.additionalFieldsLength];
			delete line["addIcon_"+line.Inspchar+"_"+line.additionalFieldsLength];
			var count;
			
			var items = oAddtElemnt.getFields();
			for(var i=0;i<items.length;i++){
				count = parseInt(i);
				if(items[i].id===line.additionalFieldsLength){
					items[i].destroy();
				}
			}
			line.additionalFieldsLength--;
			if(line.additionalFieldsLength===0){
				oAddtElemnt.setVisible(false);
				this.addFlag = false;
			}
		},
		
		setSingleIcon: function(){
			this.chrLine.Icon = "";
			this.getView().getModel("InsCharModel").setProperty("/Icon","");
			for(var i=0; i<this.chrLine.additionalFieldsLength;i++){
				var id = parseInt(i)+1;
				var fName = "addIcon_"+this.chrLine.Inspchar+"_"+id;
				if(this.chrLine[fName]==="R"){
					this.chrLine.Icon = "R";
					this.getView().getModel("InsCharModel").setProperty("/Icon","R");
					return;
				}else if(this.chrLine[fName]==="A"){
					this.chrLine.Icon = "A";
					this.getView().getModel("InsCharModel").setProperty("/Icon","A");
				}
			}
		},

//-----------------------------------------------------------------------------------------------------------------//
//----------------------------------------------  Value Help  -----------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
		onValueHelpResult: function(oEvent){
			var t = this;
			t.valueHelpResult(oEvent,t);
		},
		
		valueHelpResult: function(oEvent,t){
			if (!t._oDialogResult) {
				t._oDialogResult = sap.ui.xmlfragment("com.nauticana.demo.view.CP.fragments.ResultHelp", t);
				t._oDialogResult.setModel(t.getView().getModel("i18n"), "i18n");
				t._oDialogResult.setModel(t.getView().getModel());
				var md = sap.ui.getCore().getModel("CustomInfoModel");
				t._oDialogResult.setModel(md,"CustomInfoModel");
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			// var bMultiSelect = !!oEvent.getSource().data();
			t._oDialogResult.setMultiSelect(false);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			t._oDialogResult.setRememberSelections(bRemember);
			
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", t.getView(), t._oDialogResult);

			// clear the old search filter
			var oFilter1 = new Filter("Werks", sap.ui.model.FilterOperator.EQ, t.chrLine.PselSet1);
			var oFilter2 = new Filter("Katalogart", sap.ui.model.FilterOperator.EQ, t.chrLine.CatType1);
			var oFilter3 = new Filter("Auswahlmge", sap.ui.model.FilterOperator.EQ, t.chrLine.SelSet1);

			t._oDialogResult.getBinding("items").filter([oFilter1,oFilter2,oFilter3]);
			t._oDialogResult.open();
			
			var oSource = oEvent.getSource();
			t.helpSource = oSource;
			
		},
		
		handleConfirmResult: function(oEvent){
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSel = aContexts[0].getModel().getProperty(aContexts[0].getPath());
			// var oResult = this.byId("idResult").setValue(oSel.Kurztext);
			this.helpSource.setValue(oSel.Kurztext);
			
			if(!this.helpSource.id){
				this.QRInsCharTmp[this.selChrItem-1].Result = oSel.Kurztext;
				this.QRInsCharTmp[this.selChrItem-1].Icon = oSel.Bewertung;
				this.QRInsCharTmp[this.selChrItem-1].Key = oSel.Code;
				this.chrLine.Result = oSel.Kurztext;
				this.getView().getModel("InsCharModel").setProperty("/Result",oSel.Kurztext);
				this.getView().getModel("InsCharModel").setProperty("/Icon",oSel.Bewertung);
				this.getView().getModel("InsCharModel").setProperty("/Key",oSel.Code);
				this.changeColorSegmentButton(this.selChrItem-1);
			}else{
				var prop = "addValue_"+this.chrLine.Inspchar+"_"+this.helpSource.id;
				var propIcon = "addIcon_"+this.chrLine.Inspchar+"_"+this.helpSource.id;
				this.QRInsCharTmp[this.selChrItem-1][prop] = oSel.Kurztext;
				this.QRInsCharTmp[this.selChrItem-1][propIcon] = oSel.Bewertung;
				this.getView().getModel("InsCharModel").setProperty("/"+propIcon,oSel.Bewertung);
				
				var addKeyFname = "addKey_"+this.chrLine.Inspchar+"_"+this.helpSource.id;
				this.QRInsCharTmp[this.selChrItem-1][addKeyFname]=oSel.Code;
				this.setSingleIcon();
			}
		},
		
//-----------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------  SAVE  -------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
		onSave: function(){
			var main = {Insplot: this.prueflos};
			var mainItab = [];
			
			for(var i in this.QRInsChar){
				var mainLine = {};
				mainLine.Inspoper = this.QRInsChar[i].Inspoper;
				mainLine.Inspchar = this.QRInsChar[i].Inspchar;
				mainLine.Result   = this.QRInsChar[i].Result;
				mainLine.Key      = "";
				if(this.QRInsChar[i].CharType==="02"){
					mainLine.Key   = this.QRInsChar[i].Key;
				}
				var additionalResults = [];
				for(var j = 0; j<this.QRInsChar[i].additionalFieldsLength ;j++){
					var id = parseInt(j) +1;
					
					var fname = "addValue_"+this.QRInsChar[i].Inspchar+"_"+id;
					
					var value = this.QRInsChar[i][fname];
					var key = "";
					if(this.QRInsChar[i].CharType==="02"){
						var fnameKey = "addKey_"+this.QRInsChar[i].Inspchar+"_"+id;
					        key = this.QRInsChar[i][fnameKey];
					}
					additionalResults.push({Posnr:id.toString(),Result:value,Key:key});
				}
				mainLine.additionalResults = additionalResults;
				mainItab.push(mainLine);
			}
			
			main.mainItab = mainItab;
			
			this.saveResults();
		},
		
		saveResults: function(){
			var t = this;
			var charResults = [];
			var singleResults = [];
			
			for(var i in this.QRInsChar){
				if(this.QRInsChar[i].Closed==="X" || this.QRInsChar[i].opened!=="X"){
					continue;
				}
				var charLine = {};
				charLine.Insplot    = this.prueflos;
				charLine.Inspoper   = this.QRInsChar[i].Inspoper;
				charLine.Inspchar   = this.QRInsChar[i].Inspchar;
				charLine.Closed     = 'X';
				charLine.Evaluated  = 'X';
				charLine.Evaluation = this.QRInsChar[i].Icon;
				charLine.MeanValue  = "";
				charLine.Code1      = "";
				charLine.CodeGrp1   = "";
				
				charLine.Code1      = "";
				if(this.QRInsChar[i].CharType==="01"){
					charLine.MeanValue  = this.QRInsChar[i].Result.toString();
				}else{
					charLine.Code1   = this.QRInsChar[i].Key;
					if(charLine.Code1!==""){
						charLine.CodeGrp1 = this.getCodeGrp(this.QRInsChar[i],charLine.Code1);//SelSet1
					}
				}
				
				// Ek alanlar...
				var additionalResults = [];
				for(var j = 0; j<this.QRInsChar[i].additionalFieldsLength ;j++){
					var id = parseInt(j) +1;
					var fname = "addValue_"+this.QRInsChar[i].Inspchar+"_"+id;
					// var value = this.QRInsChar[i][fname].toString();
					
					var singleLine = {};
					singleLine.Insplot    = this.prueflos;
					singleLine.Inspoper   = this.QRInsChar[i].Inspoper;
					singleLine.Inspchar   = this.QRInsChar[i].Inspchar;
					singleLine.ResNo      = id.toString();
					singleLine.ResValue	  = "";
					singleLine.Code1	  = "";
					singleLine.CodeGrp1	  = "";
					
					if(this.QRInsChar[i].CharType==="01"){
						singleLine.ResValue   = this.QRInsChar[i][fname].toString();
					}else{
						var fnameKey = "addKey_"+this.QRInsChar[i].Inspchar+"_"+id;
						singleLine.Code1	= this.QRInsChar[i][fnameKey];
						if(singleLine.Code1!==""){
							singleLine.CodeGrp1 = this.getCodeGrp(this.QRInsChar[i],singleLine.Code1);//SelSet1
						}
					}
					
					if(singleLine.ResValue===""&&singleLine.Code1===""){
						charLine.Closed = charLine.MeanValue = charLine.Evaluation = charLine.Evaluated = "";
					}
					singleResults.push(singleLine);
				}
				if(this.QRInsChar[i].additionalFieldsLength==0){
					if(charLine.MeanValue===""&&charLine.Code1===""){
						charLine.Closed = charLine.MeanValue = charLine.Evaluation = charLine.Evaluated = "";
					}
				}
				charResults.push(charLine);
			}
			
			if(charResults.length==0){
				sap.m.MessageToast.show("Değişiklik bulunamadı");
				return;
			}
			
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/CPResultSaveSet";
			var oPostData = {
				Insplot:this.QRInsChar[i].Insplot,
				Inspoper:"",//this.QRInsChar[i].Inspoper,
				Return:"",
				Message:"",
				CPResultChar:charResults,
				CPResultSingle:singleResults,
			};
			
			Util.showBusy();
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					Util.hideBusy();
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show("Sonuçlar kaydedildi");	
						t.refreshData();
					}else{
						// sap.m.MessageToast.show(oData.Message, {
						// 	duration: 5000
						// });	
					}
					if(oData.Message!==""){
						var bCompact = !!t.getView().$().closest(".sapUiSizeCompact").length;
						MessageBox.error(
							oData.Message,
							{
								styleClass: bCompact ? "sapUiSizeCompact" : ""
							});
					}
			
				},
				error:function(err) {
					Util.hideBusy();
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
			
		},
		
		refreshData: function(){
			this.getData();
		},
		
		getCodeGrp: function(line,code){
			var md = sap.ui.getCore().getModel("CustomInfoModel");
			var items = md.oData.CustomQualitaCat;
			for(var i in items){
				if( items[i].Werks===line.PselSet1 && 
					items[i].Katalogart===line.CatType1 && 
					items[i].Auswahlmge===line.SelSet1 && 
					items[i].Code===code){
					return items[i].Codegruppe;
				}
			}
		}
		
//*--------------------------------------------------------------------------------------------------//
	});

});