/*global location */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(Util,BaseController, JSONModel, Formatter, History, Filter, MessageBox, Button, Dialog, List, StandardListItem, MessageToast,  
	FilterOperator,Device) {
	"use strict";

		return BaseController.extend("com.nauticana.demo.controller.CP.CPErrorDisplay", {

		 

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				var oRouter = this.getRouter();
				oRouter.getRoute("cperrordisplay").attachMatched(this._onRouteMatched, this);
				this._oView = this.getView();
			},
			
			_onRouteMatched : function(oEvent) {
				var oArgs, oView;
				oArgs = oEvent.getParameter("arguments");
				oView = this.getView();
				this.prueflos = oArgs.objectId;
				this.matnr    = oArgs.matnr;
				
				var oModel = new sap.ui.model.json.JSONModel({
					Prueflos:this.prueflos,
					Matnr: this.matnr,
					isPhone: Device.system.phone
				});
				this.getView().setModel(oModel);
				
				this.getData();
			},
			
			getData: function(){
				var t = this;
				
				var oModel = new sap.ui.model.json.JSONModel();
				t.getView().setModel(oModel,"ErrorModel");
				
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
				var sPath = "/ErrorDisplaySet";
				
				var oFilter = [];
		        oFilter.push(new sap.ui.model.Filter("Prueflos", sap.ui.model.FilterOperator.EQ, this.prueflos));
				
				var oDataFilter = new sap.ui.model.Filter({ 
										filters: oFilter,
										and: true 
									});
				Util.showBusy();
				oModel.read(sPath, {
					filters: [oDataFilter],
					success: function(oData, response) {
						Util.hideBusy();
						
						var oModel = new sap.ui.model.json.JSONModel();
						var items = oData.results;
						if(items.length>0){
							var items = oData.results;
							var ErrorCollection = [];
							for(var i in items){
								ErrorCollection.push({
									Matnr: t.Matnr,
									Prueflos: items[i].Prueflos,
									Posnr: items[i].Posnr,
									ErrorCode:items[i].Fecod+" "+items[i].Fegrp,
									ErrorCodeText:items[i].ECKurztext,
									ErrorLocation:items[i].Oteil+" "+items[i].Otgrp,
									ErrorLocationText:items[i].ELKurztext,
									ErrorNumber:items[i].Anzfehler,
									Explanation:items[i].Fetxt,
								});
							}
							oModel.setProperty("/ErrorCollection",ErrorCollection);
							t.getView().setModel(oModel,"ErrorModel");
						}else{
							MessageToast.show("Kayıt Bulunamadı");
						}
					},
					error: function(oError) {
						Util.hideBusy();
						// console.log("Error" + oError.responseText)
					}
				});
			},
			
			onNavBack : function() {
				history.go(-1);
			}

		});

	}
);