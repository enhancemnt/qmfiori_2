/*global location */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(Util,BaseController, JSONModel, Formatter, History, Filter, MessageBox, Button, Dialog, List, StandardListItem, MessageToast,  
	FilterOperator,Device) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.CP.CPErrorCreate", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {

			var oRouter = this.getRouter();

			oRouter.getRoute("cperrorcreate").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			Util.readCustomising();
			var oJSONModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oJSONModel,"SelectHataModel");
			
			var oJSONModel2 = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oJSONModel2,"SelectHataModel2");
			
		},

		_onRouteMatched: function(oEvent) {
			var oArgs, oView, oSelectedKey, oSelectedInspoper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			this.prueflos = oArgs.objectId;
			this.matnr = oArgs.matnr;
			this.art = oArgs.art;
			var oJSONModel = new sap.ui.model.json.JSONModel({
						Prueflos : this.prueflos,
						Matnr:this.matnr
					});
			this.getView().setModel(oJSONModel,"ErrorModel");
			
			var oJSONModel = new sap.ui.model.json.JSONModel({
				isPhone: Device.system.phone
			});
			this.getView().setModel(oJSONModel);
		},
		
//-------------------------------------------------------------------------------------------------------------------------------//
//-----------------------------------------------  Save Error  ------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//	
		onSaveHata: function(oEvent) {
			var t = this;
			var ISubsys = "000001";
			var Insplot = this.prueflos;
			var Satzart = "Q90";
			var Posnr = "0001";
			var Fekat = this.getModel("SelectHataModel").getData().Fekat;
			var Fecod = this.getModel("SelectHataModel").getData().Fecod;
			var Fegrp = this.getModel("SelectHataModel").getData().Fegrp;
			var Anzfehler = this.getView().byId("__hatasayisiID").getValue();
			var Otkat = this.getModel("SelectHataModel2").getData().Otkat;
			var Otgrp = this.getModel("SelectHataModel2").getData().Otgrp;
			var Oteil = this.getModel("SelectHataModel2").getData().Oteil;
			var Fetxt = this.getView().byId("__aciklama").getValue();

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");

			var sPath = "/ErrorCreateSet";
			var oPostData = {
				// ISubsys: ISubsys,
				Satzart: Satzart,
				Prueflos: Insplot,
				Posnr: Posnr,
				Fekat: Fekat,
				Fegrp: Fegrp,
				Fecod: Fecod,
				Anzfehler: Anzfehler,
				Otkat: Otkat,
				Otgrp: Otgrp,
				Oteil: Oteil,
				Fetxt: Fetxt
			};

			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Prueflos===t.prueflos){
						MessageToast.show("Başarılı bir şekilde Kaydedildi");
						history.go(-1);
					}else{
						sap.m.MessageToast.show("Kaydetme sırasında hata oluştu");
					}
				},

				error:function(err) {
					sap.m.MessageToast.show("Kaydetme sırasında hata oluştu");
				}
			});
			
		},

//-------------------------------------------------------------------------------------------------------------------------------//
//-----------------------------------------------  Error Code  ------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//	
		onValueHelpHatakod: function(oEvent) {
			//debugger;
			if (!this._oDialogHataKod) {
				this._oDialogHataKod = sap.ui.xmlfragment("com.nauticana.demo.view.CP.fragments.CPDialogHataKod", this, true);
				this._oDialogHataKod.setModel(this.getView().getModel("i18n"), "i18n");
			}
			
			//---------------------------------------------------------------//
			var oJsonModel = sap.ui.getCore().getModel("CustomInfoModel");
			var notHead = oJsonModel.getProperty("/CustomNotifICatHead");
			var notArt = oJsonModel.getProperty("/CustomInspeType");
			
			var notDetail = oJsonModel.getProperty("/CustomNotifICatDetail");
			
			var items = [];
			var lineHead = {};
			for(var i in notArt){
				if(notArt[i].Art===this.art){
					for(var j in notHead){
						if(notHead[j].Qmart===notArt[i].Qmart){
							lineHead = notHead[j];
							break;
						}
					}
					for(var j in notDetail){
						if(notDetail[j].Qmart===notArt[i].Qmart){
							if(notDetail[j].Rbnr===lineHead.Rbnr && notDetail[j].Qkatart===lineHead.Fekat){
								items.push(notDetail[j]);
							}
						}
					}
				}
			}
			var oModel = new sap.ui.model.json.JSONModel({
					ErrorCodeCollection:items
			});
			this._oDialogHataKod.setModel(oModel, "ErrorCodeModel");
			//---------------------------------------------------------------//
			
			this.getView().addDependent(this._oDialogHataKod);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataKod);

			this._oDialogHataKod.open();

		},
		
		handleCloseHkod: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInputHataKod = aContexts[0].getModel().getProperty(oSelPath).Code;
			var oInputHataKodGrp = aContexts[0].getModel().getProperty(oSelPath).Qcodegrp;
			var oInputHataKod2 = aContexts[0].getModel().getProperty(oSelPath).Kurztext;
			var Fekat = aContexts[0].getModel().getProperty(oSelPath).Qkatart;
			
			this.getView().byId("__hataKod").setValue(oInputHataKod + " " + oInputHataKodGrp);
			this.getView().byId("__hataKod2").setValue(oInputHataKod2);

			this.getView().getModel("SelectHataModel").setData({
				Fekat: Fekat,
				Fegrp: oInputHataKodGrp,
				Fecod: oInputHataKod
			});

		},
		

//-------------------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------  Error Location  ----------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//		
		onValueHelpHataYeri: function() {
			if (!this._oDialogHataYeri) {
				this._oDialogHataYeri = sap.ui.xmlfragment("com.nauticana.demo.view.CP.fragments.CPDialogHataYeri", this, true);
				this._oDialogHataYeri.setModel(this.getView().getModel("i18n"), "i18n");

			}
			
			//---------------------------------------------------------------//
			var oJsonModel = sap.ui.getCore().getModel("CustomInfoModel");
			var notArt = oJsonModel.getProperty("/CustomInspeType");
			var notHead = oJsonModel.getProperty("/CustomNotifICatHead");
			var notDetail = oJsonModel.getProperty("/CustomNotifICatDetail");
			
			var items = [];
			var lineHead = {};
			for(var i in notArt){
				if(notArt[i].Art===this.art){
					for(var j in notHead){
						if(notHead[j].Qmart===notArt[i].Qmart){
							lineHead = notHead[j];
							break;
						}
					}
					for(var j in notDetail){
						if(notDetail[j].Qmart===notArt[i].Qmart){
							if(notDetail[j].Rbnr===lineHead.Rbnr && notDetail[j].Qkatart===lineHead.Otkat){
								items.push(notDetail[j]);
							}
						}
					}
				}
			}
			
			// for(var i in notHead){
			// 	for(var j in notDetail){
			// 		if(notDetail[j].Art===this.art){
			// 			if(notDetail[j].Rbnr===notHead[i].Rbnr && notDetail[j].Qkatart===notHead[i].Otkat){
			// 				items.push(notDetail[j]);
			// 			}
			// 		}
			// 	}
			// }
			var oModel = new sap.ui.model.json.JSONModel({
					ErrorLocationCollection:items
			});
			this._oDialogHataYeri.setModel(oModel, "ErrorLocationModel");
			//---------------------------------------------------------------//
			

			this.getView().addDependent(this._oDialogHataYeri);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataYeri);

			this._oDialogHataYeri.open();
		},
		
		handleCloseHyeri: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInputHataYeri = aContexts[0].getModel().getProperty(oSelPath).Code;
			var oInputHataYeriGrp = aContexts[0].getModel().getProperty(oSelPath).Qcodegrp;
			var oInputHataYeri2 = aContexts[0].getModel().getProperty(oSelPath).Kurztext;
			var Otkat = aContexts[0].getModel().getProperty(oSelPath).Qkatart;

			this.getView().byId("_hataYeri").setValue(oInputHataYeri + " " + oInputHataYeriGrp);
			this.getView().byId("_hataYeri2").setValue(oInputHataYeri2);

			this.getView().getModel("SelectHataModel2").setData({
				Otkat: Otkat,
				Otgrp: oInputHataYeriGrp,
				Oteil: oInputHataYeri
			});

		},

//-------------------------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------  Others  --------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------//	
		onNavBack: function() {
			history.go(-1);
		},
		
		onExit: function() {
			if (this._oDialogHataYeri) {
				this._oDialogHataYeri.destroy();
			}
			if (this._oDialogHataKod) {
				this._oDialogHataKod.destroy();
			}
		}

	});

});