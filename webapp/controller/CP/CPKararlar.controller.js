/*global location */
sap.ui.define([
	"com/nauticana/demo/util/Util",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator"
], function(Util,BaseController, JSONModel, Dialog, List, StandardListItem, Button, formatter, MessageBox, MessageToast, History, Filter,
	FilterOperator) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.CP.CPKararlar", {
		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function(oEvent) {

			var oRouter = this.getRouter();

			oRouter.getRoute("cpkararlar").attachMatched(this._onRouteMatched, this);
			
			Util.readCustomising();

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			var KPSSonuc = new JSONModel({
				enabled: true
			});

			this.setModel(KPSSonuc, "oSonuc");

			this.setModel(oViewModel, "detailView");

			// this.setModel(operationModel, "operationM");
		},
		_onRouteMatched: function(oEvent) {
			var oArgs, oView, oSelectedKey, oSelectedInspoper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			this.prueflos = oArgs.objectId;
			this.mode	  = oArgs.mode;
			this.art	  = oArgs.art;
			this.werks	  = oArgs.werks;
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData({"Mode":this.mode});
			this.getView().setModel(oModel);
			
			var oModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oModel,"InsCharModel");
			
			
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/CPDecisionSet(Prueflos='"+this.prueflos+"')";
		
			oModel.read(sPath, {
				urlParameters: {
					"$expand": "QRInsChar"
				},
				success: function(oData, response) {
					oData.QRInsChar = oData.QRInsChar.results;
					var oModel = new sap.ui.model.json.JSONModel();
					oModel.setData(oData);
					t.getView().setModel(oModel,"InsCharModel");
				},
				error: function(oError) {
				}
			});

		},

		onNavBack: function() {
			history.go(-1);
		},
		
		onValueHelpDes: function(oEvent){
			if (!this._oDialogDecision) {
				this._oDialogDecision = sap.ui.xmlfragment("com.nauticana.demo.view.CP.fragments.CPDialogDecision", this);
				this._oDialogDecision.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogDecision.setModel(this.getView().getModel());
				var oJsonModel = sap.ui.getCore().getModel("CustomInfoModel");
				this._oDialogDecision.setModel(oJsonModel,"CustomInfoModel");
			}
			// this.getView().addDependent(this._oDialogDecision);
			this._oDialogDecision.setMultiSelect(false);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogDecision);
			
			var ofilter =  [new sap.ui.model.Filter("Art", FilterOperator.EQ, this.art),new sap.ui.model.Filter("Werks", FilterOperator.EQ, this.werks)];	
			this._oDialogDecision.getBinding("items").filter(ofilter);
			var items = this._oDialogDecision.getItems();
			for(var i in items){
				if(i%2==0){
					items[i].addStyleClass("green");
				}else{
					items[i].addStyleClass("red");
				}
			}
			this._oDialogDecision.open();
		},
		
		handleConfirmDecision: function(oEvent){
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSel = aContexts[0].getModel().getProperty(aContexts[0].getPath());
			this.byId("__kararkoduID").setValue(oSel.Code);
			this.byId("__koddegerlemesiID").setValue(oSel.Bewertung);
			this.byId("__kalitepuanID").setValue(oSel.Qkennzahl);
			this.getModel("InsCharModel").setProperty("/Codegruppe",oSel.Codegruppe);
			this.getModel("InsCharModel").setProperty("/Auswahlmge",oSel.Auswahlmge);
		},
		
		
		handleCloseDecision: function(oEvent){
		},
		
		onSave: function(){
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/UsageDecisionSet";
			var Code	   = this.getModel("InsCharModel").getProperty("/Vcode");
			var Auswahlmge = this.getModel("InsCharModel").getProperty("/Auswahlmge");
			var Codegruppe = this.getModel("InsCharModel").getProperty("/Codegruppe");
			var aciklama = this.getView().byId("__aciklamaID").getValue();
			
			var oPostData = {
				Insplot: this.prueflos,
				UdSelectedSet: Auswahlmge,
				UdPlant: this.werks,
				UdCodeGroup: Codegruppe,
				UdCode: Code,
				UdTextLine: aciklama,
				Return:"",
				Message:""
			};
			
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show("Başarılı bir şekilde Kaydedildi");	
						history.go(-1);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},

	});

});