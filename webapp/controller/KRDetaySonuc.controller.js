sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageBox",
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/List',
		'sap/m/StandardListItem',
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
],function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";


	return BaseController.extend("com.nauticana.demo.controller.KRDetaySonuc", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
			onInit: function() {
				
				//debugger;
	     	this.getRouter().getRoute("krdetaysonuc").attachMatched(this._onRouteMatched, this);
			this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});

				 var KPSSonuc = new JSONModel({
						enabled : true
				});
				
				this.setModel(KPSSonuc, "oSonuc");
			
			
				this.setModel(oViewModel, "detailView");
		    
		    var SelSonuc = new JSONModel({
						oSelectedKey : null,
						oSelectedInspoper : null,
						oSelectedInspchar : null,
						oSelEvaluation : null
				});
			     this.setModel(SelSonuc, "mSelSonuc");
		    
		     
		    	
			},
				onNavBack : function() {

				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					this.getRouter().navTo("krdetaygoruntule", {}, true);
				//	history.go(-1);
				} else {
					this.getRouter().navTo("krdetaygoruntule", {}, true);
				}
				
			},
			openValues:  function (oEvent) {
				//debugger;
				
				var EtInspoperPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.InspoperModel.sPath;
				var EtZinsPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.sPath;
				
				var InspoperIndex = parseInt(EtInspoperPath.substr(20),10);
				var EtZinsIndex = parseInt(EtZinsPath.substr(22),10);
				
				//debugger;

				var Prueflos  = oEvent.oSource.oPropagatedProperties.oModels.InspoperModel.getData().InspoperCollection[InspoperIndex].Insplot;
			    var Inspoper  = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspoper;
		     	var Inspchar  = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspchar;

			this.getRouter().navTo("krdetaysonucset", {
					objectId : Prueflos,
				    Inspoper : Inspoper,
				    Inspchar: Inspchar
				    
			}, false); 
			},
			
				nextInspoper:  function (oEvent) {
			//debugger;
			var InspoperPath =	oEvent.getSource().getBindingContext("InspoperModel").sPath;
	 		var len2 = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection.length;
			var InspoperIndex = parseInt(InspoperPath.substr(20),10);
			
			if(0 <= InspoperIndex < len2){
				InspoperIndex = InspoperIndex + 1;
						    
			  		if(InspoperIndex < len2){
					this.getView().bindElement({
									path: "/InspoperCollection/" + InspoperIndex ,
						     		model: "InspoperModel"
						    	});	
					var Inspoper = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection[InspoperIndex].Inspoper;
		  	 
		  			var aFilter = [];
				
					aFilter.push(new sap.ui.model.Filter("Inspoper", sap.ui.model.FilterOperator.EQ, Inspoper)); 
			       var list = this.getView().byId("list");
			  	   var binding = list.getBinding("items");
			 	   binding.filter(aFilter); 
			}
		}
	 	 
		},
			_onRouteMatched: function(oEvent) {
				
				//debugger;
			var oArgs, oView, oSelectedKey, oSelectedInspoper, oSelectedTxtOper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			oSelectedKey  = oArgs.objectId;
			oSelectedInspoper = oArgs.Inspoper;

			
            var InspoperM = sap.ui.getCore().getModel("EtInspoperModel");
			var InsM = {}; 
			InsM.InspoperCollection = InspoperM.getData(); 
			var InspM = new JSONModel(InsM); 
			this.getView().setModel(InspM,"InspoperModel");
 
			var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
			var EtZinsM = {}; 
			EtZinsM.EtZinsCharCollection = EtZinsCharM.getData(); 
			var EtZM = new JSONModel(EtZinsM); 
			this.getView().setModel(EtZM,"EtZinsCharModel"); 


		   	var collection = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection;
		   	var oSelectedInspchar=null;
		   	var oSelEvaluation = null;
		    
			for (var key in collection) {
	    			if (key === 'length' || !collection.hasOwnProperty(key)) continue;
	    			if ( oSelectedInspoper === collection[key].Inspoper){
	    				var value2 = collection[key] ;
	    				oSelectedInspchar = value2.Inspchar;
	    				oSelEvaluation = value2.Evaluation;
	    				break;
	    			}
	    	}
	    	
			
		     this.getView().getModel("mSelSonuc").setData({
        		oSelectedKey: oSelectedKey,
        		oSelectedInspoper: oSelectedInspoper,
        		oSelectedInspchar :  oSelectedInspchar,
				oSelEvaluation : oSelEvaluation
            });
            
             var collection1 = this.getView().getModel("InspoperModel").getData().InspoperCollection;
			var index1 = $.inArray(oSelectedInspoper, $.map(collection1, function(n){
			    return n.Inspoper;
			}));
			
			
            this.getView().bindElement({
					path: "/InspoperCollection/" + index1,
		     		model: "InspoperModel"
		    	});	
		    	
		   	var aFilter = [];
			aFilter.push(new sap.ui.model.Filter("Inspoper", sap.ui.model.FilterOperator.EQ, oSelectedInspoper)); 
			var list = this.getView().byId("list");
		  	var binding = list.getBinding("items");
		 	binding.filter(aFilter); 
			},
				
	 
		
			IsBeetween : function(a, b, inclusive) {
		 
				 var min = Math.min(a, b),
    				 max = Math.max(a, b);
					return inclusive ? inclusive >= min && inclusive <= max : inclusive > min && inclusive < max;
			},	
		getIconFlag : function (status) {
			
			//debugger;
		var oSelectedKey = this.getView().getModel("mSelSonuc").getData().oSelectedKey ;
		var oSelectedInspoper = this.getView().getModel("mSelSonuc").getData().oSelectedInspoper ;
 		var oSelectedInspchar = this.getView().getModel("mSelSonuc").getData().oSelectedInspchar ; 
		var oSelEvaluation = this.getView().getModel("mSelSonuc").getData().oSelEvaluation ; 
			
			var icon6 = "sap-icon://accept";
	    	var icon7 = "sap-icon://decline";
	 
	    //	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
	     //   var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
             if(oSelEvaluation === 'A'){
             	return  icon6 ;
             }else if(oSelEvaluation === 'R'){
             	return icon7;
             }else{
             	return null;
             }
    	 
		},
			formatIconColor : function (status) {
			  //debugger;
			  var oSelEvaluation = this.getView().getModel("mSelSonuc").getData().oSelEvaluation ; 
			  
			  if(oSelEvaluation === "A"){
             	return  "green";
             }else if(oSelEvaluation === "R"){
             	return "red";
             }else{
             	return null;
             }
     
			},	

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onExit: function() {
		//
		//	}

	});

});