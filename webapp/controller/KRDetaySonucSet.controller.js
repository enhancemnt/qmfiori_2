sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageBox",
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/List',
		'sap/m/StandardListItem',
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";

	return BaseController.extend("com.nauticana.demo.controller.KRDetaySonucSet", {


			onInit : function (oEvent) {
				//debugger;
			
		
				this.getRouter().getRoute("krdetaysonucset").attachMatched(this._onRouteMatched, this);
			
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});

				 var KPSSonuc = new JSONModel({
						enabled : true
				});
				
				this.setModel(KPSSonuc, "oSonuc");
			
			
				this.setModel(oViewModel, "detailView");
		    
		        var SelSonuc = new JSONModel({
						oSingleKey : null,
						oSingleInspoper : null,
						oSingleInspchar : null,
						oSelResValueAttr : null
				});
			     this.setModel(SelSonuc, "mSingleSonuc");
		    	
		    	
			    // this.setModel(operationModel, "operationM");
			},
 
	_onRouteMatched : function (oEvent) {
				//debugger;
			var oArgs,  oSelectedKey, oSelectedInspoper,oSelectedInspchar;
			oArgs = oEvent.getParameter("arguments");
			oSelectedKey  = oArgs.objectId;
			oSelectedInspoper = oArgs.Inspoper;
			oSelectedInspchar = oArgs.Inspchar;
           
 
			var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
			var EtZinsM = {}; 
			EtZinsM.EtZinsCharCollection = EtZinsCharM.getData(); 
			var EtZM = new JSONModel(EtZinsM); 
			this.getView().setModel(EtZM,"EtZinsCharModel"); 
			
	        var EtSingleM = sap.ui.getCore().getModel("EtSingleResultsModel");
			var EtSingM = {}; 
			EtSingM.EtSingleResultsCollection = EtSingleM.getData(); 
			var EtSM = new JSONModel(EtSingM); 
			this.getView().setModel(EtSM,"EtSingleResultsModel");

	    	var collection2 = this.getView().getModel("EtSingleResultsModel").getData().EtSingleResultsCollection;
		   	
			var index2 = $.inArray(oSelectedInspoper, $.map(collection2, function(n){
			    return n.Inspoper;
			}));
			
			this.getView().bindElement({
					path: "/EtSingleResultsCollection/" + index2,
		     		model: "EtSingleResultsModel"
		    	});	

		   var aFilter = [];
	  	      
	 	   aFilter.push(new sap.ui.model.Filter("Inspoper", "EQ", oSelectedInspoper)); 
		   aFilter.push(new sap.ui.model.Filter("Inspchar","EQ", oSelectedInspchar));
		  	
		   var list = this.getView().byId("list");
		   var binding = list.getBinding("items");
		   binding.filter(aFilter); 

		},
		
		getIconFlag : function (status) {
			//debugger;
			
			var icon6 = "sap-icon://accept";
	    	var icon7 = "sap-icon://decline";
	 
	    //	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
	     //   var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
             if(status === 'A'){
             	return  icon6 ;
             }else if(status === 'R'){
             	return icon7;
             }else{
             	return null;
             }
		},
		formatIconColor : function (status) {
			  //debugger;
			  
			 if(status === "A"){
             	return  "green";
             }else if(status === "R"){
             	return "red";
             }else{
             	return null;
             }
			},
		
		onNavBack : function() {

		    	var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();		
 
				if (sPreviousHash !== undefined) {
			 
					history.go(-1);
				} else {
					this.getRouter().navTo("krdetaygoruntule", {}, true);
				}
				
			}
			
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.KRDetaySonucSet
		 */
		//	onInit: function() {
		//
		//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.KRDetaySonucSet
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.KRDetaySonucSet
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.KRDetaySonucSet
		 */
		//	onExit: function() {
		//
		//	}

	});

});