sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(BaseController, JSONModel, formatter, History, Filter, MessageBox, Button, Dialog, List, StandardListItem, MessageToast, 
	FilterOperator, Device) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.QR.QualityDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		onInit: function() {

			//debugger;
			this.getRouter().getRoute("qrdetail").attachMatched(this._onRouteMatched, this);
			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			var KPSSonuc = new JSONModel({
				enabled: true
			});

			this.setModel(KPSSonuc, "oSonuc");

			this.setModel(oViewModel, "detailView");

			var SelSonuc = new JSONModel({
				oSelectedKey: null,
				oSelectedInspoper: null,
				oSelectedInspchar: null,
				oSelEvaluation: null
			});
			this.setModel(SelSonuc, "mSelSonuc");
			
			//Device...
		    var device = Device.system.phone;
		    
		    var mDevice = new JSONModel({
				isPhone: device,
				isNotPhone: !device
			});
		    this.getView().setModel(mDevice,"Device");

		},
		onNavBack: function() {
			history.go(-1);
		},
		
		openValues: function(oEvent) {
			//debugger;
			var path1 = oEvent.oSource.oPropagatedProperties.oBindingContexts.InsCharModel.sPath;
			var index1 = parseInt(path1.substr(11), 10);
			var line = oEvent.oSource.oPropagatedProperties.oModels.InsCharModel.getData().QRInsChar[index1];
			
			var Prueflos = line.Insplot;
			var Inspoper = line.Inspoper;
			var Inspchar = line.Inspchar;
			
			// var EtInspoperPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.InspoperModel.sPath;
			// var EtZinsPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.sPath;

			// var InspoperIndex = parseInt(EtInspoperPath.substr(20), 10);
			// var EtZinsIndex = parseInt(EtZinsPath.substr(22), 10);

			// var Prueflos = oEvent.oSource.oPropagatedProperties.oModels.InspoperModel.getData().InspoperCollection[InspoperIndex].Insplot;
			// var Inspoper = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspoper;
			// var Inspchar = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspchar;
			this.novFlag = true;
			this.getRouter().navTo("qresults", {
				objectId: Prueflos,
				Inspoper: Inspoper,
				Inspchar: Inspchar

			}, false);
		},

		nextInspoper: function(oEvent) {
			//debugger;
			var InspoperPath = oEvent.getSource().getBindingContext("InspoperModel").sPath;
			var len2 = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection.length;
			var InspoperIndex = parseInt(InspoperPath.substr(20), 10);

			if (0 <= InspoperIndex < len2) {
				InspoperIndex = InspoperIndex + 1;

				if (InspoperIndex < len2) {
					this.getView().bindElement({
						path: "/InspoperCollection/" + InspoperIndex,
						model: "InspoperModel"
					});
					var Inspoper = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection[InspoperIndex].Inspoper;

					var aFilter = [];

					aFilter.push(new sap.ui.model.Filter("Inspoper", sap.ui.model.FilterOperator.EQ, Inspoper));
					var list = this.getView().byId("list");
					var binding = list.getBinding("items");
					binding.filter(aFilter);
				}
			}

		},
		_onRouteMatched: function(oEvent) {
			
			var oArgs = oEvent.getParameter("arguments");
			this.objectId = oArgs.objectId;
			
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/QualityReportListSet(Prueflos='"+this.objectId+"')";
			
			if(!this.novFlag){
				oModel.read(sPath, {
					urlParameters: {
				                "$expand": "QRItem,QRInsChar,QRSingleResults"
				            },
					success: function(oData, response) {
						var jsonArray = [];
						t.QRItems   = oData.QRItem.results;
						t.QRInsChar = oData.QRInsChar.results;
						t.QRSingleResults = oData.QRSingleResults.results;
						var oJSONModel = new sap.ui.model.json.JSONModel({
							QRItems : t.QRItems
						});
						t.getView().setModel(oJSONModel);
						
						
						
						var oJSONModel = new sap.ui.model.json.JSONModel();
						t.getView().setModel(oJSONModel,"InsCharModel");
						if(t.QRItems.length>0){
							t.getView().getModel().setProperty("/QRSelectedItem",t.QRItems[0].Inspoper);
							t.selectedItem(t.QRItems[0].Inspoper);
						}
						
						var oJSONModel = new sap.ui.model.json.JSONModel({
							EtZinsCharCollection : t.QRInsChar
						});
						sap.ui.getCore().setModel(oJSONModel,"EtZinsCharModel");
						
						var oJSONModel = new sap.ui.model.json.JSONModel({
							QualityResultsCollection : t.QRSingleResults
						});
						sap.ui.getCore().setModel(oJSONModel,"QualityResultsModel");
						
					},
					error: function(oError) {
						// console.log("Error" + oError.responseText)
					}
				});
			}else{
				this.novFlag=false;
			}
		},
		
		onItemSelectionChange: function(oEvent){
			 var oSource = oEvent.getSource();
			 this.selectedItem(oSource.getSelectedKey());
		},
		
		selectedItem: function(Inspoper){
			var items = [];
			for(var i in this.QRInsChar){
				if(this.QRInsChar[i].Inspoper === Inspoper){
					items.push(this.QRInsChar[i]);	
				}
			}
			// var oJSONModel = new sap.ui.model.json.JSONModel({
			// 			QRInsChar: items
			// 		});
			this.getView().getModel("InsCharModel").setProperty("/QRInsChar",items);		
		},

		IsBeetween: function(a, b, inclusive) {

			var min = Math.min(a, b),
				max = Math.max(a, b);
			return inclusive ? inclusive >= min && inclusive <= max : inclusive > min && inclusive < max;
		},
		getIconFlag: function(status) {

			//debugger;
			var oSelectedKey = this.getView().getModel("mSelSonuc").getData().oSelectedKey;
			var oSelectedInspoper = this.getView().getModel("mSelSonuc").getData().oSelectedInspoper;
			var oSelectedInspchar = this.getView().getModel("mSelSonuc").getData().oSelectedInspchar;
			var oSelEvaluation = this.getView().getModel("mSelSonuc").getData().oSelEvaluation;

			var icon6 = "sap-icon://accept";
			var icon7 = "sap-icon://decline";

			//	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
			//   var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
			if (oSelEvaluation === 'A') {
				return icon6;
			} else if (oSelEvaluation === 'R') {
				return icon7;
			} else {
				return null;
			}

		},
		formatIconColor: function(status) {
			//debugger;
			var oSelEvaluation = this.getView().getModel("mSelSonuc").getData().oSelEvaluation;

			if (oSelEvaluation === "A") {
				return "green";
			} else if (oSelEvaluation === "R") {
				return "red";
			} else {
				return null;
			}

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.KRDetaySonuc
		 */
		//	onExit: function() {
		//
		//	}

	});

});