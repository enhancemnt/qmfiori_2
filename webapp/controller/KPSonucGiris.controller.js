/*global location */
sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"com/nauticana/demo/model/formatter",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/m/MessageBox",
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/List',
		'sap/m/StandardListItem',
		"sap/m/MessageToast",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, Dialog,List,StandardListItem,Button,formatter,MessageBox,MessageToast,History,Filter,FilterOperator) {
		"use strict";

		return BaseController.extend("com.nauticana.demo.controller.KPSonucGiris", {

			formatter: formatter,
   
			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				
				var oRouter = this.getRouter();

				oRouter.getRoute("kpsonucgiris").attachMatched(this._onRouteMatched, this);
			
			 
				//debugger;
				this._oView = this.getView();
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var oViewModel = new JSONModel({
					busy : false,
					delay : 0
				});

				 var KPSSonuc = new JSONModel({
						enabled : true
				});
				
				var KPSParameter = new JSONModel({
						oSelectedKey : null,
						oSelectedInspoper : null
				});
				
				this.setModel(KPSParameter, "KPSParameters");
					
				this.setModel(KPSSonuc, "oSonuc");
			
			
				this.setModel(oViewModel, "detailView");
		    
			  
				},

			IsBeetween : function(a, b, inclusive) {
		 
		 	 a  = parseInt(a);
		 	 b  = parseInt(b);
		 	inclusive  = parseInt(inclusive);
		 	 
				 var min = Math.min(a, b),
    				 max = Math.max(a, b);
					return inclusive ? inclusive >= min && inclusive <= max : inclusive > min && inclusive < max;
			},
			
		   formatIconColor : function (MeanValue,Code1,LwTolLmt,UpTolLmt,TargetVal) {
	 //debugger;
				
				var newValue;
				if( MeanValue === "" || MeanValue === null){
					newValue = Code1;
					
				}else  {
					newValue = MeanValue;
				}
				
				 var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
			 
				 if(  newValue  === "" || newValue === null){
					     
					     return "";	 
				 }
				 if(((newValue === TargetVal) || val === true )){
				 	
				 	 return "green";
				 	
				 }else{
			     		 return "red";  
			     }
	 
			},
		formatIconColorRes : function (ResValue,Code1,LwTolLmt,UpTolLmt,TargetVal) {
		 //debugger;
				
				//newValue = sap.ui.getCore().byId("__sonucID").getValue();
				var newValue;
				if( ResValue === "" || ResValue === null){
					newValue = Code1;
					
				}else  {
					newValue = ResValue;
				}
				//	 newValue = this.getView().byId("__sonucID").getValue();
				 var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
				  
				  if( newValue === "" || newValue === null){
					     
					     return "";	 
				 }
				 if( ((newValue === TargetVal) || val === true )  ){
				 	
				 	 return "green";
				 	
				 }else{
			     		 return "red";  
			     }
	 
			},
			getIconFlag : function (MeanValue,Code1,LwTolLmt,UpTolLmt,TargetVal) {
 //debugger;
			var icon6 = "sap-icon://accept";
	    	var icon7 = "sap-icon://decline";
 
				var newValue;
				if(MeanValue){
					newValue = MeanValue;
					
				}else  {
					newValue = Code1;
				}
				
				 var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
			 
				if(newValue === "" || newValue === null){
					     
					    return   null;	 
				 }
				 if( ((newValue === TargetVal) || val === true )  ){
				 	
				 	 return icon6;
				 	
				 }else{
			     		 return   icon7 ;
			     }
			     
    			 
			},
			getIconFlagRes : function (ResValue,Code1,LwTolLmt,UpTolLmt,TargetVal) {
 //debugger;
			var icon6 = "sap-icon://accept";
	    	var icon7 = "sap-icon://decline";
				 var newValue;
				if(ResValue){
					newValue = ResValue;
					
				}else  {
					newValue = Code1;
				}
				 //newValue = this.getView().byId("__sonucID").getValue();
				 var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
			 
				 if(newValue === "" || newValue === null ){
					     
					    return   null;	 
				 }
				 if( ((newValue === TargetVal) || val === true )  ){
				 	
				 	 return icon6;
				 	
				 }else{
			     		 return   icon7 ;
			     }
			     
    			 
			},
			
			InspoperCatalog:  function (otherSet) {
		/*		 
				var space = " ";
				 var oModel = this._oDialogInspoper.getModel();
            	 var addressObj = oModel.getObject("/" + otherSet[0]);//use absolute binding
                return(addressObj.Inspoper + space + addressObj.TxtOper + space + addressObj.Workcenter);*/
            },
            createContent : function(oController) {		
	
		/*	  if(!(oSelectedKey=="*"))
			
			        var oFilters = [ new sap.ui.model.Filter("CompanyId","EQ", oSelectedKey) ];
			
			          sap.ui.getCore().byId("Countries").bindItems("/Employees",sap.ui.getCore().byId("first_template"), null, oFilters);
			*/
			},
			onValueHelpMunferit : function (oEvent) {
	  //debugger;
				if (!this._oDialogMunferit) {
					this._oDialogMunferit = sap.ui.xmlfragment("com.nauticana.demo.view.DialogMunferit", this);
					this._oDialogMunferit.setModel(this.getView().getModel("i18n"), "i18n");
					this._oDialogMunferit.setModel(this.getView().getModel());
				
						var	IM = this.getModel("EtZqualitaCatModel");
					        this._oDialogMunferit.setModel(IM,"EtZqualitaCatModel");
				}
				var bRemember = !!oEvent.getSource().data("remember");
				this._oDialogMunferit.setRememberSelections(bRemember);
	
			
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogMunferit);
				
				var  path = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.sPath;
				var model = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.getModel();
				
				
					var SelSet1 = model.getProperty(path).SelSet1;
					var CatType1 = model.getProperty(path).CatType1;
				    var PselSet1 = model.getProperty(path).PselSet1;
	 
					var ofilter =  [new sap.ui.model.Filter("Auswahlmge", FilterOperator.EQ, SelSet1),new sap.ui.model.Filter("Katalogart", FilterOperator.EQ, CatType1),new sap.ui.model.Filter("Werks", FilterOperator.EQ, PselSet1)];	
				
					var oList = sap.ui.getCore().byId("_idMunferit");
					var oBinding = oList.getBinding("items");
					oBinding.filter(ofilter);
					
				this._oDialogMunferit.open();
			
			},
			onValueHelpResult : function (oEvent) {
		 //debugger;
			if (!this._oDialogResult) {
				this._oDialogResult = sap.ui.xmlfragment("com.nauticana.demo.view.DialogResult", this);
				this._oDialogResult.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogResult.setModel(this.getView().getModel());
				
						var	IM = this.getModel("EtZqualitaCatModel");
					        this._oDialogResult.setModel(IM,"EtZqualitaCatModel");
			}
		
		  

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogResult.setRememberSelections(bRemember);

		 

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogResult);
			
				var  path = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.sPath;
				var model = oEvent.getSource().mBindingInfos.showValueHelp.binding.aBindings[0].oContext.getModel();
				
				
					var SelSet1 = model.getProperty(path).SelSet1;
					var CatType1 = model.getProperty(path).CatType1;
				    var PselSet1 = model.getProperty(path).PselSet1;
	 
					var ofilter =  [new sap.ui.model.Filter("Auswahlmge", FilterOperator.EQ, SelSet1),new sap.ui.model.Filter("Katalogart", FilterOperator.EQ, CatType1),new sap.ui.model.Filter("Werks", FilterOperator.EQ, PselSet1)];	
				
					var oList = sap.ui.getCore().byId("_idResult");
					var oBinding = oList.getBinding("items");
					oBinding.filter(ofilter);
					
			
			this._oDialogResult.open();
		},
		
		nextInspoper:  function (oEvent) {
		 //debugger; 
		  	var len2 = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection.length;
			
			var InspoperPath =	oEvent.getSource().getBindingContext("InspoperModel").sPath;

	 		var InspoperIndex = parseInt(InspoperPath.substr(20),10);
	 		
		 
			
			if((0 <= InspoperIndex) && (InspoperIndex < len2 - 1)){
					this.getView().byId("__sonucID").setValue("");
				InspoperIndex = InspoperIndex + 1;
						    
			  		if(InspoperIndex < len2){
					this.getView().bindElement({
									path: "/InspoperCollection/" + InspoperIndex ,
						     		model: "InspoperModel"
						    	});	
				var Inspoper = oEvent.getSource().getBindingContext("InspoperModel").oModel.getData().InspoperCollection[InspoperIndex].Inspoper;
		  		
		    	var oList = this.getView().byId("__karakteristikID");
	 	    
		    var collection1 = oList.getBindingInfo("value").binding.getModel().getData().EtZinsCharCollection;
			var index1 = $.inArray(Inspoper, $.map(collection1, function(n){
			    return n.Inspoper;
			}));
			
					var Inspchar = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[index1].Inspchar;
	
		  			var aFilter = [];
				
				aFilter.push(new sap.ui.model.Filter("Inspoper", sap.ui.model.FilterOperator.EQ, Inspoper)); 
				aFilter.push(new sap.ui.model.Filter("Inspchar", sap.ui.model.FilterOperator.EQ, Inspchar));
		    
		    
			this.getView().bindElement({
					path: "/EtZinsCharCollection/" + index1,
		     		model: "EtZinsCharModel"
		    	});	
		    	
		    var oList2 = this.getView().byId("test-list");
			var oBinding = oList2.getBinding("items");
			oBinding.filter(aFilter);
			}
			 sap.ui.getCore().getModel("EtZinsCharModel").refresh();
            sap.ui.getCore().getModel("EtSingleResultsModel").refresh();
		}
	 		 
		},
		onFlushInputs :  function (oEvent) {
		//debugger;
		 
	//	var EtZinsCharPath = oEvent.getSource().getBindingContext("EtZinsCharModel").sPath;
	//	var Inspoper = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getProperty(EtZinsCharPath).Inspoper
	    this.getView().byId("__sonucID").setValue("");
	  
		var oInput = this.getView().byId("test-list").getAggregation("items");
		var len = oInput.length;
		
		if(len>0){
			for(var i=0;i<len;i++){
					oInput[i].mAggregations.content[0].mAggregations.items[0].setValue(null);
					oInput[i].mAggregations.content[0].mAggregations.items[1].setSrc(null)
			}
			
		}
		},
		
		onNextKarakter :  function (oEvent) {
			
			//debugger;
	     //this.getView().byId("__sonucID").setValue("");
			
		   //debugger;
 			var EtZinsCharPath = oEvent.getSource().getBindingContext("EtZinsCharModel").sPath;
		   	
			var len = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection.length;

			var EtZinsCharIndex = parseInt(EtZinsCharPath.substr(22),10);
	
			var Inspoper = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[EtZinsCharIndex].Inspoper;
	   
			var collection1 = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection;
	 
			
			var Inspoperold = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[EtZinsCharIndex].Inspoper;
		    var Inspcharold = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[EtZinsCharIndex].Inspchar;
	
	
	    	if((0 <= EtZinsCharIndex) && (EtZinsCharIndex < len - 1)){
	    		this.onFlushInputs(oEvent);	
				 EtZinsCharIndex = EtZinsCharIndex + 1;
				  var Inspoper = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[EtZinsCharIndex].Inspoper;
		          var Inspchar = oEvent.getSource().getBindingContext("EtZinsCharModel").oModel.getData().EtZinsCharCollection[EtZinsCharIndex].Inspchar;
	
				if(Inspoperold === Inspoper ){
				    
						    
			  		if(EtZinsCharIndex < len  ){
					this.getView().bindElement({
									path: "/EtZinsCharCollection/" + EtZinsCharIndex ,
						     		model: "EtZinsCharModel"
						    	});	
					}

					var aFilter = [];
					aFilter.push(new sap.ui.model.Filter("Inspoper", sap.ui.model.FilterOperator.EQ, Inspoper)); 
				    aFilter.push(new sap.ui.model.Filter("Inspchar", sap.ui.model.FilterOperator.EQ, Inspchar));

					var oList = this.getView().byId("test-list");
					var oBinding = oList.getBinding("items");
					oBinding.filter(aFilter);
			
				}
				 sap.ui.getCore().getModel("EtZinsCharModel").refresh();
            	 sap.ui.getCore().getModel("EtSingleResultsModel").refresh();
		}
			


		},
			_onRouteMatched : function (oEvent) {
				//debugger;
			var oArgs, oView, oSelectedKey, oSelectedInspoper, oSelectedTxtOper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			oSelectedKey  = oArgs.objectId;
			oSelectedInspoper = oArgs.Inspoper;
        	
       // 	 this.getView().getModel("KPSParameters").setData({
        	
        //		oSelectedKey : oSelectedKey,
       // 		oSelectedInspoper : oSelectedInspoper
  
       //     });
        	
        	//	var oSelectedKey =	 this.getView().getModel("KPSParameters").getData().oSelectedKey;
		//	var oSelectedInspoper = this.getView().getModel("KPSParameters").getData().oSelectedInspoper;
  
			
			var InspoperM = sap.ui.getCore().getModel("EtInspoperModel");
			var InsM = {}; 
			InsM.InspoperCollection = InspoperM.getData(); 
			var InspM = new JSONModel(InsM); 
			this.getView().setModel(InspM,"InspoperModel");
            
		 
			var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
			var EtZinsM = {}; 
			EtZinsM.EtZinsCharCollection = EtZinsCharM.getData(); 
			var EtZM = new JSONModel(EtZinsM); 
			this.getView().setModel(EtZM,"EtZinsCharModel"); 
		 
	
			var EtSingleM = sap.ui.getCore().getModel("EtSingleResultsModel");
			var EtSingM = {}; 
			EtSingM.EtSingleResultsCollection = EtSingleM.getData(); 
			var EtSM = new JSONModel(EtSingM); 
			this.getView().setModel(EtSM,"EtSingleResultsModel");

	 		var EtZqualitaCatM = sap.ui.getCore().getModel("EtZqualitaCatModel");
			var EEtCatM = {}; 
			EEtCatM.EtZqualitaCatCollection = EtZqualitaCatM.getData(); 
			var EtCatM = new JSONModel(EEtCatM); 
			this.getView().setModel(EtCatM,"EtZqualitaCatModel");
			
		
		    	
	
		 	var collection1 = this.getView().getModel("InspoperModel").getData().InspoperCollection;
			var index1 = $.inArray(oSelectedInspoper, $.map(collection1, function(n){
			    return n.Inspoper;
			}));
			
			this.getView().bindElement({
					path: "/InspoperCollection/" + index1,
		     		model: "InspoperModel"
		    	});	
		   
		   
		 	
			var collection = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection;
			var index = $.inArray(oSelectedInspoper, $.map(collection, function(n){
			    return n.Inspoper;
			}));
		//	 this.getView().getModel("EtZinsCharModel").getProperty("/EtZinsCharCollection/" + index );


			 //var paths=oEvent.getParameter("arguments").oPath;
				
				//var sObjectId =  oEvent.getParameter("arguments").Id;
				 
				//var	oArgs = 1;
				this.getView().bindElement({
					path: "/EtZinsCharCollection/" + index,
		     		model: "EtZinsCharModel"
		    	});	
	
		   
		    var oSelectedInspchar=null;
		    
			for (var key in collection) {
	    			if (key === 'length' || !collection.hasOwnProperty(key)) continue;
	    			if ( oSelectedInspoper === collection[key].Inspoper){
	    				var value2 = collection[key] ;
	    				oSelectedInspchar = value2.Inspchar;
	    				break;
	    			}
	    	}
			
	
		   	var collection2 = this.getView().getModel("EtSingleResultsModel").getData().EtSingleResultsCollection;
		   	
			var index2 = $.inArray(oSelectedInspoper, $.map(collection2, function(n){
			    return n.Inspoper;
			}));
			
			this.getView().bindElement({
					path: "/EtSingleResultsCollection/" + index2,
		     		model: "EtSingleResultsModel"
		    	});	
		    
		    
		   
		    var oFilters = [ new sap.ui.model.Filter("Inspoper","EQ", oSelectedInspoper), new sap.ui.model.Filter("Inspchar","EQ", oSelectedInspchar) ];
		    
		    
		    var list = this.getView().byId("test-list");
		  	var binding = list.getBinding("items");
		  	binding.filter(oFilters);
		  
		
		    

		},
		onBeforeRendering: function (oEvent) {
			//debugger;
		
			
		},
	
	onSave: function (oEvent) {
	//debugger;
		
		var oView =      this.getView();
		
	//	var oModel1 =	 oEvent.getSource().oPropagatedProperties.oModels.EtZinsCharModel;
	//	var oModel2 =	 oEvent.getSource().oPropagatedProperties.oModels.EtSingleResultsModel;

		var EtInspoperPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.InspoperModel.sPath;
		var EtZinsPath = oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.sPath;
		var EtSingleResulPath =	 oEvent.getSource().oPropagatedProperties.oBindingContexts.EtSingleResultsModel.sPath;
	 
		var InspoperIndex = parseInt(EtInspoperPath.substr(20),10);
    	var EtZinsIndex = parseInt(EtZinsPath.substr(22),10);
		var EtSingleResIndex = parseInt(EtSingleResulPath.substr(27),10);
 	 
 	 
 	// var Code =	aContexts[0].getModel().getProperty(oSelPath).Code;
 	 var  Insplot =    oEvent.oSource.oPropagatedProperties.oBindingContexts.InspoperModel.getProperty(EtInspoperPath).Insplot;
 	 
 	 if(EtZinsIndex > -1){
 	 	 var Inspoper =	oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.getProperty(EtZinsPath).Inspoper;
         var Inspchar =	oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.getProperty(EtZinsPath).Inspchar;
         var LwTolLmt =	oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.getProperty(EtZinsPath).LwTolLmt;
         var UpTolLmt =	oEvent.oSource.oPropagatedProperties.oBindingContexts.EtZinsCharModel.getProperty(EtZinsPath).UpTolLmt;
 	 }
 	 if(EtSingleResIndex > -1){
 	  	  var	ResNo = oEvent.oSource.oPropagatedProperties.oBindingContexts.EtSingleResultsModel.getProperty(EtSingleResulPath).ResNo;
 	 }
 
	//	var Insplot  = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Insplot;
	    //var Inspoper  = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspoper;
	//	var Inspchar  = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Inspchar;
     	
 	 	
 	 	
 	//	var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
	//	var EtSingleM = sap.ui.getCore().getModel("EtSingleResultsModel");
		 
			
		
		//debugger;
		
	/*
		var Status	= oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Status;
		var	MstrChar = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].MstrChar;
		var	VmstrChar = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].VmstrChar;
		var	PmstrChar = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].PmstrChar;
		var	CharDescr = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].CharDescr;
		var	CharType = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].CharType;
		var	Obligatory = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Obligatory;
		var	SingleRes = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].SingleRes;
		var	Scope = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Scope;
		var	ScopeInd = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].ScopeInd;
		var	DocuRequ = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].DocuRequ;
		var	SmplUnit = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex]. SmplUnit;
		var	TargetVal = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].TargetVal;
		var	UpTolLmt = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].UpTolLmt;
		var	LwTolLmt = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].LwTolLmt;
		var	SelSet1 = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].SelSet1;
		var	PselSet1 = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].PselSet1;
		var	CatType1 = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].CatType1;
		var	ResOrg = oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].ResOrg;
		var Closed	=	 oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Closed;
		var	Evaluated	= oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Evaluated;
		var	Evaluation	= oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Evaluation;
		var	MeanValue	= oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].MeanValue;
		var	Code1	= oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].Code1;
		var	CodeGrp1 =  oEvent.oSource.oPropagatedProperties.oModels.EtZinsCharModel.getData().EtZinsCharCollection[EtZinsIndex].CodeGrp1;	 
				 
	*/
       
     //   var ValueList =  [] ;
     //   ValueList = this.getView().byId("__ValueList").getValue();  
        
        
       
        
    
   // var index = 1;
  //  var Values = this.getView().byId("idColumnListItem").getAggregation("cells")[1].getAggregation("items")[index].getAggregation("content")[0].getAggregation("items")[0].getValue();
       /*
      var ResArray = this.getView().byId("idColumnListItem").getAggregation("cells")[1].getAggregation("items");
        var ResArrayList = [];
        
        if (ResArray && ResArray.length) {
				
				for(var i = 0; i < ResArray.length; i++){
					var olist = ResArray[i].getAggregation("content")[0].getAggregation("items")[0].getValue();
				    ResArrayList[i]  = olist ;
				}
			} else {
			
			//	MessageToast.show("No new item was selected.");
		
		}*/
			
			
		/*var Insplot =  "040000001749";
		var Inspchar = "0010";
		var Inspoper = "0010";
		var Closed	=	"";
		var	Evaluated	=	"";
		var	Evaluation	=	"";
		var	MeanValue	=	"125";
		var	Code1	=	"";
		var	CodeGrp1 = "";*/
		
		
	//		var aFilter = [];
	 //	    aFilter.push(new sap.ui.model.Filter("Insplot", "EQ", Insplot)); 
	//	    aFilter.push(new sap.ui.model.Filter("Inspoper","EQ", Inspoper));
	//	    aFilter.push(new sap.ui.model.Filter("Inspchar","EQ", Inspchar));
		   // aFilter.push(new sap.ui.model.Filter("IvTip","EQ", "1"));
		 
		     
	
        var MeanValue =	this.getView().byId("__sonucID").getValue();
        var	SmplUnit = "ADT"; 
        
		var 	ClosedCh='X';
		var 	EvaluatedCh='X';
		
		var LwTolLmtInt =	 parseInt(LwTolLmt);
		var UpTolLmtInt = 	 parseInt(UpTolLmt);
		var MeanValueInt =	 parseInt(MeanValue);
	
		var val = this.IsBeetween(LwTolLmtInt,UpTolLmtInt,MeanValueInt);
		
			if(val){
			var	EvaluationCh ='A';	
				
			}else{
				EvaluationCh='R';
			}
			
 
    //	var sPath = "/EtZinsCharSet";      
    	//EtZinsCharSet"+"?$filter=Insplot"+" eq "+ Insplot + "and" + "Inspchar" + " eq " +Inspchar+ "and" + "Inspoper" +" eq " + Inspoper;
        
       var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_DEMO_SRV"); 
	   var sPath = "/InspLotOpCharSet"+"(Insplot='"+Insplot+"',Inspoper='"+Inspoper+"',Inspchar='"+Inspchar+"')";
        
        
       // var sPath = "/EtZinsCharSet";
         var oPostData = {
         	Insplot:Insplot,
         	MeanValueCh:MeanValue,
        	Inspoper:Inspoper,
        	Inspchar:Inspchar,
        	ClosedCh:ClosedCh,
        	EvaluatedCh:EvaluatedCh,
        	EvaluationCh:EvaluationCh
        //	CharDescr:CharDescr,
        //	SingleRes:SingleRes,
       // 	SelSet1:SelSet1,
        //	ClosedCh:ClosedCh,
        //	EvaluatedCh:EvaluatedCh,
        //	EvaluationCh:EvaluationCh,
        //	ErrClassCh:ErrClassCh,
       // 	ValidValsCh:ValidValsCh,
       // 	NonconfCh:NonconfCh,
       // 	Code1Ch:Code1Ch,
      //  	CodeGrp1Ch:CodeGrp1Ch
        	
         }
         
         	oModel.update(sPath, oPostData,
         	
         	function(oData, response) {
					 MessageToast.show("Başarılı bir şekilde Kaydedildi", {
						duration: 3000
					});
			          console.log(oData);
			          console.log(response);
			        },
			function(e) {
			          sap.m.MessageToast.show("Kaydetme sırasında hata oluştu", {
						duration: 3000
					});
			          //console.log("Update failed", JSON.stringify(e));
			        });
		        
            oModel.attachRequestSent(function(){
               sap.ui.core.BusyIndicator.show(5);
    		});
			oModel.attachRequestCompleted(function(){
                sap.ui.core.BusyIndicator.hide();
                sap.m.MessageToast.show("Başarılı bir şekilde Kaydedildi", {
						duration: 3000
					});
            });
            sap.ui.getCore().getModel("EtZinsCharModel").refresh();
            sap.ui.getCore().getModel("EtSingleResultsModel").refresh();
            
            
/*

		var oPostData =	EtZinsCharM.getData();
		var oPostSingleData =	EtSingleM.getData();
		
	//	delete oPostData.__metadata;
		
	 
	//	Inspoper = "0010";
	//	Inspchar = "0010";
		
		var selPostData = [];
		 if (oPostData && oPostData.length) {
				
				for(var i = 0; i < oPostData.length; i++){
					
					if (( Insplot === oPostData[i].Insplot)&&
					( Inspoper === oPostData[i].Inspoper)&&
					( Inspchar === oPostData[i].Inspchar)){
	    				oPostData[i].MeanValue=MeanValue;
	    				oPostData[i].SmplUnit=SmplUnit;
	    			//	selPostData = oPostData[i];
	    			//	delete selPostData.__metadata;
	    		//	break;
	    			}
	    				delete oPostData[i].__metadata;
				}
			} else {
			
			//	MessageToast.show("No new item was selected.");
		}
		


	oModel.update(sPath, oPostData,function(oData, response) {

			          console.log(oData);
			        }, function(e) {
			          console.log("Update failed", JSON.stringify(e));
			        });
								 
								 

 var ResArray = this.getView().byId("idColumnListItem").getAggregation("cells")[1].getAggregation("items");
 var ResArrayLen = this.getView().byId("idColumnListItem").getAggregation("cells")[1].getAggregation("items").length;   
    //var sPath2 = "/EtSingleResultsSet";
    var sPath3 = "/EtSingleResultsSet"+"('"+Insplot+"')";
 
		
	//debugger;
	var selSingleData = [];
		 if (oPostSingleData && oPostSingleData.length) {
				
				for(var i = 1; i < oPostSingleData.length+1; i++){
					
					if (( Insplot === oPostSingleData[i-1].Insplot)&&
					( Inspoper === oPostSingleData[i-1].Inspoper)&&
					( Inspchar === oPostSingleData[i-1].Inspchar) 
				 ){
				 	
				  
					if(oPostSingleData[i].ResNo != null){
				 //	
				 		oPostSingleData[i-1].ResValue = ResArray[i-1].getAggregation("content")[0].getAggregation("items")[0].getValue();
					}else{
						oPostSingleData[i-1].ResNo = i;
			     		oPostSingleData[i-1].ResValue =ResArray[i-1].getAggregation("content")[0].getAggregation("items")[0].getValue();
					}
	    			
	    			//	selSingleData = oPostSingleData[i];
	    			//	delete selSingleData.__metadata;
	    		//	break;
	    			}
	    				delete oPostSingleData[i-1].__metadata;
				}
			} else {
			
			//	MessageToast.show("No new item was selected.");
		}
		
		oModel.update(sPath3, oPostSingleData,function(oData, response) {
        
        
			          console.log(oData);
			        }, function(e) {
			          console.log("Update failed", JSON.stringify(e));
			        });
		
     //debugger;
   
	*/

			},
	 
		onAfterRendering: function(oEvent) {
			//debugger;
			
	
	
		},
		
		validateFlag: function (oEvent) {
			//debugger;
			var newValue = oEvent.getParameter("newValue");
		//	var value = oEvent.getParameter("value");
			
		 
			var EtZinsCharPath =  oEvent.getSource().oPropagatedProperties.oBindingContexts.EtZinsCharModel.getPath();
			var EtZinsCharIndex = parseInt(EtZinsCharPath.substr(22),10);
	
 	
		    var LwTolLmt = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].LwTolLmt;
			var UpTolLmt = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].UpTolLmt;
			var TargetVal = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].TargetVal;
			
			
			var LwTolLmt = parseInt(LwTolLmt,10);
			var UpTolLmt = parseInt(UpTolLmt,10);
			var newValue  = parseInt(newValue,10);
		//	var TargetVal	= parseInt(TargetVal,10);
			/*if(){
				
			}*/
			//	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
        	//	var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
          	var icon4 = "sap-icon://accept";
    	    var icon5 = "sap-icon://decline";
	 
	 
         var oFlag = this.getView().byId("__flagResult");
		 //var oInput = this.getView().byId("__sonucID");
		 
	//	 var between = new sap.ui.model.Filter(newValue, sap.ui.model.FilterOperator.BT, LwTolLmt, UpTolLmt);
	 	var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
		 //debugger;
		 if( ((newValue === TargetVal) || val === true )  ){
			    	oFlag.setSrc(icon4);
			    	oFlag.setColor("green");
			     
			    	 
		 }else if(isNaN(newValue)){
		 	oFlag.setSrc(null);
		 	
		 }
		 else{
	     		  	oFlag.setSrc(icon5);
	     	     oFlag.setColor("red");
	     }
	 
		},
		
		validateFlagMunferit  : function (oEvent) {
			//debugger;
			var newValue = oEvent.getParameter("newValue");
		//	var value = oEvent.getParameter("value");
			
		    var oFlag = this.getView().byId("test-list").getAggregation("items");
			var oInput = this.getView().byId("test-list").getAggregation("items");
			var oSelectIndex = oEvent.getSource().getParent().sId;
			var oSelectIdx = parseInt(oSelectIndex.substr(47),10);
			
			
			var EtZinsCharPath =  oEvent.getSource().oPropagatedProperties.oBindingContexts.EtZinsCharModel.getPath();
			var EtZinsCharIndex = parseInt(EtZinsCharPath.substr(22),10);
	
 	
		    var LwTolLmt = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].LwTolLmt;
			var UpTolLmt = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].UpTolLmt;
			var TargetVal = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtZinsCharIndex].TargetVal;
			
			
			var LwTolLmt = parseInt(LwTolLmt,10);
			var UpTolLmt = parseInt(UpTolLmt,10);
			var newValue  = parseInt(newValue,10);
		//	var TargetVal	= parseInt(TargetVal,10);
			/*if(){
				
			}*/
			//	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
        	//	var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
          	var icon4 = "sap-icon://accept";
    	    var icon5 = "sap-icon://decline";
	 
		   
        // var oFlag = this.getView().byId("__flagResult");
		 //var oInput = this.getView().byId("__sonucID");
		 
		// var between = new sap.ui.model.Filter(newValue, sap.ui.model.FilterOperator.BT, LwTolLmt, UpTolLmt);
	 
		var val = this.IsBeetween(LwTolLmt,UpTolLmt,newValue);
	 
		 //debugger;
		 if( ((newValue === TargetVal) || ( val === true ) )  ){
			    	oFlag[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setSrc(icon4) ;
			    	oFlag[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setColor("green") ;
		 }else if(isNaN(newValue) ){
	     	    	oFlag[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setSrc("");
	     	     
	     }else{
	     	     	oFlag[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setSrc(icon5);
	     	    	oFlag[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setColor("red") ;
	     }
	     
	     
		},
 
		addMunferit  : function (oEvent) {
			//debugger;
		
		var Path = oEvent.oSource.oParent.oPropagatedProperties.oBindingContexts.EtZinsCharModel.sPath;
		var EtSingleIndex = parseInt(Path.substr(22),10);
		
		var Path2 = oEvent.oSource.oParent.oPropagatedProperties.oBindingContexts.EtSingleResultsModel.sPath;
		var EtSingleIndex2 = parseInt(Path2.substr(27),10);
		
	
		
	//	var MunferitModel = {};  MunferitModel.EtSingleResultsCollection = new Array();
		// this.getView().getModel("EtSingleResultsModel").getData();
			var Insplot = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtSingleIndex].Insplot;
		
			var Inspoper = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtSingleIndex].Inspoper;
			var Inspchar = this.getView().getModel("EtZinsCharModel").getData().EtZinsCharCollection[EtSingleIndex].Inspchar;
			
		
			
		
		var oSingleData = this.getView().getModel("EtSingleResultsModel").getData().EtSingleResultsCollection;
		var newSingleData = [];
		var resleng = 0;
		
		if (oSingleData && oSingleData.length) {
				 
				for(var i = 0; i < oSingleData.length; i++){
				 
				
					
					if (( Insplot === oSingleData[i].Insplot) &&
					( Inspoper === oSingleData[i].Inspoper) &&
					( Inspchar === oSingleData[i].Inspchar) ){
						resleng++;
						newSingleData = oSingleData[i];
						newSingleData.Insplot =  oSingleData[i].Insplot;
	    				newSingleData.Inspoper = oSingleData[i].Inspoper;
	    				newSingleData.Inspchar = oSingleData[i].Inspchar;
						
					}
					 	
				}
		}
		//debugger;
		if (resleng ===0){
			var Resno = 1;
			newSingleData = this.singleValueReturn();
	    	newSingleData.Insplot = Insplot;
	    	newSingleData.Inspoper = Inspoper;
	    	newSingleData.Inspchar = Inspchar;
	    	newSingleData.ResNo = Resno.toString().padStart(4, "0");
		}else{
			resleng++;
			newSingleData.ResNo = resleng.toString().padStart(4, "0");
		}
				 
		this.getView().getModel("EtSingleResultsModel").getProperty('/EtSingleResultsCollection').push(newSingleData);

		this.getView().getModel("EtSingleResultsModel").updateBindings();
  
				/*	 	var Resno = parseInt(oSingleData[i].ResNo.replace(/^0+/, ''),10);
							
							Resno++;
							newSingleData = oSingleData[i];
	    					newSingleData.ResNo = Resno.toString().padStart(4, "0");
						break;
							Resno = 1;
							newSingleData = oSingleData[i];
	    					newSingleData.ResNo = Resno.toString().padStart(4, "0");
						}
	    			
	    			
	    			//	delete selPostData.__metadata;
	    		 	break;
	    			}
	    			Resno = 1;
	    			newSingleData = this.singleValueReturn();
	    		    newSingleData.Insplot = Insplot;
	    			newSingleData.Inspoper = Inspoper;
	    			newSingleData.Inspchar = Inspchar;
	    			newSingleData.ResNo = Resno.toString().padStart(4, "0");
	    	//		newSingleData.ResNo = ResNonew.toString().padStart(4, "0");
	    			//	delete oPostData[i].__metadata;
				}
				
	    			
				
			} else {
			
			//	MessageToast.show("No new item was selected.");
			}*/
		//debugger;
	 
	 /*
		var oModel = this.getView().getModel("EtSingleResultsModel");
		var aData  = oModel.getProperty("/EtSingleResultsCollection");
		aData.push.apply(aData, newSingleData);
		oModel.setProperty("/EtSingleResultsCollection", aData);*/


	//		newSingleData = [];
		},
		
	  	singleValueReturn  : function (oEvent) {
	  	
	  	var oSingleData = [];
	   	 
	  		oSingleData = {	
	  		"Insplot" : "" ,
			"Inspoper" : "" ,
			"Inspchar" : "" ,
			"ResNo" : "" ,
	  		"AddInfo1" : ""  ,
			"AddInfo2" : "" ,
			"Code1" : "" ,
			"Code2" : "" ,
			"Code3" : "" ,
			"Code4" : "" ,
			"Code5" : "" ,
			"CodeGrp1" : "" ,
			"CodeGrp2" : "" ,
			"CodeGrp3" : "" ,
			"CodeGrp4" : "" ,
			"CodeGrp5" : "" ,
			"Defects" : "" ,
			"DiffDecPlaces" : "" ,
			"ErrClass" : "" ,
			"ExtNo" : "" ,
			"InpprocReady" : "" ,
			"InspDate" : "" ,
			"InspTime" : "" ,
			"Inspector" : "" ,
			"Inspsample" : "" ,
			"LastRes" : "" ,
			"OriginalInput" : "" ,
			"Remark" : "" ,
			"ResAttr" : "" ,
			"ResInval" : "" ,
			"ResValuat" : "" ,
			"ResValue" : "" ,
			"__metadata" : ""
	  	};

	  		return oSingleData;
	  	},

		handleCloseMunferit  : function (oEvent) {
			 //debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oFlag = this.getView().byId("test-list").getAggregation("items");
			var oInput = this.getView().byId("test-list").getAggregation("items");
			var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			var oSelectIdx = parseInt(oSelectIndex.substr(80),10);
				
		//	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
        //	var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
        
        	var icon4 = "sap-icon://accept";
    	    var icon5 = "sap-icon://decline";
	 
			var aTokens = [];
    
			if (aContexts && aContexts.length) {
				
				for(var i = 0; i < aContexts.length; i++){
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
				    var token1 = new sap.m.Token({key: oSel.Code,text: oSel.Bewertung});
				    aTokens[i]  = token1 ;
				}
			 
			oInput[oSelectIdx].mAggregations.content[0].mAggregations.items[0].setValue(aTokens[0].getProperty("key"));
				
			  //  oInput.setValue(aTokens[0].getProperty("key"));
			    if(aTokens[0].getProperty("text")==='A'){
			    	oInput[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setSrc(icon4) ;
			    	oInput[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setColor("green");
			    }else if(aTokens[0].getProperty("text")==='R'){
			    	oInput[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setSrc(icon5);
			    	oInput[oSelectIdx].mAggregations.content[0].mAggregations.items[1].setColor("red");
			    }
			} else {
			
				 sap.m.MessageToast.show("No new item was selected.");
		
			}
		},
		handleCloseResult  : function (oEvent) {
				 //debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oInput = this.getView().byId("__sonucID");
			var oFlag = this.getView().byId("__flagResult");
			
		//	var icon4 = 	this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].kabul;
        //   var icon5 =     this.getOwnerComponent().getModel("mIcon").getData().IconCollection[0].red;
        
		    var icon4 = "sap-icon://accept";
    	    var icon5 = "sap-icon://decline";
  
			var aTokens = [];
    
			if (aContexts && aContexts.length) {
				
				for(var i = 0; i < aContexts.length; i++){
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
				    var token1 = new sap.m.Token({key: oSel.Code,text: oSel.Bewertung});
				    aTokens[i]  = token1 ;
				}
			    oInput.setValue(aTokens[0].getProperty("key"));
			    if(aTokens[0].getProperty("text")==='A'){
			    	oFlag.setSrc(icon4);
			    	oFlag.setColor("green");
			    }else if(aTokens[0].getProperty("text")==='R'){
			    	oFlag.setSrc(icon5);
			    	oFlag.setColor("red");
			    }
			} else {
			
				 sap.m.MessageToast.show("No new item was selected.");
		
			}
		 
	 
				 
		},
		
		onNavBack : function () {
			 //debugger;
			 // clear input values
			// this.getView().byId("__sonucID").setValue("");
			  //this.getView().byId("__sonucID").setValue("");
			 // clear models
		//	 var dataModel = this.getView().getModel("EtSingleResultsModel"); 
		//		if(dataModel){
		//		    dataModel.setData(null);
		//		    dataModel.updateBindings(true);
		//		}

		 
			//  this.getView().setBindingContext(null);
			  
			 // back nav
				// var oHistory = sap.ui.core.routing.History.getInstance();
				// var sPreviousHash = oHistory.getPreviousHash();		
 
				// if (sPreviousHash !== undefined) {
				// 	this.getRouter().navTo("master", {}, true);
					history.go(-1);
				// } else {
					// this.getRouter().navTo("master", {}, true);
				// }
			},
			createGroupHeader : function () {
				
			},
			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Event handler when the share by E-Mail button has been clicked
			 * @public
			 */
			onShareEmailPress : function () {
				var oViewModel = this.getModel("detailView");

				sap.m.URLHelper.triggerEmail(
					null,
					oViewModel.getProperty("/shareSendEmailSubject"),
					oViewModel.getProperty("/shareSendEmailMessage")
				);
			},

			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress : function () {
				var oViewModel = this.getModel("detailView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name : "sap.collaboration.components.fiori.sharing.dialog",
						settings : {
							object :{
								id : location.href,
								share : oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});

				oShareDialog.open();
			},
 
	 		/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */

			/**
			 * Binds the view to the object path and expands the aggregated line items.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
	 		
	 //debugger;
		 
		/*
			 var sObjectId =  oEvent.getParameter("arguments").objectId;
		 	 this.getOwnerComponent().getModel().metadataLoaded().then( function() {
					var sObjectPath = this.getOwnerComponent().getModel().createKey("EtQalsSet", {
						Prueflos :  sObjectId
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
*/
		    
			 },

			/**
			 * Binds the view to the object path. Makes sure that detail view displays
			 * a busy indicator while data for the corresponding element binding is loaded.
			 * @function
			 * @param {string} sObjectPath path to the object to be bound to the view.
			 * @private
			 */
			_bindView : function (sObjectPath) {
			  
				// Set busy indicator during view binding
				var oViewModel = this.getModel("detailView");
		 		// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				oViewModel.setProperty("/busy", false);

				this.getView().bindElement({
					path : sObjectPath,
					events: {
						change : this._onBindingChange.bind(this),
						dataRequested : function () {
							oViewModel.setProperty("/busy", true);
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
			},

			_onBindingChange : function () {
				  
				var oView = this.getView(),
					oElementBinding = oView.getElementBinding();

				// No data for the binding
				if (!oElementBinding.getBoundContext()) {
					this.getRouter().getTargets().display("detailObjectNotFound");
					// if object could not be found, the selection in the master list
					// does not make sense anymore.
				//	this.getOwnerComponent().oListSelector.clearMasterListSelection();
					return;
				}

			
			},
		
			_onMetadataLoaded : function () {
	 			//debugger;
				// Store original busy indicator delay for the detail view
				var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
					oViewModel = this.getModel("detailView");

				// Make sure busy indicator is displayed immediately when
				// detail view is displayed for the first time
				oViewModel.setProperty("/delay", 0);

				// Binding the view will set it to not busy - so the view is always busy if it is not bound
				oViewModel.setProperty("/busy", true);
				// Restore original busy indicator delay for the detail view
				oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
			},
				onExit : function () {
					if (this._oDialogMunferit) {
						this._oDialogMunferit.destroy();
					}
					if (this._oDialogResult) {
						this._oDialogResult.destroy();
					}
				}
			//			_getDialogInspoper : function(list) {
			/*	//debugger;
					if (!this._oDialogInspoper) {
						this._oDialogInspoper = sap.ui.xmlfragment("com.nauticana.demo.view.DialogInspoper", this,true);
						var	IM = this.getModel("InspoperModel");
					    this._oDialogInspoper.setModel(IM);
					}
						this.getView().addDependent(this._oDialogInspoper);
				
			           // toggle compact style
						jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogInspoper);
						
					return this._oDialogInspoper;*/
		//	},
		//	list_to_tree : function(list) {
	/*		//debugger;
			    var map = {}, node, roots = [], i;
			    for (i = 0; i < list.length; i += 1) {
			        map[list[i].id] = i; // initialize the map
			        list[i].children = []; // initialize the children
			    }
			    for (i = 0; i < list.length; i += 1) {
			        node = list[i];
			        if (node.parentId !== "0") {
			            // if you have dangling branches check that map[node.parentId] exists
			            list[map[node.parentId]].children.push(node);
			        } else {
			            roots.push(node);
			        }
			    }
			    return roots;*/
		//	}
			
			

		});

	}
);