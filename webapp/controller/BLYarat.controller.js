/*global history */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/nauticana/demo/model/formatter",
	"com/nauticana/demo/model/grouper",
	"com/nauticana/demo/model/GroupSortState",
	"sap/ui/core/mvc/Controller"
], function(BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.BLYarat", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onInit: function() {
			//debugger;

			var oRouter = this.getRouter();

			oRouter.getRoute("blyarat").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			this.setModel(oViewModel, "detailView");

			var Oncelikonay = new JSONModel(jQuery.sap.getModulePath("com.nauticana.demo.model", "/oncelik.json"));

			sap.ui.getCore().setModel(Oncelikonay, "OncelikModel");

		},

		_onRouteMatched: function(oEvent) {

		},

		onValueHelpHataKod: function(oEvent) {

			//debugger;
			if (!this._oDialogHataKod) {
				this._oDialogHataKod = sap.ui.xmlfragment("com.nauticana.demo.view.DialogBYHata", this);
				this._oDialogHataKod.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogHataKod.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogHataKod.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogHataKod.setRememberSelections(bRemember);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogHataKod);

			var Qmart = sap.ui.getCore().getModel("CatHeadSetModel").getData()[0].Qmart;
			var Rbnr = sap.ui.getCore().getModel("CatHeadSetModel").getData()[0].Rbnr;
			var Sakat = sap.ui.getCore().getModel("CatHeadSetModel").getData()[0].Sakat;
			//var Uname = sap.ui.getCore().getModel("UserAuthModel").getData().Uname;
			var ofilter = [new sap.ui.model.Filter("Qmart", FilterOperator.EQ, Qmart), new sap.ui.model.Filter("Rbnr", FilterOperator.EQ, Rbnr),
				new sap.ui.model.Filter("Qkatart", FilterOperator.EQ, Sakat)
			];

			var IM = sap.ui.getCore().getModel("CatDetlSetModel");

			var NotifiCatDelM = {};
			NotifiCatDelM.NotifiCatDelCollection = IM.getData();
			var NotifiCatDelModel = new JSONModel(NotifiCatDelM);
			//this.getView().setModel(NotifiCatDelModel,"NotifiCatDelModel"); 
			this._oDialogHataKod.setModel(NotifiCatDelModel, "NotifiCatDelModel");

			var oList = sap.ui.getCore().byId("_hataKod");
			var oBinding = oList.getBinding("items");
			oBinding.filter(ofilter);

			this._oDialogHataKod.open();

		},

		onValueHelpOncelik: function(oEvent) {
			//debugger;
			if (!this._oDialogOncelik) {
				this._oDialogOncelik = sap.ui.xmlfragment("com.nauticana.demo.view.DialogOncelik", this);
				this._oDialogOncelik.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogOncelik.setModel(this.getView().getModel());
			}
			var IM = sap.ui.getCore().getModel("OncelikModel");

			this._oDialogOncelik.setModel(IM, "OncelikModel");

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogOncelik.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogOncelik.setRememberSelections(bRemember);

			// clear the old search filter
			//this._oDialogOncelik.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogOncelik);
			this._oDialogOncelik.open();
		},

		onValueHelpQmart: function(oEvent) {
			//debugger;
			if (!this._oDialogQmart) {
				this._oDialogQmart = sap.ui.xmlfragment("com.nauticana.demo.view.DialogQmart", this);
				this._oDialogQmart.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogQmart.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = false;
			this._oDialogQmart.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogQmart.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogQmart.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogQmart);
			this._oDialogQmart.open();

		},

		handleCloseQmart: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oInput = this.getView().byId("__bilturu");

			var value = oEvent.getParameter("selectedItem").mProperties.title;

			if (aContexts && aContexts.length) {

				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");
				var Uname = sap.ui.getCore().getModel("UserAuthModel").getData().Uname;
				oInput.setValue(value);

				var ofilter10 = [new Filter("Qmart", FilterOperator.EQ, value), new Filter("IvUname", FilterOperator.EQ, Uname)];

				var sPath10 = "/NotifiCatHeadSet";

				oModel.read(sPath10, {
					filters: ofilter10,
					success: function(oData, response) {
						var jsonArray = [];
						jsonArray = response.data.results;

						var oJSONModel = new sap.ui.model.json.JSONModel();
						oJSONModel.setData(jsonArray);
						sap.ui.getCore().setModel(oJSONModel, "CatHeadSetModel");

						console.log(jsonArray);

					},
					error: function(oError) {
						console.log("Error" + oError.responseText)
					}
				});

				var ofilter11 = [new Filter("Qmart", FilterOperator.EQ, value), new Filter("IvUname", FilterOperator.EQ, Uname)];

				var sPath11 = "/NotifiCatDetlSet";

				oModel.read(sPath11, {
					filters: ofilter11,
					success: function(oData, response) {
						var jsonArray = [];
						jsonArray = response.data.results;

						var oJSONModel = new sap.ui.model.json.JSONModel();
						oJSONModel.setData(jsonArray);
						sap.ui.getCore().setModel(oJSONModel, "CatDetlSetModel");

						console.log(jsonArray);

					},
					error: function(oError) {
						console.log("Error" + oError.responseText)
					}
				});

			} else {

				//	MessageToast.show("No new item was selected.");

			}
			//	oEvent.getSource().getBinding("items").filter([]);
			//	this._oDialogQmart.close();
		},
		handleCloseOncelik: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oInput = aContexts[0].getModel().getProperty(oSelPath).No;

			this.getView().byId("__oncelik").setValue(oInput);
		},

		handleCloseHkod: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			var oSelPath = aContexts[0].sPath;
			var oHata = aContexts[0].getModel().getProperty(oSelPath);
			var oInputHataKod = oHata.Code;

			var oInputHataKod2 = oHata.Kurztext;

			this.getView().byId("__hatakod").setValue(oInputHataKod);
			this.getView().byId("__hatakod").data({"Qcodegrp":oHata.Qcodegrp});
			this.getView().byId("__hatakod").data({"Qkatart":oHata.Qkatart});
			this.getView().byId("__hatatext").setValue(oInputHataKod2);

			this._oDialogHataKod.close();
		},
		onNavBack: function() {

			this.getView().byId("__oncelik").setValue("");
			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
				history.go(-1);
			} else {
				this.getRouter().navTo("master", {}, true);
			}

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onBeforeRendering: function() {

			/*	  	var NotifiCatGroup = {}; 
					NotifiCatGroup.NotifiCatGroupCollection = new Array( ); 
				
			    	var collection1 = this.getView().getModel("NotifiCatDetlSetModel").getData().NotifiCatCollection;
					var collection2 = this.getView().getModel("NotifiCatHeadSetModel").getData().NotifiCatHeadCollection;
				for (var key in collection2) {	
					if (key === 'length' || !collection2.hasOwnProperty(key)) continue;
					for (var key1 in collection1) {
			    			if (key1 === 'length' || !collection1.hasOwnProperty(key1)) continue;
			    			if ((collection2[key].Qmart === collection1[key1].Qmart) && (collection2[key].Rbnr === collection1[key1].Rbnr) && (collection2[key].Sakat === collection1[key1].Qkatart)){
			    			 
			    			 NotifiCatGroup.NotifiCatGroupCollection.push(collection1[key1]);
			    				
			    			//	break;
			    			}
			    	}
				}
				
				var NotifiCatGroupDM = new JSONModel(NotifiCatGroup); 
				this.getView().setModel(NotifiCatGroupDM,"NotifiCatGroupModel"); 
				
				 
				
						this.getView().getModel("NotifiCatGroupModel").refresh();*/

		},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		onAfterRendering: function() {

		},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLYarat
		 */
		//	onExit: function() {
		//
		//	}

		//------------------------------------------------------------------------------------------------------------------------//

		onSaveKarar: function(oEvent) {
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/NotifCreateSet";
			
			var Qmart     = this.byId("__bilturu").getValue();
			var Matnr     = this.byId("__malzeme").getValue();
			var Batch     = this.byId("__partino").getValue();
			var Serialno  = this.byId("__serino").getValue();
			
			var Catalogue = this.getView().byId("__hatakod").data("Qkatart");
			var CodeGroup = this.getView().byId("__hatakod").data("Qcodegrp");
			var Code      = this.byId("__hatakod").getValue();
			var ShortText = this.byId("__tanim").getValue();
			var VendNo    = this.byId("__satici").getValue();
			var CustNo    = this.byId("__mustari").getValue();
			var Priority  = this.byId("__oncelik").getValue();
			var Longtext  = this.byId("__aciklama").getValue();
			
			var oPostData = {
				Qmart:     Qmart,
				Matnr:     Matnr,
				Batch:     Batch,
				Serialno:  Serialno ,
				Catalogue: Catalogue  ,
				CodeGroup: CodeGroup  ,
				Code:      Code     ,
				ShortText: ShortText,
				VendNo:    VendNo   ,
				CustNo:    CustNo   ,
				Priority:  Priority ,
				Longtext:  Longtext,
				Return:    "",
				Message:   ""
			};
			
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show("Bildirim Oluşturuldu");	
						history.go(-1);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		}

	});

});