sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/unified/DateRange",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, BaseController, JSONModel, DateRange, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("com.nauticana.demo.controller.HomePageBildirim", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.HomePageBildirim
		 */
		onInit: function() {

			//debugger;

		},
		onCreateBildirim: function(oEvent) {

			this.getOwnerComponent().getRouter().navTo("blyarat", true);
		},
		onDisplayBildirim: function(oEvent) {

			this.getOwnerComponent().getRouter().navTo("bllistelesec", true);
		},
		onNavBack: function() {
			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			// if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
				history.go(-1);
			// } else {
			// 	this.getRouter().navTo("master", {}, true);
			// }
		},
		
		onBildirim: function(oEvent){
			this.getOwnerComponent().getRouter().navTo("notif", true);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.HomePageBildirim
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.HomePageBildirim
		 */
		onAfterRendering: function() {

			/*		//debugger;
				var EtNotifAuthM = sap.ui.getCore().getModel("EtNotifAuthModel");
				var EtNotif = {};  	
				EtNotif.EtNotifAuthMCollection = EtNotifAuthM.getData(); 
				var EtNotifM = new JSONModel(EtNotif); 
				this.getView().setModel(EtNotifM,"EtNotifMModel");
				
				var NotifAuthM = sap.ui.getCore().getModel("NotifAuthModel"); 
				var Notif = {};  	
				Notif.NotifAuthMCollection = NotifAuthM.getData(); 
				var NotifM = new JSONModel(Notif); 
				this.getView().setModel(NotifM,"NotifMModel");
		
		
			*/
		}

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.HomePageBildirim
		 */
		//	onExit: function() {
		//
		//	}

	});

});