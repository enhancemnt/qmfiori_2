sap.ui.define([
		"com/nauticana/demo/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("com.nauticana.demo.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);