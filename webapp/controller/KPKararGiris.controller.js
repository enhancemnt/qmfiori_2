/*global location */
sap.ui.define([
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"com/nauticana/demo/model/formatter",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/StandardListItem',
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator"
], function(BaseController, JSONModel, Dialog, List, StandardListItem, Button, formatter, MessageBox, MessageToast, History, Filter,
	FilterOperator) {
	"use strict";

	return BaseController.extend("com.nauticana.demo.controller.KPKararGiris", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {

			var oRouter = this.getRouter();

			oRouter.getRoute("kpkarargiris").attachMatched(this._onRouteMatched, this);

			this._oView = this.getView();
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0
			});

			var KPSSonuc = new JSONModel({
				enabled: true
			});

			this.setModel(KPSSonuc, "oSonuc");

			this.setModel(oViewModel, "detailView");

			var SelectItemModel = new JSONModel({

				Selected: false,
				Codegruppe: null,
				Auswahlmge: null
			});

			this.setModel(SelectItemModel, "SelectItemModel");

			var SelectItemModel2 = new JSONModel({
				objectId: null,
				Art: null,
				werks: null

			});

			this.setModel(SelectItemModel2, "SelectItemModel2");

		},

		onSelectionChange: function(oEvent) {

			//debugger;
			var oSelect = oEvent.getSource();

			var selectedItemPath = oSelect.getSelectedItem().oBindingContexts.EtZinsCharModel.sPath;
			var EtZinsIndex = parseInt(selectedItemPath.substr(22), 10);
			var Insplot = oSelect.getSelectedItem().oBindingContexts.EtZinsCharModel.getModel().getData().EtZinsCharCollection[EtZinsIndex].Insplot;

			var Art = sap.ui.getCore().getModel("EtQalsSetModel").getData()[0].Art;
			var Werks = sap.ui.getCore().getModel("EtQalsSetModel").getData()[0].Werk;

			//var Selected = true;

			this.getView().getModel("SelectItemModel2").setData({
				objectId: Insplot,
				Art: Art,
				Werks: Werks
					//	Selected : Selected
			});

			this.getView().byId("__kararkoduID").setValue("");
			this.getView().byId("__koddegerlemesiID").setValue("");
			this.getView().byId("__kalitepuanID").setValue("");

		},
		myFormatterDecision: function(oEvent) {
			//debugger;
			return "test";
		},

		onValueHelpDes: function(oEvent) {
			//debugger;
			//  var Selected = this.getModel("SelectItemModel").getData().Selected;

			//	if(Selected){

			this._getDialogDecision().open();

			//	}else {

			//		sap.m.MessageToast.show("Seçim Yapılmadı Seçim Yapın");

			//	}	
		},
		handleCloseDecision: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts) {

				var oSelPath = aContexts[0].sPath;

				var Code = aContexts[0].getModel().getProperty(oSelPath).Code;
				var Codegruppe = aContexts[0].getModel().getProperty(oSelPath).Codegruppe;
				var Bewertung = aContexts[0].getModel().getProperty(oSelPath).Bewertung;
				var Qkennzahl = aContexts[0].getModel().getProperty(oSelPath).Qkennzahl;
				var Auswahlmge = aContexts[0].getModel().getProperty(oSelPath).Auswahlmge;

				this.getView().getModel("SelectItemModel").setData({
					Codegruppe: Codegruppe,
					Auswahlmge: Auswahlmge,
					Code: Code
				});

				this.getView().byId("__kararkoduID").setValue(Code);
				this.getView().byId("__koddegerlemesiID").setValue(Bewertung);
				this.getView().byId("__kalitepuanID").setValue(Qkennzahl);
			}

			//	this._getDialogDecision.close();

		},
		_getDialogDecision: function(oEvent) {
			//debugger;

			if (!this._oDialogDecision) {
				this._oDialogDecision = sap.ui.xmlfragment("com.nauticana.demo.view.DialogDecision", this, true);
				this._oDialogDecision.setModel(this.getView().getModel("i18n"), "i18n");
			}
			var IM = this.getModel("EtZusageDecisionModel");
			this._oDialogDecision.setModel(IM);
			this.getView().addDependent(this._oDialogDecision);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogDecision);

			var Art = this.getModel("SelectItemModel2").getData().Art;
			var Werks = this.getModel("SelectItemModel2").getData().Werks;

			// var ofilter =  [new sap.ui.model.Filter("Art", FilterOperator.EQ, Art),new sap.ui.model.Filter("Werks", FilterOperator.EQ, Werks)];	
			var ofilter =  [new sap.ui.model.Filter("Art", FilterOperator.EQ, this.art),new sap.ui.model.Filter("Werks", FilterOperator.EQ, this.werks)];	
			// var ofilter = [];
			var oList = sap.ui.getCore().byId("_idDecision");
			var oBinding = oList.getBinding("items");
			oBinding.filter(ofilter);

			return this._oDialogDecision;
		},
		_onRouteMatched: function(oEvent) {
			//debugger;
			var oArgs, oView, oSelectedKey, oSelectedInspoper;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();
			oSelectedKey = oArgs.objectId;
			this.ObjectId = oSelectedKey;
			this.art = oArgs.art;
			this.werks = oArgs.werks;

			var EtQalsSetM = sap.ui.getCore().getModel("EtQalsSetModel");
			var EtQals = {};
			EtQals.EtQalsCollection = EtQalsSetM.getData();
			var EtQalsM = new JSONModel(EtQals);
			this.getView().setModel(EtQalsM, "EtQalsSetModel");

			var EtZinsCharM = sap.ui.getCore().getModel("EtZinsCharModel");
			var EtZinsM = {};
			EtZinsM.EtZinsCharCollection = EtZinsCharM.getData();
			var EtZM = new JSONModel(EtZinsM);
			this.getView().setModel(EtZM, "EtZinsCharModel");

			var EtZusageDecisionM = sap.ui.getCore().getModel("EtZusageDecisionSetModel");
			var EtZusageM = {};
			EtZusageM.EtZusageDecisionCollection = EtZusageDecisionM.getData();
			var EtusageM = new JSONModel(EtZusageM);
			this.getView().setModel(EtusageM, "EtZusageDecisionModel");

			var Evaluation = new sap.ui.model.Filter("Evaluation", sap.ui.model.FilterOperator.EQ, 'R');

			var Obligatory = new sap.ui.model.Filter("Obligatory", sap.ui.model.FilterOperator.EQ, 'X');

			var Code1 = new sap.ui.model.Filter("Code1", sap.ui.model.FilterOperator.EQ, '');
			var MeanValue = new sap.ui.model.Filter("MeanValue", sap.ui.model.FilterOperator.EQ, '');

			var orFilter = new sap.ui.model.Filter({
				filters: [Obligatory, Code1, MeanValue],
				and: false
			}); //or 
			var oFilter = new sap.ui.model.Filter({
				filters: [orFilter, Evaluation],
				and: true
			}); //and

			var oFilter = [];
			var list = this.getView().byId("list");
			var oBinding = list.getBinding("items");
			oBinding.filter(oFilter);

		},
		onAfterRendering: function() {
			//debugger;

		},

		onSaveKarar: function(oEvent) {
			//debugger;
			this.onEnesDeneme();
			return;
			var Insplot = this.getModel("SelectItemModel2").getData().objectId;
			var Werks = this.getModel("SelectItemModel2").getData().Werks;
			var Code = this.getModel("SelectItemModel").getData().Code;
			var Auswahlmge = this.getModel("SelectItemModel").getData().Auswahlmge;
			var Codegruppe = this.getModel("SelectItemModel").getData().Codegruppe;
			var aciklama = this.getView().byId("__aciklamaID").getValue();
			/*	
				
				var Insplot ="890000001001";
				var Werks = "1000";
				var Code = "011";
				var Auswahlmge  = "KK03";
				var Codegruppe = "KR01";
				var aciklama = "test veri";*/

			 var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_DEMO_SRV");
			//var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			//var sPath = "/InspLotOpCharSet"+"(Insplot='"+Insplot+"',Inspoper='"+Inspoper+"',Inspchar='"+Inspchar+"')";
			if(!Insplot){
				Insplot = this.ObjectId;	
			}
			var sPath = "/UsageDecSet" + "(Insplot='" + Insplot + "')";

			// var sPath = "/EtZinsCharSet";
			var oPostData = {
				Insplot: Insplot,
				UdSelectedSet: Auswahlmge,
				UdPlant: Werks,
				UdCodeGroup: Codegruppe,
				UdCode: Code,
				UdTextLine: aciklama

			};

			oModel.update(sPath, oPostData,

				function(oData, response) {
					MessageToast.show("Başarılı bir şekilde Kaydedildi", {
						duration: 3000
					});
					//  console.log(oData);
					console.log(response);
				},
				function(err) {
					//	MessageToast.show(JSON.parse(error.response.body).error.message.value);
					var message = err.response.body.error.message.value;
					sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
					//console.log("Update failed", JSON.stringify(e));
				});
				
			oModel.attachRequestSent(function() {
				sap.ui.core.BusyIndicator.show(5);
			});
			
			return;
			oModel.attachRequestCompleted(function(oEvent) {
				sap.ui.core.BusyIndicator.hide();
				sap.m.MessageToast.show("Başarılı bir şekilde Kaydedildi", {
					duration: 3000
				});
				this.getRouter().navTo("master", true);
			});

			//   sap.ui.getCore().getModel("EtZinsCharModel").refresh();
			//   sap.ui.getCore().getModel("EtSingleResultsModel").refresh();
		},
		
		onEnesDeneme: function(){
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
			
			var sPath = "/UsageDecisionSet";//+ "(Insplot='" + this.ObjectId + "')";
			// oModel.read(sPath, {
			// 	// filters: ofilter,
			// 	success: function(oData, response) {
			// 		var jsonArray = [];
			// 		jsonArray = response.data.results;
			// 		var oJSONModel = new sap.ui.model.json.JSONModel();
			// 		oJSONModel.setData(jsonArray);
			// 		sap.ui.getCore().setModel(oJSONModel, "EtInspoperModel");
			// 		// console.log(jsonArray);
			// 	},
			// 	error: function(oError) {
			// 		// console.log("Error" + oError.responseText)
			// 	}
			// });
			
			var Insplot = this.getModel("SelectItemModel2").getData().objectId;
			// var Werks = this.getModel("SelectItemModel2").getData().Werks;
			var Code = this.getModel("SelectItemModel").getData().Code;
			var Auswahlmge = this.getModel("SelectItemModel").getData().Auswahlmge;
			var Codegruppe = this.getModel("SelectItemModel").getData().Codegruppe;
			var aciklama = this.getView().byId("__aciklamaID").getValue();
			
			var oPostData = {
				Insplot: this.ObjectId,
				UdSelectedSet: Auswahlmge,
				UdPlant: this.werks,
				UdCodeGroup: Codegruppe,
				UdCode: Code,
				UdTextLine: aciklama,
				Return:"",
				Message:""
			};
			
			oModel.create(sPath, oPostData, {
				success:function(oData, response) {
					if(oData.Return=="SUCCESS"){
						sap.m.MessageToast.show("Başarılı bir şekilde Kaydedildi");	
						history.go(-1);
					}else{
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});	
					}
				},
				error:function(err) {
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},

		onNavBack: function() {

			this.getView().byId("__aciklamaID").setValue("");
			this.getView().byId("__kararkoduID").setValue("");
			this.getView().byId("__koddegerlemesiID").setValue("");
			this.getView().byId("__kalitepuanID").setValue("");

			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				this.getRouter().navTo("master", {}, true);
				//	history.go(-1);
			} else {
				this.getRouter().navTo("master", {}, true);
			}

		},
		onExit: function() {
			if (this._oDialogDecision) {
				this._oDialogDecision.destroy();
			}

		}

	});

});