sap.ui.define([
		"com/nauticana/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/m/GroupHeaderListItem",
		"sap/ui/Device",
		"com/nauticana/demo/model/formatter",
		"com/nauticana/demo/model/grouper",
		"com/nauticana/demo/model/GroupSortState",
		"sap/ui/core/mvc/Controller"
	], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter, grouper, GroupSortState) {
		"use strict";


	return BaseController.extend("com.nauticana.demo.controller.BLListesi", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.BLListesi
		 */
			onInit: function() {
				//  filtering  
				this._oView = this.getView();
	 
				// Control state model
				var oList = this.byId("list"),
					oViewModel = this._createViewModel(),
					// Put down master list's original value for busy indicator delay,
					// so it can be restored later on. Busy handling on the master list is
					// taken care of by the master list itself.
					iOriginalBusyDelay = oList.getBusyIndicatorDelay();
			 

				this._oGroupSortState = new GroupSortState(oViewModel, grouper.groupUnitNumber(this.getResourceBundle()));

             	this._oList = oList;
				// keeps the filter and search state
				this._oListFilterState = {
					aFilter : [],
					aSearch : []
				};
		 
	            
				this.setModel(oViewModel, "masterView");
				// Make sure, busy indication is showing immediately so there is no
				// break after the busy indication for loading the view's meta data is
				// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
				oList.attachEventOnce("updateFinished", function(){
					// Restore original busy indicator delay for the list
					oViewModel.setProperty("/delay", iOriginalBusyDelay);
				});


				this.getRouter().getRoute("bllistesi").attachPatternMatched(this._onMasterMatched, this);
				this.getRouter().attachBypassed(this.onBypassed, this);
				
				
			},
			
			onSelectionChange: function(oEvent) {
				 //debugger;
		  		 
				var oSelect =  oEvent.getSource();
				
				var selectedItemObject = oSelect.getSelectedItem().getBindingContext().getObject();
			
			
			//	var NotifM = {}; 
			//	NotifM.NotifCollection = selectedItemObject;
				var NotifModel = new JSONModel(selectedItemObject); 
				//this.getView().setModel(NotifModel,"NotifModel");
				sap.ui.getCore().setModel(NotifModel,"NotifModel");
			
		 
		 	var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV"); 
		 
		 	var Uname = sap.ui.getCore().getModel("UserAuthModel").getData().Uname;
        	
			var ofilter9 =  [new Filter("IvUname", FilterOperator.EQ, Uname)];
		
			var sPath9 = "/NotifiCatDetlSet";
					 
    			oModel.read(sPath9, { 
    				filters: ofilter9,
					success : function(oData,response)
				{
						var jsonArray = [];
						 jsonArray = response.data.results;
						 
						 var oJSONModel = new sap.ui.model.json.JSONModel();
						 oJSONModel.setData(jsonArray);
						sap.ui.getCore().setModel(oJSONModel,"NDetlSetModel");

			           console.log(jsonArray);
            		 
    					}, 
					error : function(oError){ 
				    	console.log("Error" +oError.responseText) 
				    }
    			});	
    	
    			
    		
    		var ofilter10 =  [new Filter("IvUname", FilterOperator.EQ, Uname)];
		
			var sPath10 = "/NotifiCatHeadSet";
					 
    			oModel.read(sPath10, { 
    				filters: ofilter10,
					success : function(oData,response)
				{
						var jsonArray = [];
						 jsonArray = response.data.results;
						 
						 var oJSONModel = new sap.ui.model.json.JSONModel();
						 oJSONModel.setData(jsonArray);
						sap.ui.getCore().setModel(oJSONModel,"NHeadSetModel");

			           console.log(jsonArray);
            		 
    					}, 
					error : function(oError){ 
				    	console.log("Error" +oError.responseText) 
				    }
    			});	
    			
    			
			oModel.attachRequestSent(function(){
            		sap.ui.core.BusyIndicator.show(10);
    		});
			oModel.attachRequestCompleted(function(){
                sap.ui.core.BusyIndicator.hide();
            });
            
        
        
        
	 	    
			},
			_onMasterMatched : function(oEvent) {
			 
        	    //debugger;
               
        		var cb1 = sap.ui.getCore().getModel("mBLFilters").getData().checkbox0;
                var cb2 = sap.ui.getCore().getModel("mBLFilters").getData().checkbox1;
               
        		var IQmartf = sap.ui.getCore().getModel("mBLFilters").getData().bilturu;
        		var Qmnum  = sap.ui.getCore().getModel("mBLFilters").getData().bildirim;
        		var sFilterMatnr = sap.ui.getCore().getModel("mBLFilters").getData().Matnr;
        		var sValDate = sap.ui.getCore().getModel("mBLFilters").getData().Idate;
        		var sValFirstDate = sap.ui.getCore().getModel("mBLFilters").getData().firstDate;
                var sValSecondDate = sap.ui.getCore().getModel("mBLFilters").getData().secondDate;
                
                 var dateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
        			 pattern: "yyyy-MM-dd'T'HH:mm:ss.SSS"
				});
				//dateFormat.setUTCDate("GMT"+3);
			//	var date = new Date(sValFirstDate);
			//	date.setUTCDate(date);
				sValFirstDate = dateFormat.format(new Date(sValFirstDate));
				sValSecondDate = dateFormat.format(new Date(sValSecondDate));	
				//dateFormat.setUTCDate(TimeZone.getTimeZone("GMT"));
				
				
			//	var	sValFirstDate = dateFormat.format(new Date(sValFirstDate));
				//oDate1 = oDate1 + "T00:00:00";
			//sValFirstDate = sValFirstDate + "T30:30:00.000";
			//	var	oDate2 = dateFormat.format(new Date(sValSecondDate));
				
			//	var sValFirstDate2 = new sap.ui.unified.DateRange({startDate: sValFirstDate, endDate: sValSecondDate});
			
			 //sValSecondDate = sValSecondDate + "T30:30:00.000";
			
			//	var time = this.formatTime(12000);
              //  sValFirstDate = "datetime'"+  sValFirstDate + "T03:30:00.000'";
             //   sValSecondDate = "datetime'"+ sValSecondDate + "T03:30:00.000'";
                
                if((cb1 === true)&&(cb2 === false) ){
                	 var IStat = 'O';    //açık
                }else if((cb1 === false)&&(cb2 === true)){
                	 var IStat = 'C';   // tamamlanmış
                }else if((cb1 === true)&&(cb2 === true)){  // hepsi
                	 var IStat = ""; 
                }else{     // hiçbiri
                	 var IStat = "X"; 
                } 
               
                var IStat = ""; 
         
          		var sign = 'I';
    			var option = 'BT';
        		var sPathDate = "IDateRangeType";
        	 	var IStatPath = "IStat";
        	 //	var IQmartPath = "IQmart";
        	 	var IQmnumPath = "Qmnum";
        		var LowPath = "Low";
        	 	var HighPath = "High";
        		var OptionPath = "Option";
       			var SignPath = "Sign";
        	 	
        	 	
    			if(sValDate === "5") {
   

				//debugger;
				
				var sfilterDate =  new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.EQ, sValDate);
				var  sfsign =  new sap.ui.model.Filter(SignPath, sap.ui.model.FilterOperator.EQ, sign);
				var  sfoption =  new sap.ui.model.Filter(OptionPath, sap.ui.model.FilterOperator.EQ, option);
				var  sfFirstDate =  new sap.ui.model.Filter(LowPath, sap.ui.model.FilterOperator.GT, sValFirstDate);
				var  sfSecondDate =  new sap.ui.model.Filter(HighPath, sap.ui.model.FilterOperator.LT, sValSecondDate);
				
	 
				var  sfIStat =  new sap.ui.model.Filter(IStatPath, sap.ui.model.FilterOperator.EQ, IStat);
			//	var sfIQmart =  new sap.ui.model.Filter(IQmartPath, sap.ui.model.FilterOperator.EQ, IQmart);
				var  sfIQmnum =  new sap.ui.model.Filter(IQmnumPath, sap.ui.model.FilterOperator.EQ, Qmnum);
					
				var oDataFilter = new sap.ui.model.Filter({ 
						   filters: [ sfilterDate,sfsign,sfoption,sfFirstDate,sfSecondDate,sFilterMatnr,sfIStat,IQmartf,sfIQmnum],
						   and: true 
						});
        		}else{
				//debugger;
				
					var sfilterDate =  new sap.ui.model.Filter(sPathDate, sap.ui.model.FilterOperator.EQ, sValDate);
				
					var  sfIStat =  new sap.ui.model.Filter(IStatPath, sap.ui.model.FilterOperator.EQ, IStat);
				//	var sfIQmart =  new sap.ui.model.Filter(IQmartPath, sap.ui.model.FilterOperator.EQ, IQmart);
					var  sfIQmnum =  new sap.ui.model.Filter(IQmnumPath, sap.ui.model.FilterOperator.EQ, Qmnum);
					
					
					var oDataFilter = new sap.ui.model.Filter({ 
						filters: [ sfilterDate,sFilterMatnr,sfIStat,IQmartf,sfIQmnum],
						and: true 
					});

        		} 
				
				// var binding = this.byId("list").getBinding("items");
				// binding.filter(oDataFilter,true);
     
				var t = this;
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT2_SRV");
				
				var sPath = "/NotifListSet";
				
				oModel.read(sPath, {
					filters:[oDataFilter],
					success: function(oData, response) {
						var jsonArray = [];
						jsonArray = response.data.results;
						var oJSONModel = new sap.ui.model.json.JSONModel();
						oJSONModel.setData(jsonArray);
						sap.ui.getCore().setModel(oJSONModel, "EtInspoperModel");
						// console.log(jsonArray);
					},
					error: function(oError) {
						// console.log("Error" + oError.responseText)
					}
				});
				
				var t = this;
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZQM_INSPLOT_SRV");
				
				var sPath = "/NotifInfoSet";
				
				oModel.read(sPath, {
					filters:[oDataFilter],
					success: function(oData, response) {
						var oModel = new sap.ui.model.json.JSONModel({
				            NotifListCollection: response.data.results
				        });
				        t.getView().setModel(oModel, "NotifListModel");
					},
					error: function(oError) {
						// console.log("Error" + oError.responseText)
					}
				});
				
				
			 },
		 
			onBlDegistir : function(oEvent) {
			 
				var isSel = sap.ui.getCore().getModel("NHeadSetModel");
				if(isSel){
					this.getOwnerComponent().getRouter().navTo("bldegistir",true);
					
				}
			},
			onBlGoruntule : function(oEvent) {
			 
					var isSel = sap.ui.getCore().getModel("NHeadSetModel");
				if(isSel){
					this.getOwnerComponent().getRouter().navTo("blgoruntule",true);
					
				}
			},
			
			onNavBack : function() {

				var oHistory = sap.ui.core.routing.History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
					history.go(-1);
				} else {
					this.getRouter().navTo("master", {}, true);
				}
				
			},
	_createViewModel : function() {
				return new JSONModel({
					isFilterBarVisible: false,
					filterBarLabel: "",
					delay: 0,
					title: this.getResourceBundle().getText("masterTitleCount", [0]),
					noDataText: this.getResourceBundle().getText("masterListNoDataText"),
					sortBy: "Matnr",
					groupBy: "None"
				});
			},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.BLListesi
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.BLListesi
		 */
			onAfterRendering: function() {
		/*		
			var EtNotifAuthM = sap.ui.getCore().getModel("EtNotifAuthModel");
			var EtNotifAuthMM = {}; 
			EtNotifAuthMM.EtNotifAuthCollection = EtNotifAuthM.getData(); 
			var EtNotifM = new JSONModel(EtNotifAuthMM); 
			this.getView().setModel(EtNotifM,"EtNotifAuthModel"); */
		//debugger;
			var EtNotifAuthM = new JSONModel({
						change : false,
						display : false
					 
				});
				
				this.setModel(EtNotifAuthM, "EtNotifAuthM");
			
		        var ChangeNot = sap.ui.getCore().getModel("EtNotifAuthModel").getData().ChangeNot; 
			//	var CreateNot = sap.ui.getCore().getModel("EtNotifAuthModel").getData().CreateNot; 
				var DisplayNot = sap.ui.getCore().getModel("EtNotifAuthModel").getData().DisplayNot; 
				
			// ChangeNot='X';
			// DisplayNot='X';
				if((ChangeNot==='X') &&(DisplayNot==='X')) {
					this.getView().getModel("EtNotifAuthM").setData({
						change : true,
						display : true
            		});
            
				}
				if((ChangeNot==='X') &&(DisplayNot==='') ){
					this.getView().getModel("EtNotifAuthM").setData({
						change : true,
						display : false
            		});
				}
				if((ChangeNot==='') &&(DisplayNot==='X') ){
					this.getView().getModel("EtNotifAuthM").setData({
						change : false,
						display : true
            		});
				}
				if((ChangeNot==='') &&(DisplayNot==='') ){
					this.getView().getModel("EtNotifAuthM").setData({
						change : false,
						display : false
            		});
				}
 	
			}

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.BLListesi
		 */
		//	onExit: function() {
		//
		//	}

	});

});