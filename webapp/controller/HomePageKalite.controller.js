sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/nauticana/demo/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/unified/DateRange",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, BaseController, JSONModel, DateRange, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("com.nauticana.demo.controller.HomePageKalite", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.nauticana.demo.view.HomePageKalite
		 */
		onInit: function() {

			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({
				pattern: "yyyyMMdd",
				calendarType: sap.ui.core.CalendarType.Gregorian
			});

			var dateFrom = "";
			var dateTo = "";

			var cModel = new JSONModel();
			cModel.setData({
				firstDate: dateFrom,
				secondDate: dateTo
			});

			this.getView().setModel(cModel, "calModel");

		},
		onPressDetay: function(oEvent) {
			//debugger;
			var dateid = this.getView().byId("__dateID2").getSelectedKey();
			var statusid = this.getView().byId("__StatusID2").getSelectedKey();
			var firstDateid = this.getView().getModel("calModel").getData().firstDate;
			var secondDateid = this.getView().getModel("calModel").getData().secondDate;
			var oMaterialFilter = this._getMatNumbers();
			var oWerkFilter = this._getWerkNumbers();
			var oChargFilter = this._getChargNumbers();
			var oArtFilter = this._getArtNumbers();
			var oLagortFilter = this._getLagortNumbers();
			var zIvTip = "1";

			this.getView().getModel("mFilters").setData({
				Idate: dateid,
				firstDate: firstDateid,
				secondDate: secondDateid,
				Werk: oWerkFilter,
				Matnr: oMaterialFilter,
				Charg: oChargFilter,
				Art: oArtFilter,
				Lagortchrg: oLagortFilter,
				Stat35: statusid,
				IvTip: zIvTip
			});

			this.getOwnerComponent().getRouter().navTo("qrlist", true);// krdetaygoruntule
		},
		
		onPressDetay2: function(oEvent) {
			//debugger;
			var dateid = this.getView().byId("__dateID2").getSelectedKey();
			var statusid = this.getView().byId("__StatusID2").getSelectedKey();
			var firstDateid = this.getView().getModel("calModel").getData().firstDate;
			var secondDateid = this.getView().getModel("calModel").getData().secondDate;
			var oMaterialFilter = this._getMatNumbers();
			var oWerkFilter = this._getWerkNumbers();
			var oChargFilter = this._getChargNumbers();
			var oArtFilter = this._getArtNumbers();
			var oLagortFilter = this._getLagortNumbers();
			var zIvTip = "1";

			this.getView().getModel("mFilters").setData({
				Idate: dateid,
				firstDate: firstDateid,
				secondDate: secondDateid,
				Werk: oWerkFilter,
				Matnr: oMaterialFilter,
				Charg: oChargFilter,
				Art: oArtFilter,
				Lagortchrg: oLagortFilter,
				Stat35: statusid,
				IvTip: zIvTip
			});

			this.getOwnerComponent().getRouter().navTo("krdetaygoruntule", true);// krdetaygoruntule
		},

		_getMatNumbers: function() {
			var aTokens = this.byId("__malzemeID2").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
			if (aTokens.length != 0) {
				var aMatnrFilter = [];

				for (var i in aTokens) {
					aMatnrFilter.push(new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey()));
				}
				var oFilter = new sap.ui.model.Filter({
					filters: aMatnrFilter,
					and: false
				});
			} else {
				var aValue = this.byId("__malzemeID2").getValue();
				var sfilterMatnr = new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.EQ, aValue);
				oFilter = sfilterMatnr;
			}

			return oFilter;
		},
		_getWerkNumbers: function() {
			var aTokens = this.byId("__uYeriID2").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
			if (aTokens.length !== 0) {
				var aWerkFilter = [];
				for (var i in aTokens) {
					aWerkFilter.push(new sap.ui.model.Filter("Werk", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey()));
				}
				var oFilter = new sap.ui.model.Filter({
					filters: aWerkFilter,
					and: false
				});
			} else {
				var aValue = this.byId("__uYeriID2").getValue();
				var sfilterWerk = new sap.ui.model.Filter("Werk", sap.ui.model.FilterOperator.EQ, aValue);
				oFilter = sfilterWerk;
			}
			return oFilter;
		},
		_getChargNumbers: function() {
			var aTokens = this.byId("__partiNoID2").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
			if (aTokens.length !== 0) {
				var aChargFilter = [];
				for (var i in aTokens) {
					aChargFilter.push(new sap.ui.model.Filter("Charg", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey()));
				}
				var oFilter = new sap.ui.model.Filter({
					filters: aChargFilter,
					and: false
				});
			} else {
				var aValue = this.byId("__partiNoID2").getValue();
				var sfilterCharg = new sap.ui.model.Filter("Charg", sap.ui.model.FilterOperator.EQ, aValue);
				oFilter = sfilterCharg;
			}
			return oFilter;
		},
		_getArtNumbers: function() {
			var aTokens = this.byId("__turID2").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
			if (aTokens.length !== 0) {
				var aArtFilter = [];
				for (var i in aTokens) {
					aArtFilter.push(new sap.ui.model.Filter("Art", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey()));
				}
				var oFilter = new sap.ui.model.Filter({
					filters: aArtFilter,
					and: false
				});
			} else {
				var aValue = this.byId("__turID2").getValue();
				var sfilterArt = new sap.ui.model.Filter("Art", sap.ui.model.FilterOperator.EQ, aValue);
				oFilter = sfilterArt;
			}
			return oFilter;
		},
		_getLagortNumbers: function() {
			var aTokens = this.byId("__DYeriID2").getTokens(); //ITERATE THROUGH EACH VALUES ENTERED BY USER
			if (aTokens.length !== 0) {
				var aLagortFilter = [];
				for (var i in aTokens) {
					aLagortFilter.push(new sap.ui.model.Filter("Lagortchrg", sap.ui.model.FilterOperator.EQ, aTokens[i].getKey()));
				}
				var oFilter = new sap.ui.model.Filter({
					filters: aLagortFilter,
					and: false
				});
			} else {
				var aValue = this.byId("__DYeriID2").getValue();
				var sfilterLagort = new sap.ui.model.Filter("Lagortchrg", sap.ui.model.FilterOperator.EQ, aValue);
				oFilter = sfilterLagort;
			}
			return oFilter;
		},

		handleChange: function(oEvent) {
			var sFrom = oEvent.getParameter("from");
			var sTo = oEvent.getParameter("to");
			var bValid = oEvent.getParameter("valid");

			this._iEvent++;

			var oText = this.byId("TextEvent");
			oText.setText("Id: " + oEvent.getSource().getId() + "\nFrom: " + sFrom + "\nTo: " + sTo);

			var oDRS = oEvent.getSource();
			if (bValid) {
				oDRS.setValueState(sap.ui.core.ValueState.None);
			} else {
				oDRS.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onSelectChange: function(oEvent) {
			var skey = oEvent.getParameter("selectedItem").mProperties.key;

			if (skey === "5") {

				var oCalendarDialog = sap.ui.xmlfragment("com.nauticana.demo.view.Calendar", this);
				this.getView().addDependent(oCalendarDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
				oCalendarDialog.open();

			}
		},

		handleOkPress: function(oEvent) {
			//debugger;
			var dateFrom = sap.ui.getCore().byId("selectedDateFrom").getText();
			var dateTo = sap.ui.getCore().byId("selectedDateTo").getText();

			this.getView().getModel("calModel").setData({
				firstDate: dateFrom,
				secondDate: dateTo
			});

			// var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			//	var oSelectIdx = oSelectIndex.substr(25);

			//	if(oSelectIdx === "__dateID"){
			//			var oText = this.byId("__dateID");
			//	}else{
			//		
			//				var oText = this.byId("__dateID2");
			//	}

			//	var oText = this.byId("__dateID");
			var oText2 = this.byId("__dateID2");
			//	oText.getList().getItems()[5].setText(dateFrom + "-" + dateTo);
			oText2.getList().getItems()[5].setText(dateFrom + "-" + dateTo);
			sap.ui.getCore().byId("Calendar").destroy();

		},
		handleCalendarSelect: function(oEvent) {
			var oCalendar = oEvent.oSource;
			this._updateText(oCalendar);
		},
		_updateText: function(oCalendar) {

			var oSelectedDateFrom = sap.ui.getCore().byId("selectedDateFrom");
			var oSelectedDateTo = sap.ui.getCore().byId("selectedDateTo");
			var aSelectedDates = oCalendar.getSelectedDates();
			var oDate;
			if (aSelectedDates.length > 0) {
				oDate = aSelectedDates[0].getStartDate();
				if (oDate) {
					oSelectedDateFrom.setText(this.oFormatYyyymmdd.format(oDate));
				} else {
					oSelectedDateTo.setText("No Date Selected");
				}
				oDate = aSelectedDates[0].getEndDate();
				if (oDate) {
					oSelectedDateTo.setText(this.oFormatYyyymmdd.format(oDate));
				} else {
					oSelectedDateTo.setText("No Date Selected");
				}

			} else {
				oSelectedDateFrom.setText("No Date Selected");
				oSelectedDateTo.setText("No Date Selected");
			}
		},

		handleSelectThisWeek: function(oEvent) {
			this._selectWeekInterval(6);
		},

		handleSelectWorkWeek: function(oEvent) {
			this._selectWeekInterval(4);
		},

		_selectWeekInterval: function(iDays) {
			var oCurrent = new Date(); // get current date
			var iWeekstart = oCurrent.getDate() - oCurrent.getDay() + 1;
			var iWeekend = iWeekstart + iDays; // end day is the first day + 6
			var oMonday = new Date(oCurrent.setDate(iWeekstart));
			var oSunday = new Date(oCurrent.setDate(iWeekend));

			var oCalendar = this.getView().byId("calendar");

			oCalendar.removeAllSelectedDates();
			oCalendar.addSelectedDate(new DateRange({
				startDate: oMonday,
				endDate: oSunday
			}));

			this._updateText(oCalendar);
		},
		onExit: function() {
			if (this._oDialog) {
				this._oDialog.destroy();
			}
		},

		onValueHelpWerk: function(oEvent) {
			//debugger;
			if (!this._oDialogWerk) {
				this._oDialogWerk = sap.ui.xmlfragment("com.nauticana.demo.view.DialogWerk", this);
				this._oDialogWerk.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogWerk.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogWerk.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogWerk.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogWerk.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogWerk);
			this._oDialogWerk.open();
		},

		onValueHelpMatnr: function(oEvent) {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("com.nauticana.demo.view.DialogMatnr", this);
				this._oDialog.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialog.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialog.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialog.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialog.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
		},
		onValueHelpLagort: function(oEvent) {
			if (!this._oDialogLagort) {
				this._oDialogLagort = sap.ui.xmlfragment("com.nauticana.demo.view.DialogLagort", this);
				this._oDialogLagort.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogLagort.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogLagort.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogLagort.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogLagort.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogLagort);
			this._oDialogLagort.open();
		},
		onValueHelpCharg: function(oEvent) {
			if (!this._oDialogCharg) {
				this._oDialogCharg = sap.ui.xmlfragment("com.nauticana.demo.view.DialogCharg", this);
				this._oDialogCharg.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogCharg.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogCharg.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogCharg.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogCharg.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogCharg);
			this._oDialogCharg.open();
		},
		onValueHelpArt: function(oEvent) {
			if (!this._oDialogArt) {
				this._oDialogArt = sap.ui.xmlfragment("com.nauticana.demo.view.DialogArt", this);
				this._oDialogArt.setModel(this.getView().getModel("i18n"), "i18n");
				this._oDialogArt.setModel(this.getView().getModel());
			}

			// Multi-select if required
			//	var bMultiSelect = !!oEvent.getSource().data("multi");
			var bMultiSelect = !!oEvent.getSource().data();
			this._oDialogArt.setMultiSelect(bMultiSelect);

			// Remember selections if required
			var bRemember = !!oEvent.getSource().data("remember");
			this._oDialogArt.setRememberSelections(bRemember);

			// clear the old search filter
			this._oDialogArt.getBinding("items").filter([]);

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogArt);
			this._oDialogArt.open();
		},
		handleSearchMatnr: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Maktg", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleSearchWerk: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Bukrs", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleSearchCharg: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Charg", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleSearchArt: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Art", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleSearchLagort: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Lgort", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handleCloseWerk: function(oEvent) {
			//debugger;

			var aContexts = oEvent.getParameter("selectedContexts");
			//var aContexts2 = oEvent.getParameter("selectedItems");

			//		var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			//		var oSelectIdx = oSelectIndex.substr(25);

			//		if(oSelectIdx === "__uYeriID"){
			//				var oInput = this.getView().byId("__uYeriID");
			//			}else{

			var oInput = this.getView().byId("__uYeriID2");
			//		}

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Bukrs,
						text: oSel.Butxt
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		handleCloseMatnr: function(oEvent) {

			//debugger;

			var aContexts = oEvent.getParameter("selectedContexts");
			//var aContexts2 = oEvent.getParameter("selectedItems");

			//	var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			///		var oSelectIdx = oSelectIndex.substr(25);

			///		if(oSelectIdx === "__malzemeID"){
			//				var oInput = this.getView().byId("__malzemeID");
			//		}else{

			var oInput = this.getView().byId("__malzemeID2");
			//		}

			//var oInput = this.getView().byId("__malzemeID");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Matnr,
						text: oSel.Maktg
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		handleCloseCharg: function(oEvent) {
			//debugger;
			var aContexts = oEvent.getParameter("selectedContexts");
			//var aContexts2 = oEvent.getParameter("selectedItems");

			//		var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			//		var oSelectIdx = oSelectIndex.substr(25);

			//		if(oSelectIdx === "__partiNoID"){
			///				var oInput = this.getView().byId("__partiNoID");
			//		}else{

			var oInput = this.getView().byId("__partiNoID2");
			//		}

			//	var oInput = this.getView().byId("__partiNoID");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Charg,
						text: oSel.Charg
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		handleCloseArt: function(oEvent) {

			//debugger;

			var aContexts = oEvent.getParameter("selectedContexts");
			//var aContexts2 = oEvent.getParameter("selectedItems");

			//	var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			//		var oSelectIdx = oSelectIndex.substr(25);

			//	if(oSelectIdx === "__turID"){
			//			var oInput = this.getView().byId("__turID");
			//	}else{

			var oInput = this.getView().byId("__turID2");
			//		}

			//	var oInput = this.getView().byId("__turID");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Art,
						text: oSel.Kurztext
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		handleCloseLagort: function(oEvent) {

			var aContexts = oEvent.getParameter("selectedContexts");
			//var aContexts2 = oEvent.getParameter("selectedItems");

			//	var oSelectIndex = oEvent.oSource.oParent.oCore.oFocusHandler.oLastFocusedControlInfo.id;
			//var index = oEvent.oSource.oParent.oParent.indexOfItem(oEvent.oSource.oParent);
			//	var oSelectIdx = oSelectIndex.substr(25);

			//	if(oSelectIdx === "__DYeriID"){
			//			var oInput = this.getView().byId("__DYeriID");
			//	}else{

			var oInput = this.getView().byId("__DYeriID2");
			//	}

			//	var oInput = this.getView().byId("__DYeriID");

			var aTokens = [];

			if (aContexts && aContexts.length) {

				for (var i = 0; i < aContexts.length; i++) {
					var oSel = aContexts[i].getModel().getProperty(aContexts[i].getPath());
					var token1 = new sap.m.Token({
						key: oSel.Lgort,
						text: oSel.Lgobe
					});
					aTokens[i] = token1;
				}
				oInput.setTokens(aTokens);
			} else {

				//	MessageToast.show("No new item was selected.");

			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		onNavBack: function() {
			var oHistory = sap.ui.core.routing.History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				//	this.getRouter().navTo("master", {}, true);
				history.go(-1);
			} else {
				this.getRouter().navTo("master", {}, true);
			}
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.nauticana.demo.view.HomePageKalite
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.nauticana.demo.view.HomePageKalite
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.nauticana.demo.view.HomePageKalite
		 */
		//	onExit: function() {
		//
		//	}

	});

});